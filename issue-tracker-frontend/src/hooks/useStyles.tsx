import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
  },
  flexDirectionColumn: {
    display: "flex",
    flexDirection: "column",
    flexGrow: 1,
    padding: theme.spacing(2),
  },
  flexDirectionColumnWithoutPadding: {
    display: "flex",
    flexDirection: "column",
    flexGrow: 1,
  },
  flexDirectionRow: {
    display: "flex",
    flexDirection: "row",
    flexGrow: 1,
    flexWrap: "wrap",
    overflow: "auto",
    maxHeight: "100px",
    // padding: theme.spacing(2)
  },
  flexDirectionRowWithoutFlexGrow: {
    display: "flex",
    flexDirection: "row",
    paddingRight: theme.spacing(1),
  },
  flexDirectionColumnWithoutFlexGrow: {
    display: "flex",
    flexDirection: "column",
  },
  buttonStyle: {
    marginTop: theme.spacing(2),
  },
  tabsStyle: {
    flexGrow: 1,
    width: "100%",
    backgroundColor: theme.palette.background.paper,
  },
  pageableTablePaginationActions: {
    flexShrink: 0,
    marginLeft: theme.spacing(2.5),
  },
  pageableTable: {
    table: {
      minWidth: 500,
    },
  },
  invalidFeedback: {
    color: "red",
  },
  scrollable: {
    overflow: "auto",
  },
  cursorPointer: {
    cursor: "pointer",
  },
  flexJustifyItemsEnd: {
    display: "flex",
    justifyItems: "end",
  },
  floatRight: {
    float: "right",
  },
}));
