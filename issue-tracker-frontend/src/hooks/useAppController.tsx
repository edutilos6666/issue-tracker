import { Maybe } from "../types/Maybe";
import { useContext, useEffect, useState, useCallback } from "react";
import { AppContext, UserInfoInterface } from "../context/AppContext";
import * as jwt from "jsonwebtoken";
import {
  LOCAL_STORAGE_SESSION_INFO,
  LOCAL_STORAGE_TOKEN,
} from "../utils/StorageConstants";
import { useSecurityService } from "../services/useSecurityService";

export interface TokenInterface {
  iat?: number;
  exp?: number;
  nbf?: number;
  sub?: string; // subject of jwt was set userId in backend
  firstname?: string;
  lastname?: string;
  username?: string;
  roles?: [string];
}

interface AppControllerInterface {
  token?: Maybe<string>;
  setToken: (token: Maybe<string>) => void;
  loggedIn?: Maybe<boolean>;
  setLoggedIn: (loggedIn: Maybe<boolean>) => void;
  userInfo?: Maybe<UserInfoInterface>;
  setUserInfo: (userInfo: Maybe<UserInfoInterface>) => void;
  checkSessionInfo: () => void;
  isLoggedInInit: () => boolean | undefined | null | void;
  logout: () => void;
  refreshAccessToken: () => void;
  getAuthHeaders: () => any;
}
const useAppController = (): AppControllerInterface => {
  const {
    token,
    setToken,
    loggedIn,
    setLoggedIn,
    userInfo,
    setUserInfo,
    setLastCheckDate,
  } = useContext(AppContext);

  const [publicKeyData, setPublicKeyData] = useState<string>();

  const getAuthHeaders = () => {
    // debugger;
    // if (!localStorage.getItem(LOCAL_STORAGE_TOKEN)) {
    //   setToken(null);
    // }
    return {
      accept: "application/json",
      "content-type": "application/json",
      authorization: `bearer ${localStorage.getItem("token")}`,
      credentials: "include",
    };
  };

  const {
    fetchPublicKey,
    refreshAccessTokenRemote,
    logout: logoutRemote,
  } = useSecurityService();

  useEffect(() => {
    const storageWatcher = () => {
      console.log("storage changed");
      if (!localStorage.getItem(LOCAL_STORAGE_TOKEN)) {
        setToken(null);
        window.location.reload();
      }
    };

    window.addEventListener("storage", storageWatcher);
    return () => {
      window.removeEventListener("storage", storageWatcher);
    };
  }, []);

  useEffect(() => {
    refreshToken();
    console.log("token changed", token);
  }, [token, publicKeyData]);

  useEffect(() => {
    fetchPublicKey().then((res) => {
      console.log(res);
      setPublicKeyData(res.data);
    });
  }, []);

  useEffect(() => {
    setToken(localStorage.getItem(LOCAL_STORAGE_TOKEN));
  }, [setToken]);

  const isLoggedInInit = () => {
    // if (!loggedIn) {
    //   return refreshToken();
    // }
    return loggedIn;
  };

  const refreshToken = () => {
    if (token && token.length > 0 && publicKeyData) {
      localStorage.setItem("token", token);
      const decodedWithoutVerify = jwt.decode(token) as TokenInterface;

      // verify and save token into localStorage
      try {
        jwt.verify(token, publicKeyData ? publicKeyData : "");
      } catch (ex) {
        console.log("Token verification failed.", ex);
        localStorage.removeItem(LOCAL_STORAGE_SESSION_INFO);
        localStorage.removeItem(LOCAL_STORAGE_TOKEN);
        setToken(null);
        setLastCheckDate(null);
        setLoggedIn(false);
        setUserInfo(null);
        return;
      }

      if (decodedWithoutVerify != null) {
        setLastCheckDate(decodedWithoutVerify.iat);
        const userInfo = {
          id: decodedWithoutVerify.sub,
          username: decodedWithoutVerify.username,
          firstname: decodedWithoutVerify.firstname,
          lastname: decodedWithoutVerify.lastname,
          roles: decodedWithoutVerify.roles ? decodedWithoutVerify.roles : [], // TODO: changed roles from string[]|undefined -> string[]
        };

        setUserInfo(userInfo);
        setLoggedIn(true);
        localStorage.setItem(LOCAL_STORAGE_TOKEN, token);
        localStorage.setItem(
          LOCAL_STORAGE_SESSION_INFO,
          JSON.stringify({
            exp: decodedWithoutVerify.exp,
            iat: decodedWithoutVerify.iat,
            nbf: decodedWithoutVerify.nbf,
            userInfo: userInfo,
          })
        );
      }
    } else {
      checkSessionInfo();
    }
  };

  const calculateRemainingTimePercent = (iat: number, exp: number) => {
    const exp_iat_diff_seconds = exp - iat;
    const exp_iat_current_time_diff_seconds = exp - Date.now() / 1000;
    const percent =
      100 - (exp_iat_current_time_diff_seconds / exp_iat_diff_seconds) * 100;
    return percent;
  };

  const checkSessionInfo = () => {
    const sessionInfo = navigator.cookieEnabled
      ? localStorage.getItem(LOCAL_STORAGE_SESSION_INFO)
      : null;
    if (sessionInfo) {
      const sessionObject = JSON.parse(sessionInfo);
      // if token already expired
      if (sessionObject.exp < Date.now() / 1000) {
        logout();
      } else {
        const timePercent = calculateRemainingTimePercent(
          sessionObject.iat,
          sessionObject.exp
        );
        // check if localStorage has the latest data, if not reload window
        if (userInfo && userInfo.username !== sessionObject.userInfo.username) {
          window.location.reload();
        } else if (userInfo && loggedIn) {
          // if time span between iat and current time in relation to exp (iat -> curr -> exp)
          // is > 10% => then make backend refreshAccessToken()
          if (timePercent > 10) {
            refreshAccessToken();
          }
        } else {
          // I am not sure, if that is correct to relogin user here
          // because checkSessionInfo() is called when token and/or publicKeyData are not correct.
          setUserInfo(sessionObject.userInfo);
          setLoggedIn(true);
        }
      }
    }
  };

  const refreshAccessToken = () => {
    console.log("refreshAccessToken");
    refreshAccessTokenRemote().then((res) => {
      console.log(res.data);
      if (res && res.data && res.data.authResponse && res.data.token) {
        setToken(res.data.token);
      }
    });
  };

  const logout = useCallback(() => {
    if (token) {
      logoutRemote(token).then((res) => {
        console.log(res);
        logoutCleanup();
      });
    } else {
      logoutCleanup();
    }
  }, [setLoggedIn, setToken]);

  const logoutCleanup = () => {
    localStorage.removeItem(LOCAL_STORAGE_SESSION_INFO);
    localStorage.removeItem(LOCAL_STORAGE_TOKEN);
    setLoggedIn(false);
    setToken(null);
    window.location.reload();
  };

  return {
    token,
    setToken,
    loggedIn,
    setLoggedIn,
    userInfo,
    setUserInfo,
    checkSessionInfo,
    logout,
    refreshAccessToken,
    isLoggedInInit,
    getAuthHeaders,
  };
};

export default useAppController;
