import { User } from "./User";

export interface Comment {
  id?: string;
  issueId: string;
  user: User;
  content: string;
  createdAt: Date;
}
