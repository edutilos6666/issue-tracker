import { User } from "./User";
import { Issue } from "./Issue";

export interface Project {
  id?: string;
  name: string;
  description: string;
  createdAt: Date;
  lastModifiedAt: Date;
  users: Array<User>;
  // issues: Array<Issue>;
}
