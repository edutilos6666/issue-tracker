import { User } from "./User";
import { Priority } from "./Priority";
import { Severity } from "./Severity";
import { Status } from "./Status";
import { Label } from "./Label";
import { Comment } from "./Comment";

export interface Issue {
  id?: string;
  projectId: string;
  name: string;
  description: string;
  createdAt: Date;
  lastModifiedAt: Date;
  createdBy: User;
  assignedTo: User;
  priority: Priority;
  severity: Severity;
  status: Status;
  labels: Array<Label>;
  // comments: Array<Comment>;
}
