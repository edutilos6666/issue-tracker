export interface Severity {
  id: string;
  name: string;
}
