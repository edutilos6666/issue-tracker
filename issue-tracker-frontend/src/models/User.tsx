import { Role } from "./Role";

export interface User {
  id?: string;
  username: string;
  password: string;
  firstname: string;
  lastname: string;
  roles: Role[];
}
