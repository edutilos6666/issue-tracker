export interface Role {
  id: string;
  name: string;
  readonly: boolean;
}
