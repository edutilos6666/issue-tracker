export type CustomWindow = Window & WindowExtension;

interface ENV {
  REACT_APP_BASE_URL?: string;
  REACT_APP_SOCKET_URL?: string;
}

interface WindowExtension {
  ENV?: ENV;
}

const useBaseUrl = () => {
  const browserWindow: CustomWindow = window;
  const baseUrl =
    browserWindow.ENV && browserWindow.ENV.REACT_APP_BASE_URL
      ? browserWindow.ENV.REACT_APP_BASE_URL
      : process.env.REACT_APP_BASE_URL;

  const baseSocketUrl =
    browserWindow.ENV && browserWindow.ENV.REACT_APP_SOCKET_URL
      ? browserWindow.ENV.REACT_APP_SOCKET_URL
      : process.env.REACT_APP_SOCKET_URL;
  return { baseUrl, baseSocketUrl };
};

export default useBaseUrl;
