export type ColorType = undefined | "primary" | "secondary" | "default";

const colors: ColorType[] = ["primary", "secondary", "default"];
export const getRandomColorFromString = (value: string): ColorType => {
  if (!value) return colors[colors.length - 1];
  let calculatedNumber = 0;
  for (let i = 0; i < value.length; ++i) {
    calculatedNumber += value.charCodeAt(i);
  }
  let randomIndex =
    Math.floor(Math.random() * calculatedNumber) % colors.length;
  return colors[randomIndex];
};
