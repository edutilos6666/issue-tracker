export const useTimeUtil = () => {
  const optionsDate = {
    year: "numeric",
    month: "2-digit",
    day: "2-digit"
  };
  const optionsTime = {
    hour: "2-digit",
    minute: "2-digit",
    second: "2-digit"
  };

  const optionsDateTime = {
    year: "numeric",
    month: "2-digit",
    day: "2-digit",
    hour: "2-digit",
    minute: "2-digit",
    second: "2-digit"
  };

  const convertTimestampToDate = (ts: string | undefined): Date => {
    if (ts !== undefined) return new Date(parseInt(ts));
    return new Date();
  };

  const convertAndFormatTimestamp = (ts: string | undefined): string => {
    const date: Date = convertTimestampToDate(ts);
    return date.toLocaleDateString("en-US", optionsDate);
  };

  const formatDate = (date: Date): string => {
    return date.toLocaleDateString("en-US", optionsDate);
  };

  const formatDateTime = (date: string): string => {
    return new Date(date).toLocaleDateString("en-US", optionsDateTime);
  };

  const convertTimestampToDateString = (ts: string | undefined): string => {
    const date: Date = convertTimestampToDate(ts);
    return date.toLocaleDateString("en-US", optionsDate);
  };

  const convertTimestampToTimeString = (ts: string | undefined): string => {
    const date: Date = convertTimestampToDate(ts);
    return date.toLocaleTimeString("en-US", optionsTime);
  };

  const convertTimestampToDateTimeString = (ts: string | undefined): string => {
    const date: Date = convertTimestampToDate(ts);
    return date.toLocaleTimeString("en-US", optionsDateTime);
  };

  const convertDateToUTCDate = (input: Date): Date => {
    return new Date(
      Date.UTC(input.getFullYear(), input.getMonth(), input.getDate())
    );
  };

  const convertDateTimeToUTCDateTime = (input: Date): Date => {
    return new Date(
      input.getUTCFullYear(),
      input.getUTCMonth(),
      input.getUTCDate(),
      input.getUTCHours(),
      input.getUTCMinutes(),
      input.getUTCSeconds(),
      input.getUTCMilliseconds()
    );
  };

  const checkDatesAreEquals = (
    datum1: Date | null,
    datum2: Date | null
  ): boolean => {
    if (datum1 === null && datum2 === null && datum1 === datum2) return true;
    else if (
      datum1 !== null &&
      datum2 !== null &&
      datum1.getFullYear() === datum2.getFullYear() &&
      datum1.getMonth() === datum2.getMonth() &&
      datum1.getDate() === datum2.getDate()
    )
      return true;
    return false;
  };

  const updateAndGetTimeForDate = (
    date: Date,
    lowerBound: boolean = true
  ): number => {
    const tmp = new Date(date);
    if (lowerBound) {
      tmp.setHours(0);
      tmp.setMinutes(0);
      tmp.setSeconds(0);
      tmp.setMilliseconds(0);
    } else {
      tmp.setHours(23);
      tmp.setMinutes(59);
      tmp.setSeconds(59);
      tmp.setMilliseconds(999);
    }
    return tmp.getTime();
  };

  const updateAndGetDate = (
    date: Date | undefined,
    lowerBound: boolean = true
  ): Date | undefined => {
    if (date === undefined) return date;
    const tmp = new Date(date);
    if (lowerBound) {
      tmp.setHours(0);
      tmp.setMinutes(0);
      tmp.setSeconds(0);
      tmp.setMilliseconds(0);
    } else {
      tmp.setHours(23);
      tmp.setMinutes(59);
      tmp.setSeconds(59);
      tmp.setMilliseconds(999);
    }
    return tmp;
  };

  const getMaxTimestamp = (tsList: any[]): string => {
    return `${Math.max(...tsList.map(one => parseFloat(one)))}`;
  };

  const padTimeUnit = (timeUnit: number): string => {
    return timeUnit < 10 ? `0${timeUnit}` : `${timeUnit}`;
  };

  const convertDateToISODateTime = (
    date: Date,
    lowerBound: boolean = true,
    includeTimezone: boolean = true
  ): string => {
    const fullYear: number = date.getFullYear();
    const month: number = date.getMonth() + 1;
    const dayOfMonth: number = date.getDate();
    let hours = 0;
    let minutes = 0;
    let seconds = 0;
    let ms = 0;
    if (!lowerBound) {
      hours = 23;
      minutes = 59;
      seconds = 59;
      ms = 999;
    }
    let res = `${fullYear}-${padTimeUnit(month)}-${padTimeUnit(
      dayOfMonth
    )}T${padTimeUnit(hours)}:${padTimeUnit(minutes)}:${padTimeUnit(
      seconds
    )}.${ms}`;
    if (includeTimezone) {
      const tz = (date.getTimezoneOffset() / 60).toFixed(2);
      const parts0: string[] = tz.split("-");
      const parts0_firstElement = parts0.length > 1 ? parts0[1] : parts0[0];
      const parts: string[] = parts0_firstElement.split(".");
      date.getTimezoneOffset() > 0
        ? (res = `${res}+${padTimeUnit(parseInt(parts[0]))}:${parts[1]}`)
        : (res = `${res}-${padTimeUnit(parseInt(parts[0]))}:${parts[1]}`);
    }
    return res;
  };

  return {
    optionsDate,
    optionsTime,
    optionsDateTime,
    convertTimestampToDate,
    convertAndFormatTimestamp,
    formatDate,
    formatDateTime,
    convertTimestampToDateString,
    convertTimestampToTimeString,
    convertTimestampToDateTimeString,
    convertDateToUTCDate,
    convertDateTimeToUTCDateTime,
    checkDatesAreEquals,
    updateAndGetTimeForDate,
    updateAndGetDate,
    getMaxTimestamp,
    convertDateToISODateTime
  };
};
