export const PROJECT_OVERVIEW_BASE_PATH = "/";
export const PROJECT_DETAILS_BASE_PATH = "/project-details";
export const ISSUE_DETAILS_BASE_PATH = "/issue-details";
export const USER_DETAILS_BASE_PATH = "/user-details";
export const ISSUE_CREATE_BASE_PATH = "/issue-create";
export const PROJECT_CREATE_BASE_PATH = "/project-create";
export const USER_HOME_BASE_PATH = "/home";
