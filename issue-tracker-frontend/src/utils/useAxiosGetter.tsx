import { LOCAL_STORAGE_TOKEN } from "./StorageConstants";
import useAppController from "../hooks/useAppController";
import Axios from "axios";

const useAxiosGetter = () => {
  const token = localStorage.getItem(LOCAL_STORAGE_TOKEN);
  const { refreshAccessToken } = useAppController();
  if (!token) {
    refreshAccessToken();
  }

  return {
    Axios,
  };
};

export default useAxiosGetter;
