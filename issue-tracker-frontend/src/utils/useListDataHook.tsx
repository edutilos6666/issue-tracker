import useBaseUrl from "./useBaseUrl";
import { useState, useEffect } from "react";
import { Status } from "../models/Status";
import Axios from "axios";
import { Priority } from "../models/Priority";
import { Severity } from "../models/Severity";
import { Label } from "../models/Label";
import { getAuthHeaders } from "../services/AuthHeadersService";
import { Role } from "../models/Role";

export const useListDataHooks = () => {
  const { baseUrl } = useBaseUrl();

  const useFindAllStatus = () => {
    const [statusList, setStatusList] = useState<Status[]>([]);

    useEffect(() => {
      Axios.get(`${baseUrl}/status`, {
        headers: getAuthHeaders(),
      }).then((res) => {
        setStatusList(res.data);
      });
    }, [baseUrl]);
    return {
      statusList,
    };
  };

  const useFindAllPriorities = () => {
    const [priorityList, setPriorityList] = useState<Priority[]>([]);
    useEffect(() => {
      Axios.get(`${baseUrl}/priorities`, {
        headers: getAuthHeaders(),
      }).then((res) => {
        setPriorityList(res.data);
      });
    }, [baseUrl]);
    return {
      priorityList,
    };
  };

  const useFindAllSeverity = () => {
    const [severityList, setSeverityList] = useState<Severity[]>([]);
    useEffect(() => {
      Axios.get(`${baseUrl}/severity`, {
        headers: getAuthHeaders(),
      }).then((res) => {
        setSeverityList(res.data);
      });
    }, [baseUrl]);
    return {
      severityList,
    };
  };

  const useFindAllLabels = () => {
    const [labelList, setLabelList] = useState<Label[]>([]);
    useEffect(() => {
      Axios.get(`${baseUrl}/labels`, {
        headers: getAuthHeaders(),
      }).then((res) => {
        setLabelList(res.data);
      });
    }, [baseUrl]);
    return {
      labelList,
    };
  };

  const useFindAllRoles = () => {
    const [roleList, setRoleList] = useState<Role[]>([]);
    useEffect(() => {
      Axios.get(`${baseUrl}/roles`, {
        // headers: getAuthHeaders(),
      }).then((res) => {
        setRoleList(res.data);
      });
    }, [baseUrl]);
    return {
      roleList,
    };
  };

  return {
    useFindAllStatus,
    useFindAllPriorities,
    useFindAllSeverity,
    useFindAllLabels,
    useFindAllRoles,
  };
};
