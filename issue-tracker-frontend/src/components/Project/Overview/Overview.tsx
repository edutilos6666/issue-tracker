import React, { FC, useReducer, useState } from "react";
import {
  Button,
  ExpansionPanel,
  ExpansionPanelSummary,
  Typography,
  ExpansionPanelDetails,
} from "@material-ui/core";
import { useStyles } from "../../../hooks/useStyles";
import {
  ProjectOverviewReducer,
  emptyInstance_ProjectOverviewData,
} from "./TypesAndReducer";
import { useLocalStyles } from "../useLocalStyles";
import { ProjectOverviewTable } from "./Table/Table";
import { ProjectOverviewFilter } from "./Filter/Filter";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { PROJECT_CREATE_BASE_PATH } from "../../../utils/RouterConstants";
import { Maybe } from "../../../types/Maybe";
import { useHistory } from "react-router";

interface Props {
  username?: Maybe<string>;
}

export const ProjectOverview: FC<Props> = ({ username }) => {
  const { flexDirectionColumn, buttonStyle } = useStyles();
  const { heading } = useLocalStyles();
  const [reducerData, reducerDispatch] = useReducer(ProjectOverviewReducer, {
    ...emptyInstance_ProjectOverviewData,
    username: username,
  });
  const [expanded, setExpanded] = useState<boolean>(true);

  const history = useHistory();

  const handleCreateNewProject = () => {
    history.push(`${PROJECT_CREATE_BASE_PATH}`);
  };

  return (
    <div className={flexDirectionColumn}>
      <ExpansionPanel
        expanded={expanded}
        onChange={() => setExpanded(!expanded)}
      >
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel-filter-content"
          id="panel-filter-header"
        >
          <Typography className={heading}>
            <strong>Filter</strong>
          </Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <ProjectOverviewFilter
            reducerData={reducerData}
            reducerDispatch={reducerDispatch}
          />
        </ExpansionPanelDetails>
      </ExpansionPanel>

      <ProjectOverviewTable reducerData={reducerData} />

      {username ? (
        ""
      ) : (
        <Button
          variant="contained"
          color="primary"
          className={buttonStyle}
          onClick={handleCreateNewProject}
        >
          Create new project
        </Button>
      )}
    </div>
  );
};

// ProjectOverview.defaultProps = {
//   forCurrentUser: undefined,
//   username: undefined,
// };
