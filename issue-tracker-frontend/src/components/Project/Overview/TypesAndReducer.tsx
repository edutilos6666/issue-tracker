import { Maybe } from "../../../types/Maybe";

export interface ProjectOverviewData {
  name?: Maybe<string>;
  description?: Maybe<string>;
  createdAtFrom?: Maybe<Date>;
  createdAtTo?: Maybe<Date>;
  lastModifiedAtFrom?: Maybe<Date>;
  lastModifiedAtTo?: Maybe<Date>;
  username?: Maybe<string>;
  firstname?: Maybe<string>;
  lastname?: Maybe<string>;
  triggerRefetch?: Maybe<boolean>;
  // that was too slow for ExpansionPanel
  expansionPanelExpanded?: Maybe<boolean>;
}

export const emptyInstance_ProjectOverviewData: ProjectOverviewData = {
  name: undefined,
  description: undefined,
  createdAtFrom: null,
  createdAtTo: null,
  lastModifiedAtFrom: null,
  lastModifiedAtTo: null,
  username: undefined,
  firstname: undefined,
  lastname: undefined,
  triggerRefetch: undefined,
  expansionPanelExpanded: true,
};

export enum ProjectOverviewReducerActionTypes {
  SET_NAME = "setName",
  SET_DESCRIPTION = "setDescription",
  SET_CREATED_AT_FROM = "setCreatedAtFrom",
  SET_CREATED_AT_TO = "setCreatedAtTo",
  SET_LAST_MODIFIED_AT_FROM = "setLastModifiedAtFrom",
  SET_LAST_MODIFIED_AT_TO = "setLastModifiedAtTo",
  SET_USERNAME = "setUsername",
  SET_FIRSTNAME = "setFirstname",
  SET_LASTNAME = "setLastname",
  SET_TRIGGER_REFETCH = "setTriggerRefetch",
  SET_EXPANSION_PANEL_EXPANDED = "setExpansionPanelExpanded",
  SET_OBJECT = "SET_OBJECT",
}

export const {
  SET_NAME,
  SET_DESCRIPTION,
  SET_CREATED_AT_FROM: SET_CREATEDAT_FROM,
  SET_CREATED_AT_TO: SET_CREATEDAT_TO,
  SET_LAST_MODIFIED_AT_FROM: SET_LASTMODIFIEDAT_FROM,
  SET_LAST_MODIFIED_AT_TO: SET_LASTMODIFIEDAT_TO,
  SET_USERNAME,
  SET_FIRSTNAME,
  SET_LASTNAME,
  SET_TRIGGER_REFETCH,
  SET_EXPANSION_PANEL_EXPANDED,
  SET_OBJECT,
} = ProjectOverviewReducerActionTypes;

export type ProjectOverviewReducerAction =
  | {
      type:
        | ProjectOverviewReducerActionTypes.SET_NAME
        | ProjectOverviewReducerActionTypes.SET_DESCRIPTION
        | ProjectOverviewReducerActionTypes.SET_USERNAME
        | ProjectOverviewReducerActionTypes.SET_FIRSTNAME
        | ProjectOverviewReducerActionTypes.SET_LASTNAME;
      value: string;
    }
  | {
      type:
        | ProjectOverviewReducerActionTypes.SET_CREATED_AT_FROM
        | ProjectOverviewReducerActionTypes.SET_CREATED_AT_TO
        | ProjectOverviewReducerActionTypes.SET_LAST_MODIFIED_AT_FROM
        | ProjectOverviewReducerActionTypes.SET_LAST_MODIFIED_AT_TO;
      value: Date;
    }
  | {
      type:
        | ProjectOverviewReducerActionTypes.SET_EXPANSION_PANEL_EXPANDED
        | ProjectOverviewReducerActionTypes.SET_TRIGGER_REFETCH;
      value: boolean;
    }
  | {
      type: ProjectOverviewReducerActionTypes.SET_OBJECT;
      value: ProjectOverviewData;
    };

export const ProjectOverviewReducer = (
  state: ProjectOverviewData,
  action: ProjectOverviewReducerAction
): ProjectOverviewData => {
  let newValue;
  switch (action.type) {
    case SET_NAME:
      newValue = { ...state, name: action.value };
      break;
    case SET_DESCRIPTION:
      newValue = { ...state, description: action.value };
      break;

    case SET_CREATEDAT_FROM:
      newValue = { ...state, createdAtFrom: action.value };
      break;
    case SET_CREATEDAT_TO:
      newValue = { ...state, createdAtTo: action.value };
      break;
    case SET_LASTMODIFIEDAT_FROM:
      newValue = { ...state, lastModifiedAtFrom: action.value };
      break;
    case SET_LASTMODIFIEDAT_TO:
      newValue = { ...state, lastModifiedAtTo: action.value };
      break;
    case SET_USERNAME:
      newValue = { ...state, username: action.value };
      break;
    case SET_FIRSTNAME:
      newValue = { ...state, firstname: action.value };
      break;
    case SET_LASTNAME:
      newValue = { ...state, lastname: action.value };
      break;
    case SET_TRIGGER_REFETCH:
      newValue = { ...state, triggerRefetch: action.value };
      break;
    case SET_EXPANSION_PANEL_EXPANDED:
      newValue = { ...state, expansionPanelExpanded: action.value };
      break;
    case SET_OBJECT:
      newValue = action.value;
      break;
    default:
      newValue = state;
  }

  return newValue;
};
