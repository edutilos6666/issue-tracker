import React, { FC } from "react";
import { Project } from "../../../../models/Project";
import { useStyles } from "../../../../hooks/useStyles";
import { Paper, Button } from "@material-ui/core";
import { useHistory } from "react-router";

interface Props {
  project: Project;
}
export const ProjectOverviewItem: FC<Props> = ({ project }) => {
  const { paper } = useStyles();
  const history = useHistory();
  const handleBtnDetailsClick = () => {
    history.push(`/project-details/${project.id}`, { project: project });
  };
  return (
    <Paper className={paper}>
      {project.name}
      <Button
        variant="contained"
        color="primary"
        onClick={handleBtnDetailsClick}
      >
        Details
      </Button>
    </Paper>
  );
};
