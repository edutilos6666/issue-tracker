import React, { FC, useState, useEffect } from "react";
import PageableTableContainer from "../../../PageableTable/Container/Container";
import { Issue } from "../../../../models/Issue";
import useColumns from "./useColumns";
import { useTimeUtil } from "../../../../utils/useTimeUtil";
import { ProjectOverviewData } from "../TypesAndReducer";
import { useProjectService } from "../../../../services/useProjectService";
import { PROJECT_DETAILS_BASE_PATH } from "../../../../utils/RouterConstants";
import { useStyles } from "../../../../hooks/useStyles";
import { useHistory } from "react-router";

interface Props {
  reducerData: ProjectOverviewData;
}

export const ProjectOverviewTable: FC<Props> = ({ reducerData }) => {
  const [page, setPage] = useState<number>(0);
  const [rowsPerPage, setRowsPerPage] = useState<number>(5);
  const [rows, setRows] = useState<Issue[]>([]);
  const [count, setCount] = useState<number>(0);

  const { columns } = useColumns();
  const { convertDateToISODateTime } = useTimeUtil();

  const history = useHistory();
  const { findAllProjects, findALlProjecjtsCount } = useProjectService();

  useEffect(() => {
    setPage(0);
    findALlProjecjtsCount(convertDateToISODateTime, reducerData)
      .then((res) => {
        setCount(res.data);
      })
      .catch((ex) => {
        console.log(ex);
      });
  }, [reducerData]);

  const { cursorPointer } = useStyles();

  useEffect(() => {
    if (count) {
      findAllProjects(
        convertDateToISODateTime,
        page,
        rowsPerPage,
        reducerData
      ).then((res) => {
        setRows(res.data);
      });
    } else if (count === 0) {
      setRows([]);
    }
  }, [count, page, rowsPerPage]);

  return (
    <PageableTableContainer
      rows={rows}
      columns={columns}
      count={count}
      page={page}
      setPage={setPage}
      rowsPerPage={rowsPerPage}
      setRowsPerPage={setRowsPerPage}
      rowClickHandler={(row: Issue) =>
        history.push(`${PROJECT_DETAILS_BASE_PATH}/${row.id}`, {
          project: row,
          issue: row,
        })
      }
      rowCssClassName={cursorPointer}
    />
  );
};
