import React from "react";
import { PageableTableColumnProps } from "../../../PageableTable/Container/Container";
import { Project } from "../../../../models/Project";
import { useTimeUtil } from "../../../../utils/useTimeUtil";

const useColumns = () => {
  const { formatDateTime } = useTimeUtil();

  const columns: PageableTableColumnProps[] = [
    {
      name: "Project Name",
      accessor: (row: Project) => <h3>{row.name}</h3>
    },
    {
      name: "Project Description",
      accessor: (row: Project) => <span>{row.description}</span>
    },
    {
      name: "Created At",
      accessor: (row: Project) => (
        <span>{formatDateTime(row.createdAt + "")}</span>
      )
    },
    {
      name: "Last Modified At",
      accessor: (row: Project) => (
        <span>{formatDateTime(row.lastModifiedAt + "")}</span>
      )
    }
  ];

  return {
    columns
  };
};

export default useColumns;
