import "date-fns";
import React, { FC, Dispatch } from "react";
import DateFnsUtils from "@date-io/date-fns";
import {
  ProjectOverviewData,
  ProjectOverviewReducerAction,
  SET_OBJECT,
  emptyInstance_ProjectOverviewData
} from "../TypesAndReducer";
import { Card, Grid, Button } from "@material-ui/core";
import { Formik } from "formik";
import * as Yup from "yup";
import { CustomInput } from "../../../CustomInput/CustomInput";
import { useStyles } from "../../../../hooks/useStyles";
import { useLocalStyles } from "../../useLocalStyles";

import { MuiPickersUtilsProvider } from "@material-ui/pickers";
import CustomDatePicker from "../../../CustomDatePicker/CustomDatePicker";

interface Props {
  reducerData: ProjectOverviewData;
  reducerDispatch: Dispatch<ProjectOverviewReducerAction>;
}

export const ProjectOverviewFilter: FC<Props> = ({
  reducerData,
  reducerDispatch
}) => {
  const { flexDirectionColumn } = useStyles();
  const { margin, marginTop } = useLocalStyles();
  const validationSchema = Yup.object().shape({});

  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <Card className={flexDirectionColumn}>
        <Formik
          enableReinitialize
          validationSchema={validationSchema}
          initialValues={reducerData}
          onSubmit={(values, { setSubmitting }) => {
            console.log(values);
            reducerDispatch({
              type: SET_OBJECT,
              value: { ...values }
            });
            setSubmitting(false);
          }}
          onReset={fields => {
            // formikActions.setValues({});
            //   reducerDispatch({
            //     type: SET_OBJECT,
            //     value: emptyInstance_ProjectOverviewData
            //   });
          }}
        >
          {({
            isSubmitting,
            values,
            touched,
            handleSubmit,
            setFieldValue,
            handleChange,
            handleBlur,
            errors,
            resetForm,
            handleReset
          }) => (
            <form noValidate onSubmit={handleSubmit}>
              <Grid container spacing={3}>
                {/* Issue name */}
                <Grid item xs={4}>
                  <CustomInput
                    name="name"
                    labelName="Project Name"
                    className={margin}
                    value={values.name}
                    handleBlur={handleBlur}
                    handleChange={handleChange}
                  />
                </Grid>

                {/* description */}
                <Grid item xs={4}>
                  <CustomInput
                    name="description"
                    labelName="Project Description"
                    className={margin}
                    value={values.description}
                    handleBlur={handleBlur}
                    handleChange={handleChange}
                  />
                </Grid>

                {/* username */}
                <Grid item xs={4}>
                  <CustomInput
                    name="username"
                    labelName="Username"
                    className={margin}
                    value={values.username}
                    handleBlur={handleBlur}
                    handleChange={handleChange}
                  />
                </Grid>

                {/* firstname */}
                <Grid item xs={4}>
                  <CustomInput
                    name="firstname"
                    labelName="Firstname"
                    className={margin}
                    value={values.firstname}
                    handleBlur={handleBlur}
                    handleChange={handleChange}
                  />
                </Grid>

                {/* lastname */}
                <Grid item xs={4}>
                  <CustomInput
                    name="lastname"
                    labelName="Lastname"
                    className={margin}
                    value={values.lastname}
                    handleBlur={handleBlur}
                    handleChange={handleChange}
                  />
                </Grid>

                {/* createdAtFrom */}
                <Grid item xs={4}>
                  <CustomDatePicker
                    selectedDate={values.createdAtFrom}
                    handleChange={(date: Date | null) =>
                      setFieldValue("createdAtFrom", date)
                    }
                    label="Created At From"
                  />
                </Grid>

                {/* createdAtTo */}
                <Grid item xs={4}>
                  <CustomDatePicker
                    selectedDate={values.createdAtTo}
                    handleChange={(date: Date | null) =>
                      setFieldValue("createdAtTo", date)
                    }
                    label="Created At To"
                  />
                </Grid>

                {/* lastModifiedAtFrom */}
                <Grid item xs={4}>
                  <CustomDatePicker
                    selectedDate={values.lastModifiedAtFrom}
                    handleChange={(date: Date | null) =>
                      setFieldValue("lastModifiedAtFrom", date)
                    }
                    label="Last Modified At From"
                  />
                </Grid>

                {/* lastModifiedAtTo */}
                <Grid item xs={4}>
                  <CustomDatePicker
                    selectedDate={values.lastModifiedAtTo}
                    handleChange={(date: Date | null) =>
                      setFieldValue("lastModifiedAtTo", date)
                    }
                    label="Last Modified At To"
                  />
                </Grid>

                <Grid item xs={10} />

                <Grid item xs={1} className={marginTop}>
                  <Button type="submit" variant="contained" color="primary">
                    Submit
                  </Button>
                </Grid>

                <Grid item xs={1} className={marginTop}>
                  <Button
                    variant="contained"
                    color="secondary"
                    onClick={() => {
                      reducerDispatch({
                        type: SET_OBJECT,
                        value: emptyInstance_ProjectOverviewData
                      });
                    }}
                  >
                    Reset
                  </Button>
                </Grid>
              </Grid>
            </form>
          )}
        </Formik>
      </Card>
    </MuiPickersUtilsProvider>
  );
};
