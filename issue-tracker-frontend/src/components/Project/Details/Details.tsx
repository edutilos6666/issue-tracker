import React, { FC, useState, useEffect, useContext } from "react";
import { Project } from "../../../models/Project";
import { ProjectDetailsHistoryContext } from "../../../context/HistoryContext";
import { useStyles } from "../../../hooks/useStyles";
import { Tabs, Tab, AppBar } from "@material-ui/core";
import { BugReport, People } from "@material-ui/icons";
import { ProjectDetailsTabPanel } from "./TabPanel/TabPanel";
import { IssueOverview } from "../../Issue/Overview/Overview";
import UserOverview from "../../User/Overview/Overview";
import { checkIfCurrentUserInProject } from "../../../services/useCurrentUserService";

interface Props {}

export const ProjectDetails: FC<Props> = () => {
  const { flexDirectionColumn, tabsStyle } = useStyles();
  const { history } = useContext(ProjectDetailsHistoryContext);
  const [project, setProject] = useState<Project>();
  const [tabIndex, setTabIndex] = useState<number>(0);

  const handleTabIndexChange = (event: any, newValue: number) => {
    setTabIndex(newValue);
  };

  const a11yProps = (index: number) => {
    return {
      id: `scrollable-force-tab-${index}`,
      "aria-controls": `scrollable-force-tabpanel-${index}`,
    };
  };

  useEffect(() => {
    if (history.location.state.project) {
      setProject(history.location.state.project);
    }
  }, [history.location.state]);

  useEffect(() => {
    console.log(project);
  }, [project]);

  const [currentUserInProject, setCurrentUserInProject] = useState<boolean>(
    false
  );

  useEffect(() => {
    setCurrentUserInProject(checkIfCurrentUserInProject(project));
  }, [project]);

  return project ? (
    <div className={flexDirectionColumn}>
      <h2>{project!.name}</h2>
      <h5>{project!.description}</h5>

      {project.users && project.users.length > 0 ? (
        <div className={tabsStyle}>
          <AppBar position="static" color="default">
            <Tabs
              value={tabIndex}
              onChange={handleTabIndexChange}
              textColor="primary"
              indicatorColor="primary"
              variant="scrollable"
              scrollButtons="on"
            >
              <Tab icon={<BugReport />} {...a11yProps(0)} />
              <Tab icon={<People />} {...a11yProps(1)} />
            </Tabs>
          </AppBar>
          <ProjectDetailsTabPanel value={tabIndex} index={0}>
            <IssueOverview
              project={project}
              currentUserInProject={currentUserInProject}
            />
          </ProjectDetailsTabPanel>
          <ProjectDetailsTabPanel value={tabIndex} index={1}>
            <UserOverview
              project={project}
              currentUserInProject={currentUserInProject}
            />
          </ProjectDetailsTabPanel>
        </div>
      ) : (
        ""
      )}
    </div>
  ) : (
    <></>
  );
};
