import { Maybe } from "../../../types/Maybe";

export interface ProjectCreateData {
  name?: Maybe<string>;
  description?: Maybe<string>;
  triggerCreate?: Maybe<boolean>;
}

export const emptyInstance_ProjectCreateData: ProjectCreateData = {
  name: undefined,
  description: undefined,
  triggerCreate: undefined,
};

export enum ProjectCreateReducerActionTypes {
  SET_NAME = "setName",
  SET_DESCRIPTION = "setDescription",
  SET_TRIGGER_CREATE = "setTriggerUpdate",
  SET_OBJECT = "SET_OBJECT",
}

export const {
  SET_NAME,
  SET_DESCRIPTION,
  SET_TRIGGER_CREATE,
  SET_OBJECT,
} = ProjectCreateReducerActionTypes;

export type ProjectCreateReducerAction =
  | {
      type:
        | ProjectCreateReducerActionTypes.SET_NAME
        | ProjectCreateReducerActionTypes.SET_DESCRIPTION;
      value: string;
    }
  | {
      type: ProjectCreateReducerActionTypes.SET_TRIGGER_CREATE;
      value: boolean;
    }
  | {
      type: ProjectCreateReducerActionTypes.SET_OBJECT;
      value: ProjectCreateData;
    };

export const ProjectCreateReducer = (
  state: ProjectCreateData,
  action: ProjectCreateReducerAction
): ProjectCreateData => {
  let newValue;
  switch (action.type) {
    case SET_NAME:
      newValue = { ...state, name: action.value };
      break;
    case SET_DESCRIPTION:
      newValue = { ...state, description: action.value };
      break;
    case SET_TRIGGER_CREATE:
      newValue = { ...state, triggerCreate: action.value };
      break;
    case SET_OBJECT:
      newValue = action.value;
      break;
    default:
      newValue = state;
  }
  return newValue;
};
