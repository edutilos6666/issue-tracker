import React, { FC, useReducer, useEffect, useContext } from "react";
import { Paper, Typography, Grid, Button } from "@material-ui/core";
import { Formik } from "formik";
import * as Yup from "yup";
import {
  emptyInstance_ProjectCreateData,
  ProjectCreateReducer,
  SET_OBJECT,
} from "./TypesAndReducer";
import { ProjectCreateHistoryContext } from "../../../context/HistoryContext";
import { CustomInput } from "../../CustomInput/CustomInput";
import { useLocalStyles } from "../useLocalStyles";
import SaveIcon from "@material-ui/icons/Save";
import UndoIcon from "@material-ui/icons/Undo";
import { useProjectService } from "../../../services/useProjectService";

interface Props {}
const ProjectCreate: FC<Props> = () => {
  const { history } = useContext(ProjectCreateHistoryContext);
  const { createProject, testIfProjectNameExists } = useProjectService();

  const validationSchema = Yup.object().shape({
    name: Yup.string()
      .required("Project Name is required.")
      .test("name", "Name already taken", async (value: any) => {
        const exists = (await testIfProjectNameExists(value)).data;
        console.log(value, exists);
        return !exists;
      }),
    description: Yup.string().required("Project Description is required."),
  });
  const [reducerData, reducerDispatch] = useReducer(
    ProjectCreateReducer,
    emptyInstance_ProjectCreateData
  );

  const { paperStyle, margin, spanStyle, centeredDiv } = useLocalStyles();

  useEffect(() => {
    if (
      reducerData.triggerCreate === true ||
      reducerData.triggerCreate === false
    ) {
      createProject({
        name: reducerData.name!,
        description: reducerData.description!,
        createdAt: new Date(),
        lastModifiedAt: new Date(),
        users: [], // I add currentUser in Backend
      }).then((res) => {
        history.goBack();
      });
    }
  }, [reducerData.triggerCreate]);

  return (
    <Paper className={paperStyle}>
      <div className={centeredDiv}>
        <Typography>
          <strong>Create new Project</strong>
        </Typography>
      </div>
      <Formik
        enableReinitialize
        validationSchema={validationSchema}
        initialValues={reducerData}
        onSubmit={(values, { setSubmitting }) => {
          console.log(values);
          reducerDispatch({
            type: SET_OBJECT,
            value: {
              ...values,
              triggerCreate: !reducerData.triggerCreate,
            },
          });
          setSubmitting(false);
        }}
        onReset={(fields) => {
          reducerDispatch({
            type: SET_OBJECT,
            value: emptyInstance_ProjectCreateData,
          });
        }}
      >
        {({
          isSubmitting,
          values,
          touched,
          handleSubmit,
          setFieldValue,
          handleChange,
          handleBlur,
          errors,
          resetForm,
          handleReset,
        }) => (
          <form noValidate onSubmit={handleSubmit}>
            <Grid container spacing={3}>
              {/* project name */}
              <Grid item xs={12}>
                <CustomInput
                  name="name"
                  labelName="Project Name"
                  className={margin}
                  error={errors.name && touched.name ? true : false}
                  value={values.name}
                  handleBlur={handleBlur}
                  handleChange={handleChange}
                />
              </Grid>

              {/* project description */}
              <Grid item xs={12}>
                <CustomInput
                  name="description"
                  labelName="Project Description"
                  className={margin}
                  error={
                    errors.description && touched.description ? true : false
                  }
                  value={values.description}
                  handleBlur={handleBlur}
                  handleChange={handleChange}
                />
              </Grid>

              <Grid item xs={10}></Grid>
              <Grid item xs={1}>
                <Button
                  type="submit"
                  variant="contained"
                  color="primary"
                  // onClick={() => handleSubmit()}
                >
                  <span className={spanStyle}>Submit</span>
                  <SaveIcon />
                </Button>
              </Grid>
              <Grid item xs={1}>
                <Button
                  variant="contained"
                  color="secondary"
                  onClick={() => handleReset()}
                >
                  <span className={spanStyle}>Reset</span>
                  <UndoIcon />
                </Button>
              </Grid>
            </Grid>
          </form>
        )}
      </Formik>
    </Paper>
  );
};
export default ProjectCreate;
