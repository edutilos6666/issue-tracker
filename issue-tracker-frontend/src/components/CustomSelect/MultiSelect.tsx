import React, { FC, useEffect } from "react";
import {
  Select,
  FormControl,
  InputLabel,
  MenuItem,
  Chip
} from "@material-ui/core";
import { Maybe } from "../../types/Maybe";
import { ErrorMessage } from "formik";
import { useStyles } from "../../hooks/useStyles";
import { useLocalStyles } from "./useLocalStyles";

interface Props {
  name: string;
  labelName: string;
  className: string;
  error?: boolean;
  value: Maybe<any>;
  handleBlur: (evt: any) => void;
  handleChange: (evt: any) => void;
  readOnlyMode?: boolean;
  data: any[];
  accessorItemValue: (el: any) => any;
  accessorItemText: (el: any) => string;
}

export const MultiSelect: FC<Props> = ({
  name,
  labelName,
  className,
  error,
  value,
  handleBlur,
  handleChange,
  readOnlyMode,
  data,
  accessorItemText,
  accessorItemValue
}) => {
  const { invalidFeedback } = useStyles();
  useEffect(() => {
    console.log("VALUE = ", value);
  }, [value]);

  const { chips, chip } = useLocalStyles();
  return (
    <FormControl fullWidth className={className} error={error}>
      <InputLabel htmlFor={name}>{labelName}</InputLabel>
      <Select
        name={name}
        id={name}
        value={
          accessorItemValue(value)
            ? accessorItemValue(value)
            : value
            ? value
            : []
        }
        onChange={handleChange}
        readOnly={readOnlyMode}
        multiple
        renderValue={selected => (
          <div className={chips}>
            {(selected as string[]).map(value => (
              <Chip key={value} label={value} className={chip} />
            ))}
          </div>
        )}
      >
        {data &&
          data.map((one, i) => (
            <MenuItem key={i} value={accessorItemValue(one)}>
              {accessorItemText(one)}
            </MenuItem>
          ))}
      </Select>
      <ErrorMessage name={name} component="div" className={invalidFeedback} />
    </FormControl>
  );
};

MultiSelect.defaultProps = {
  error: false,
  readOnlyMode: false
};
