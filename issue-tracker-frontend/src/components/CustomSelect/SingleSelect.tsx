import React, { FC, useEffect } from "react";
import { Select, FormControl, InputLabel, MenuItem } from "@material-ui/core";
import { Maybe } from "../../types/Maybe";
import { ErrorMessage } from "formik";
import { useStyles } from "../../hooks/useStyles";

interface Props {
  name: string;
  labelName: string;
  className: string;
  error?: boolean;
  value: Maybe<any>;
  handleBlur: (evt: any) => void;
  handleChange: (evt: any) => void;
  readOnlyMode?: boolean;
  data: any[];
  accessorItemValue: (el: any) => any;
  accessorItemText: (el: any) => string;
}

export const SingleSelect: FC<Props> = ({
  name,
  labelName,
  className,
  error,
  value,
  handleBlur,
  handleChange,
  readOnlyMode,
  data,
  accessorItemText,
  accessorItemValue
}) => {
  const { invalidFeedback } = useStyles();
  useEffect(() => {
    console.log("VALUE = ", value);
  }, [value]);
  return (
    <FormControl fullWidth className={className} error={error}>
      <InputLabel htmlFor={name}>{labelName}</InputLabel>
      <Select
        name={name}
        id={name}
        value={
          accessorItemValue(value)
            ? accessorItemValue(value)
            : value
            ? value
            : ""
        }
        onChange={handleChange}
        readOnly={readOnlyMode}
      >
        {data &&
          data.map((one, i) => (
            <MenuItem key={i} value={accessorItemValue(one)}>
              {accessorItemText(one)}
            </MenuItem>
          ))}
      </Select>
      <ErrorMessage name={name} component="div" className={invalidFeedback} />
    </FormControl>
  );
};

SingleSelect.defaultProps = {
  error: false,
  readOnlyMode: false
};
