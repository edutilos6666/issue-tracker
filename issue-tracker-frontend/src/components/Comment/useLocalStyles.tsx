import { makeStyles } from "@material-ui/core/styles";
import { green, blue } from "@material-ui/core/colors";

export const useLocalStyles = makeStyles(theme => ({
  greenAvatar: {
    color: "#fff",
    backgroundColor: green[500],
    width: theme.spacing(7),
    height: theme.spacing(7)
  },
  blueAvatar: {
    color: "#fff",
    backgroundColor: blue[500],
    width: theme.spacing(7),
    height: theme.spacing(7)
  },
  margin: {
    margin: theme.spacing(1)
  }
}));
