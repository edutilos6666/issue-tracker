import React, { FC } from "react";
import { Comment } from "../../../models/Comment";
import { useStyles } from "../../../hooks/useStyles";
import {
  Paper,
  Avatar,
  Typography,
  FormControl,
  Input,
  Grid
} from "@material-ui/core";
import { useLocalStyles } from "../useLocalStyles";
import FaceIcon from "@material-ui/icons/Face";
import { useTimeUtil } from "../../../utils/useTimeUtil";

interface Props {
  comment: Comment;
}
const CommentOverviewItem: FC<Props> = ({ comment }) => {
  const {
    flexDirectionRowWithoutFlexGrow,
    flexDirectionColumnWithoutFlexGrow,
    scrollable
  } = useStyles();
  const { blueAvatar } = useLocalStyles();
  const { formatDateTime } = useTimeUtil();

  return (
    <Paper className={flexDirectionRowWithoutFlexGrow}>
      <Grid container spacing={3}>
        <Grid item xs={1}>
          <Avatar className={blueAvatar}>
            <FaceIcon />
          </Avatar>
        </Grid>

        <Grid item xs={11}>
          <div className={scrollable}>
            <Typography>{comment.content}</Typography>
          </div>
        </Grid>

        <Grid item xs={1}>
          <Typography>{comment.user.username}</Typography>
        </Grid>

        <Grid item xs={11}>
          <div className={flexDirectionRowWithoutFlexGrow}>
            <Typography>{formatDateTime(comment.createdAt + "")}</Typography>
          </div>
        </Grid>
      </Grid>
    </Paper>
  );
};
export default CommentOverviewItem;
