import React from "react";
import { Comment } from "../../../models/Comment";
import CommentOverviewItem from "./Item";
import { PageableTableColumnProps } from "../../PageableTable/Container/Container";

const useColumns = () => {
  const columns: PageableTableColumnProps[] = [
    {
      name: "Issue Name",
      accessor: (row: Comment) => <CommentOverviewItem comment={row} />
    }
  ];

  return {
    columns
  };
};

export default useColumns;
