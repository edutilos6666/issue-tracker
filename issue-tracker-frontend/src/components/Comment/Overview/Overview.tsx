import React, { FC, useState, useEffect } from "react";
import { Comment } from "../../../models/Comment";
import { useCommentService } from "../../../services/useCommentService";
import { Issue } from "../../../models/Issue";
import PageableTableContainer from "../../PageableTable/Container/Container";
import useColumns from "./useColumns";
import { Maybe } from "../../../types/Maybe";
import CommentOverviewAddComment from "./AddComment";
interface Props {
  issue: Issue;
  readOnly: boolean;
  triggerUpdate: Maybe<boolean>;
}
const CommentOverview: FC<Props> = ({ issue, readOnly, triggerUpdate }) => {
  const [page, setPage] = useState<number>(0);
  const [rowsPerPage, setRowsPerPage] = useState<number>(5);
  const [rows, setRows] = useState<Comment[]>([]);
  const [count, setCount] = useState<number>(0);

  const { columns } = useColumns();
  const [newComment, setNewComment] = useState<string>();
  const [triggerRefetch, setTriggerRefetch] = useState<boolean>(false);
  const {
    findAllCommentsForIssue,
    findAllCommentsCountForIssue,
    createComment,
  } = useCommentService();

  useEffect(() => {
    setPage(0);
    findAllCommentsCountForIssue(issue.id!).then((res) => {
      setCount(res.data);
    });
  }, [issue, triggerRefetch]);

  useEffect(() => {
    if (count) {
      findAllCommentsForIssue(issue.id!, page, rowsPerPage).then((res) => {
        setRows(res.data);
      });
    } else if (count === 0) {
      setRows([]);
    }
  }, [count, page, rowsPerPage, triggerRefetch]);

  useEffect(() => {
    if (!newComment || newComment.length === 0) return;
    if (triggerUpdate === true || triggerUpdate === false) {
      createComment({
        issueId: issue.id!,
        user: issue.createdBy,
        content: newComment,
        createdAt: new Date(),
      }).then((res) => {
        console.log(res);
        setTriggerRefetch(!triggerRefetch);
      });
    }
  }, [triggerUpdate]);

  if (readOnly) {
    return (
      <PageableTableContainer
        rows={rows}
        columns={columns}
        count={count}
        page={page}
        setPage={setPage}
        rowsPerPage={rowsPerPage}
        setRowsPerPage={setRowsPerPage}
      />
    );
  } else {
    return (
      <CommentOverviewAddComment
        newComment={newComment}
        setNewComment={setNewComment}
      />
    );
  }
};
export default CommentOverview;
