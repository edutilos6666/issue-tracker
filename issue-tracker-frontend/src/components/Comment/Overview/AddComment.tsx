import React, { FC } from "react";
import { CustomInput } from "../../CustomInput/CustomInput";
import { useLocalStyles } from "../useLocalStyles";
interface Props {
  newComment: string | undefined;
  setNewComment: (newComment: string | undefined) => void;
}
const CommentOverviewAddComment: FC<Props> = ({
  newComment,
  setNewComment
}) => {
  const { margin } = useLocalStyles();

  return (
    <CustomInput
      name="newComment"
      labelName="Add Comment"
      className={margin}
      value={newComment}
      handleChange={(
        evt: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>
      ) => setNewComment(evt.currentTarget.value)}
    />
  );
};
export default CommentOverviewAddComment;
