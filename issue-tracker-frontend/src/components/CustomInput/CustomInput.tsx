import React, { FC } from "react";
import {
  FormControl,
  InputLabel,
  Input,
  InputAdornment,
} from "@material-ui/core";
import { Maybe } from "../../types/Maybe";
import { ErrorMessage } from "formik";
import { useStyles } from "../../hooks/useStyles";

interface Props {
  type?: string;
  name: string;
  labelName: string;
  className: string;
  error?: boolean;
  value: Maybe<string>;
  handleBlur?: (
    evt: React.FocusEvent<HTMLTextAreaElement | HTMLInputElement>
  ) => void;
  handleChange?: (
    evt: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>
  ) => void;
  handleKeyPress?: (evt: React.KeyboardEvent<HTMLDivElement>) => void;
  readOnlyMode?: boolean;
  withAdornment?: boolean;
}
export const CustomInput: FC<Props> = ({
  type,
  name,
  labelName,
  className,
  error,
  value,
  handleBlur,
  handleChange,
  readOnlyMode,
  withAdornment,
  handleKeyPress,
}) => {
  const { invalidFeedback } = useStyles();
  return (
    <FormControl fullWidth className={className} error={error}>
      <InputLabel htmlFor={name}>{labelName}</InputLabel>
      <Input
        type={type}
        name={name}
        id={name}
        value={`${value || ""}`}
        onBlur={handleBlur}
        onChange={handleChange}
        onKeyPress={handleKeyPress!}
        startAdornment={
          withAdornment ? (
            <InputAdornment position="start">{labelName + ":"}</InputAdornment>
          ) : (
            ""
          )
        }
        readOnly={readOnlyMode}
      />
      <ErrorMessage name={name} component="div" className={invalidFeedback} />
    </FormControl>
  );
};

CustomInput.defaultProps = {
  type: "text",
  withAdornment: false,
  error: false,
  handleBlur: () => {},
  handleChange: () => {},
  handleKeyPress: () => {},
  readOnlyMode: false,
};
