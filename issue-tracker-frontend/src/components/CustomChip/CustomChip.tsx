import React, { FC, useState, useEffect } from "react";
import { Chip } from "@material-ui/core";
import { getRandomColorFromString, ColorType } from "../../utils/ColorUtil";

interface Props {
  key?: string;
  label: string;
  className: string;
}
export const CustomChip: FC<Props> = ({ key, label, className }) => {
  const [color, setColor] = useState<ColorType>();
  useEffect(() => {
    if (label) setColor(getRandomColorFromString(label));
  }, [label]);
  return <Chip key={key} label={label} className={className} color={color} />;
};

CustomChip.defaultProps = {
  key: ""
};
