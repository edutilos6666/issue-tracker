import React, { FC, useContext } from "react";
import { Route, Redirect, RouteProps } from "react-router";
import Header from "../Header/Header";

interface ProtectedRouteInterface extends RouteProps {
  loggedIn: boolean;
  component: FC<any>; // eslint-disable-line
}
const SecurityProtectedRoute: FC<ProtectedRouteInterface> = ({
  loggedIn,
  component: Component,
  ...rest
}) => {
  return (
    <Route
      {...rest}
      render={(props) =>
        loggedIn === true ? (
          <Header>
            <Component {...props} />
          </Header>
        ) : (
          <Redirect
            to={{
              pathname: "/login",
              state: { from: props.location },
            }}
          />
        )
      }
    />
  );
};
export default SecurityProtectedRoute;
