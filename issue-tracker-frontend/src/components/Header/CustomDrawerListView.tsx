import React from "react";
import {
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Divider,
  Tooltip,
} from "@material-ui/core";
import HomeIcon from "@material-ui/icons/Home";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import ListAltIcon from "@material-ui/icons/ListAlt";
import { useSecurityService } from "../../services/useSecurityService";
import {
  LOCAL_STORAGE_TOKEN,
  LOCAL_STORAGE_SESSION_INFO,
} from "../../utils/StorageConstants";
import { useHistory } from "react-router-dom";
import {
  PROJECT_OVERVIEW_BASE_PATH,
  USER_HOME_BASE_PATH,
} from "../../utils/RouterConstants";

const CustomDrawerListView = () => {
  const { logout } = useSecurityService();
  const history = useHistory();

  const handleBtnUserHomeClick = () => {
    history.push(USER_HOME_BASE_PATH);
  };

  const handleBtnProjectListClick = () => {
    history.push(PROJECT_OVERVIEW_BASE_PATH);
  };

  const handleBtnSignOutClick = () => {
    const token = localStorage.getItem(LOCAL_STORAGE_TOKEN);
    token &&
      logout(token).then((res) => {
        localStorage.removeItem(LOCAL_STORAGE_SESSION_INFO);
        localStorage.removeItem(LOCAL_STORAGE_TOKEN);
        window.location.reload();
      });
  };
  return (
    <>
      <List>
        {/* User Home */}
        <ListItem button onClick={handleBtnUserHomeClick}>
          <ListItemIcon>
            <Tooltip title="User Home" arrow>
              <HomeIcon />
            </Tooltip>
          </ListItemIcon>
          <ListItemText primary="User Home" />
        </ListItem>

        {/* Project List */}
        <ListItem button onClick={handleBtnProjectListClick}>
          <ListItemIcon>
            <Tooltip title="Project List" arrow>
              <ListAltIcon />
            </Tooltip>
          </ListItemIcon>
          <ListItemText primary="Project List" />
        </ListItem>
      </List>
      <Divider />
      <List>
        {/* Sign Out */}
        <ListItem button onClick={handleBtnSignOutClick}>
          <ListItemIcon>
            <Tooltip title="Sign Out" arrow>
              <ExitToAppIcon />
            </Tooltip>
          </ListItemIcon>
          <ListItemText primary="Sign Out" />
        </ListItem>
      </List>
    </>
  );
};

export default CustomDrawerListView;
