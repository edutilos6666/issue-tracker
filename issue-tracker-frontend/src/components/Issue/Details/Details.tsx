import React, { FC, useContext, useEffect, useState, useReducer } from "react";
import { IssueDetailsHistoryContext } from "../../../context/HistoryContext";
import { Project } from "../../../models/Project";
import { Issue } from "../../../models/Issue";
import { useStyles } from "../../../hooks/useStyles";
import {
  IssueDetailsReducer,
  emptyInstance_IssueDetailsData,
  SET_OBJECT,
} from "./TypesAndReducer";
import { Grid, Button, Card } from "@material-ui/core";
import { Formik } from "formik";
import * as Yup from "yup";
import EditIcon from "@material-ui/icons/Edit";
import SaveIcon from "@material-ui/icons/Save";
import { useLocalStyles } from "../useLocalStyles";
import { CustomInput } from "../../CustomInput/CustomInput";
import { useTimeUtil } from "../../../utils/useTimeUtil";
import { User } from "../../../models/User";
import { SingleSelect } from "../../CustomSelect/SingleSelect";
import { useListDataHooks } from "../../../utils/useListDataHook";
import { Priority } from "../../../models/Priority";
import { Severity } from "../../../models/Severity";
import { Status } from "../../../models/Status";
import { useIssueService } from "../../../services/useIssueService";
import CommentOverview from "../../Comment/Overview/Overview";
import { LabelChips } from "../../Label/Chips/Chips";
import { checkIfCurrentUserInProject } from "../../../services/useCurrentUserService";

interface Props {}

const IssueDetails: FC<Props> = () => {
  const { history } = useContext(IssueDetailsHistoryContext);
  const [project, setProject] = useState<Project>();
  const [issue, setIssue] = useState<Issue>();
  const { flexDirectionColumn, floatRight } = useStyles();
  const { margin, spanStyle } = useLocalStyles();
  const [reducerData, reducerDispatch] = useReducer(
    IssueDetailsReducer,
    emptyInstance_IssueDetailsData
  );
  const validationSchema = Yup.object().shape({
    projectName: Yup.string().required("Project Name is required"),
    name: Yup.string()
      .required("Issue Name is required")
      .test("name", "Name already exists", async (value: any) => {
        if (issue && value === issue.name) return true;
        const exists = (await testIfIssueNameExists(value)).data;
        console.log(value, exists);
        return !exists;
      }),
    description: Yup.string().required("Issue Description is required"),
  });

  const [readOnlyMode, setReadonlyMode] = useState<boolean>(true);
  const { formatDateTime } = useTimeUtil();

  const {
    useFindAllStatus,
    useFindAllPriorities,
    useFindAllSeverity,
  } = useListDataHooks();

  const { statusList } = useFindAllStatus();
  const { priorityList } = useFindAllPriorities();
  const { severityList } = useFindAllSeverity();
  const { updateIssue, testIfIssueNameExists } = useIssueService();

  const [currentUserInProject, setCurrentUserInProject] = useState<boolean>(
    false
  );

  useEffect(() => {
    setCurrentUserInProject(checkIfCurrentUserInProject(project));
  }, [project]);

  useEffect(() => {
    if (history.location.state.project) {
      setProject(history.location.state.project);
    }

    if (history.location.state.issue) {
      setIssue(history.location.state.issue);
    }
  }, [history.location.state]);

  useEffect(() => {
    if (project && issue) {
      reducerDispatch({
        type: SET_OBJECT,
        value: {
          projectName: project.name,
          name: issue.name,
          description: issue.description,
          createdAt: issue.createdAt,
          lastModifiedAt: issue.lastModifiedAt,
          createdBy: issue.createdBy,
          assignedTo: issue.assignedTo,
          priority: issue.priority,
          severity: issue.severity,
          status: issue.status,
          labels: issue.labels,
        },
      });
    }
  }, [project, issue]);

  useEffect(() => {
    console.log(reducerData);
    if (
      issue &&
      reducerData &&
      (reducerData.triggerUpdate === true ||
        reducerData.triggerUpdate === false)
    ) {
      updateIssue({
        id: issue.id,
        projectId: issue.projectId,
        name: reducerData.name!,
        description: reducerData.description!,
        createdAt: reducerData.createdAt!,
        lastModifiedAt: reducerData.lastModifiedAt!,
        createdBy: reducerData.createdBy!,
        assignedTo: reducerData.assignedTo!,
        priority: reducerData.priority!,
        severity: reducerData.severity!,
        status: reducerData.status!,
        labels: reducerData.labels!,
      });
    }
  }, [reducerData.triggerUpdate]);

  return (
    <Card className={flexDirectionColumn}>
      <Formik
        enableReinitialize
        validationSchema={validationSchema}
        initialValues={reducerData}
        onSubmit={(values, { setSubmitting }) => {
          console.log(values);
          reducerDispatch({
            type: SET_OBJECT,
            value: {
              ...values,
              assignedTo:
                values.assignedTo && values.assignedTo.id
                  ? values.assignedTo
                  : project && values.assignedTo
                  ? project.users.filter(
                      (one) => one.id === values.assignedTo + ""
                    )[0]
                  : undefined,

              priority:
                values.priority && values.priority.id
                  ? values.priority
                  : values.priority
                  ? priorityList.filter(
                      (one) => one.id === values.priority + ""
                    )[0]
                  : undefined,

              severity:
                values.severity && values.severity.id
                  ? values.severity
                  : values.severity
                  ? severityList.filter(
                      (one) => one.id === values.severity + ""
                    )[0]
                  : undefined,

              status:
                values.status && values.status.id
                  ? values.status
                  : values.status
                  ? statusList.filter((one) => one.id === values.status + "")[0]
                  : undefined,

              lastModifiedAt: new Date(),
              triggerUpdate: !reducerData.triggerUpdate,
            },
          });
          setReadonlyMode(true);
          setSubmitting(false);
        }}
      >
        {({
          isSubmitting,
          values,
          touched,
          handleSubmit,
          setFieldValue,
          handleChange,
          handleBlur,
          errors,
          resetForm,
          handleReset,
          setFieldError,
          setFieldTouched,
        }) => (
          <form noValidate onSubmit={handleSubmit}>
            <Grid container spacing={3}>
              {/* project name */}
              <Grid item xs={12}>
                <CustomInput
                  name="projectName"
                  labelName="Project Name"
                  className={margin}
                  error={
                    errors.projectName && touched.projectName ? true : false
                  }
                  value={values.projectName}
                  handleBlur={handleBlur}
                  handleChange={handleChange}
                  readOnlyMode={true}
                />
              </Grid>

              {/* issue name */}
              <Grid item xs={12}>
                <CustomInput
                  name="name"
                  labelName="Issue Name"
                  className={margin}
                  error={errors.name && touched.name ? true : false}
                  value={values.name}
                  handleBlur={handleBlur}
                  handleChange={handleChange}
                  readOnlyMode={readOnlyMode}
                />
              </Grid>

              {/* issue description */}
              <Grid item xs={12}>
                <CustomInput
                  name="description"
                  labelName="Issue Description"
                  className={margin}
                  error={
                    errors.description && touched.description ? true : false
                  }
                  value={values.description}
                  handleBlur={handleBlur}
                  handleChange={handleChange}
                  readOnlyMode={readOnlyMode}
                />
              </Grid>

              {/* created at */}
              <Grid item xs={12}>
                <CustomInput
                  name="createdAt"
                  labelName="Created At"
                  className={margin}
                  error={errors.createdAt && touched.createdAt ? true : false}
                  value={formatDateTime(values.createdAt + "")}
                  handleBlur={handleBlur}
                  handleChange={handleChange}
                  readOnlyMode={true}
                />
              </Grid>

              {/* last modified at */}
              <Grid item xs={12}>
                <CustomInput
                  name="lastModifiedAt"
                  labelName="Last Modified At"
                  className={margin}
                  error={
                    errors.lastModifiedAt && touched.lastModifiedAt
                      ? true
                      : false
                  }
                  value={formatDateTime(values.lastModifiedAt + "")}
                  handleBlur={handleBlur}
                  handleChange={handleChange}
                  readOnlyMode={true}
                />
              </Grid>

              {/* created by */}
              <Grid item xs={12}>
                <CustomInput
                  name="createdBy"
                  labelName="Created By"
                  className={margin}
                  value={
                    values.createdBy
                      ? `${values.createdBy.firstname} ${values.createdBy.lastname}`
                      : ""
                  }
                  handleBlur={handleBlur}
                  handleChange={handleChange}
                  readOnlyMode={true}
                />
              </Grid>

              {/* assignedTo */}
              <Grid item xs={12}>
                <SingleSelect
                  name="assignedTo"
                  labelName="Assigned To"
                  className={margin}
                  error={errors.assignedTo && touched.assignedTo ? true : false}
                  value={values.assignedTo}
                  handleBlur={handleBlur}
                  handleChange={handleChange}
                  readOnlyMode={readOnlyMode}
                  data={project && project.users ? project.users : []}
                  accessorItemText={(el: User) => {
                    return el ? `${el.firstname} ${el.lastname}` : "";
                  }}
                  accessorItemValue={(el: User) => {
                    return el ? el.id : "";
                  }}
                />
              </Grid>

              {/* priority */}
              <Grid item xs={12}>
                <SingleSelect
                  name="priority"
                  labelName="Priority"
                  className={margin}
                  error={errors.priority && touched.priority ? true : false}
                  value={values.priority}
                  handleBlur={handleBlur}
                  handleChange={handleChange}
                  readOnlyMode={readOnlyMode}
                  data={priorityList}
                  accessorItemText={(el: Priority) => {
                    return el ? el.name! : "";
                  }}
                  accessorItemValue={(el: Priority) => {
                    return el ? el.id : "";
                  }}
                />
              </Grid>

              {/* severity */}
              <Grid item xs={12}>
                <SingleSelect
                  name="severity"
                  labelName="Severity"
                  className={margin}
                  error={errors.severity && touched.severity ? true : false}
                  value={values.severity}
                  handleBlur={handleBlur}
                  handleChange={handleChange}
                  readOnlyMode={readOnlyMode}
                  data={severityList}
                  accessorItemText={(el: Severity) => {
                    return el ? el.name! : "";
                  }}
                  accessorItemValue={(el: Severity) => {
                    return el ? el.id : "";
                  }}
                />
              </Grid>

              {/* status */}
              <Grid item xs={12}>
                <SingleSelect
                  name="status"
                  labelName="Status"
                  className={margin}
                  error={errors.status && touched.status ? true : false}
                  value={values.status}
                  handleBlur={handleBlur}
                  handleChange={handleChange}
                  readOnlyMode={readOnlyMode}
                  data={statusList}
                  accessorItemText={(el: Status) => {
                    return el ? el.name! : "";
                  }}
                  accessorItemValue={(el: Status) => {
                    return el ? el.id : "";
                  }}
                />
              </Grid>

              {/* labels */}
              <Grid item xs={12}>
                <LabelChips
                  readOnly={readOnlyMode}
                  labels={values.labels ? values.labels : []}
                  setFieldValue={setFieldValue}
                />
              </Grid>

              {/* comments */}
              {issue ? (
                <Grid item xs={12}>
                  <CommentOverview
                    issue={issue}
                    readOnly={readOnlyMode}
                    triggerUpdate={reducerData.triggerUpdate}
                  />
                </Grid>
              ) : (
                ""
              )}

              <Grid item xs={10} />

              {readOnlyMode ? (
                <Grid item xs={2}>
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={() => setReadonlyMode(false)}
                    disabled={!currentUserInProject}
                    className={floatRight}
                  >
                    <span className={spanStyle}>Edit Infos</span>
                    <EditIcon />
                  </Button>
                </Grid>
              ) : (
                <Grid item xs={2}>
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={() => handleSubmit()}
                    className={floatRight}
                  >
                    <span className={spanStyle}>Save Changes</span>
                    <SaveIcon />
                  </Button>
                </Grid>
              )}
            </Grid>
          </form>
        )}
      </Formik>
    </Card>
  );
};

export default IssueDetails;
