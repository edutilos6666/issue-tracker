import { Maybe } from "../../../types/Maybe";
import { User } from "../../../models/User";
import { Priority } from "../../../models/Priority";
import { Severity } from "../../../models/Severity";
import { Status } from "../../../models/Status";
import { Label } from "../../../models/Label";
import { Comment } from "../../../models/Comment";

export interface IssueDetailsData {
  projectName?: Maybe<string>;
  name?: Maybe<string>;
  description?: Maybe<string>;
  createdAt?: Maybe<Date>;
  lastModifiedAt?: Maybe<Date>;
  createdBy?: Maybe<User>;
  assignedTo?: Maybe<User>;
  priority?: Maybe<Priority>;
  severity?: Maybe<Severity>;
  status?: Maybe<Status>;
  labels?: Maybe<Label[]>;
  triggerUpdate?: Maybe<boolean>;
}

export const emptyInstance_IssueDetailsData: IssueDetailsData = {
  projectName: undefined,
  name: undefined,
  description: undefined,
  createdAt: undefined,
  lastModifiedAt: undefined,
  createdBy: undefined,
  assignedTo: undefined,
  priority: undefined,
  severity: undefined,
  status: undefined,
  labels: undefined,
  triggerUpdate: undefined,
};

export enum IssueDetailsReducerActionTypes {
  SET_PROJECT_NAME = "setProjectName",
  SET_NAME = "setName",
  SET_DESCRIPTION = "setDescription",
  SET_CREATED_AT = "setCreatedAt",
  SET_LAST_MODIFIED_AT = "setLastModifiedAt",
  SET_CREATED_BY = "setCreatedBy",
  SET_ASSIGNED_TO = "setAssignedTo",
  SET_PRIORITY = "setPriority",
  SET_SEVERITY = "setSeverity",
  SET_STATUS = "setStatus",
  SET_LABELS = "setLabels",
  SET_TRIGGER_UPDATE = "setTriggerUpdate",
  SET_OBJECT = "SET_OBJECT",
}

export const {
  SET_PROJECT_NAME,
  SET_NAME,
  SET_DESCRIPTION,
  SET_CREATED_AT,
  SET_LAST_MODIFIED_AT,
  SET_CREATED_BY,
  SET_ASSIGNED_TO,
  SET_PRIORITY,
  SET_SEVERITY,
  SET_STATUS,
  SET_LABELS,
  SET_TRIGGER_UPDATE,
  SET_OBJECT,
} = IssueDetailsReducerActionTypes;

export type IssueDetailsReducerAction =
  | {
      type:
        | IssueDetailsReducerActionTypes.SET_PROJECT_NAME
        | IssueDetailsReducerActionTypes.SET_NAME
        | IssueDetailsReducerActionTypes.SET_DESCRIPTION;
      value: string;
    }
  | {
      type:
        | IssueDetailsReducerActionTypes.SET_CREATED_AT
        | IssueDetailsReducerActionTypes.SET_LAST_MODIFIED_AT;
      value: Date;
    }
  | {
      type:
        | IssueDetailsReducerActionTypes.SET_CREATED_BY
        | IssueDetailsReducerActionTypes.SET_ASSIGNED_TO;
      value: User;
    }
  | {
      type: IssueDetailsReducerActionTypes.SET_PRIORITY;
      value: Priority;
    }
  | {
      type: IssueDetailsReducerActionTypes.SET_SEVERITY;
      value: Severity;
    }
  | {
      type: IssueDetailsReducerActionTypes.SET_STATUS;
      value: Status;
    }
  | {
      type: IssueDetailsReducerActionTypes.SET_LABELS;
      value: Label[];
    }
  | {
      type: IssueDetailsReducerActionTypes.SET_TRIGGER_UPDATE;
      value: boolean;
    }
  | {
      type: IssueDetailsReducerActionTypes.SET_OBJECT;
      value: IssueDetailsData;
    };

export const IssueDetailsReducer = (
  state: IssueDetailsData,
  action: IssueDetailsReducerAction
): IssueDetailsData => {
  let newValue;

  switch (action.type) {
    case SET_PROJECT_NAME:
      newValue = { ...state, projectName: action.value };
      break;
    case SET_NAME:
      newValue = { ...state, name: action.value };
      break;
    case SET_DESCRIPTION:
      newValue = { ...state, description: action.value };
      break;
    case SET_CREATED_AT:
      newValue = { ...state, createdAt: action.value };
      break;
    case SET_LAST_MODIFIED_AT:
      newValue = { ...state, lastModifiedAt: action.value };
      break;
    case SET_CREATED_BY:
      newValue = { ...state, createdBy: action.value };
      break;
    case SET_ASSIGNED_TO:
      newValue = { ...state, assignedTo: action.value };
      break;
    case SET_PRIORITY:
      newValue = { ...state, priority: action.value };
      break;
    case SET_SEVERITY:
      newValue = { ...state, severity: action.value };
      break;
    case SET_STATUS:
      newValue = { ...state, status: action.value };
      break;
    case SET_LABELS:
      newValue = { ...state, labels: action.value };
      break;
    case SET_TRIGGER_UPDATE:
      newValue = { ...state, triggerUpdate: action.value };
      break;
    case SET_OBJECT:
      newValue = action.value;
      break;

    default:
      newValue = state;
  }
  return newValue;
};
