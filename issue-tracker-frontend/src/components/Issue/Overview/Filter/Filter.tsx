import "date-fns";
import React, { FC, Dispatch } from "react";
import DateFnsUtils from "@date-io/date-fns";
import {
  IssueOverviewData,
  IssueOverviewReducerAction,
  SET_OBJECT,
  emptyInstance_IssueOverviewData,
} from "../TypesAndReducer";
import { Card, Grid, Button } from "@material-ui/core";
import { Formik } from "formik";
import * as Yup from "yup";
import { CustomInput } from "../../../CustomInput/CustomInput";
import { useStyles } from "../../../../hooks/useStyles";
import { useLocalStyles } from "../../useLocalStyles";

import { MuiPickersUtilsProvider } from "@material-ui/pickers";
import CustomDatePicker from "../../../CustomDatePicker/CustomDatePicker";
import { SingleSelect } from "../../../CustomSelect/SingleSelect";
import { Project } from "../../../../models/Project";
import { User } from "../../../../models/User";
import { Priority } from "../../../../models/Priority";
import { MultiSelect } from "../../../CustomSelect/MultiSelect";
import { Status } from "../../../../models/Status";
import { Severity } from "../../../../models/Severity";
import { Label } from "../../../../models/Label";

interface Props {
  project: Project;
  reducerData: IssueOverviewData;
  reducerDispatch: Dispatch<IssueOverviewReducerAction>;
  statusList: Array<Status>;
  priorityList: Array<Priority>;
  severityList: Array<Severity>;
  labelList: Array<Label>;
}

export const IssueOverviewFilter: FC<Props> = ({
  project,
  reducerData,
  reducerDispatch,
  statusList,
  priorityList,
  severityList,
  labelList,
}) => {
  const { flexDirectionColumn } = useStyles();
  const { margin, marginTop } = useLocalStyles();
  const validationSchema = Yup.object().shape({});

  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <Card className={flexDirectionColumn}>
        <Formik
          enableReinitialize
          validationSchema={validationSchema}
          initialValues={reducerData}
          onSubmit={(values, { setSubmitting }) => {
            console.log(values);
            reducerDispatch({
              type: SET_OBJECT,
              value: { ...values },
            });
            setSubmitting(false);
          }}
          onReset={(fields) => {
            // formikActions.setValues({});
            //   reducerDispatch({
            //     type: SET_OBJECT,
            //     value: emptyInstance_IssueOverviewData
            //   });
          }}
        >
          {({
            isSubmitting,
            values,
            touched,
            handleSubmit,
            setFieldValue,
            handleChange,
            handleBlur,
            errors,
            resetForm,
            handleReset,
          }) => (
            <form noValidate onSubmit={handleSubmit}>
              <Grid container spacing={3}>
                {/* Issue name */}
                <Grid item xs={3}>
                  <CustomInput
                    name="name"
                    labelName="Issue Name"
                    className={margin}
                    value={values.name}
                    handleBlur={handleBlur}
                    handleChange={handleChange}
                  />
                </Grid>

                {/* description */}
                <Grid item xs={3}>
                  <CustomInput
                    name="description"
                    labelName="Issue Description"
                    className={margin}
                    value={values.description}
                    handleBlur={handleBlur}
                    handleChange={handleChange}
                  />
                </Grid>

                {/* createdAtFrom */}
                <Grid item xs={3}>
                  <CustomDatePicker
                    selectedDate={values.createdAtFrom}
                    handleChange={(date: Date | null) =>
                      setFieldValue("createdAtFrom", date)
                    }
                    label="Created At From"
                  />
                </Grid>

                {/* createdAtTo */}
                <Grid item xs={3}>
                  <CustomDatePicker
                    selectedDate={values.createdAtTo}
                    handleChange={(date: Date | null) =>
                      setFieldValue("createdAtTo", date)
                    }
                    label="Created At To"
                  />
                </Grid>

                {/* createdById */}
                <Grid item xs={3}>
                  <SingleSelect
                    name="createdById"
                    labelName="Created By"
                    className={margin}
                    value={values.createdById}
                    handleBlur={handleBlur}
                    handleChange={handleChange}
                    data={project.users}
                    accessorItemText={(el: User) => {
                      return el ? el.username! : "";
                    }}
                    accessorItemValue={(el: User) => {
                      return el ? el.id : "";
                    }}
                  />
                </Grid>

                {/* assignedToId */}
                <Grid item xs={3}>
                  <SingleSelect
                    name="assignedToId"
                    labelName="Assigned To"
                    className={margin}
                    value={values.assignedToId}
                    handleBlur={handleBlur}
                    handleChange={handleChange}
                    data={project.users}
                    accessorItemText={(el: User) => {
                      return el ? el.username! : "";
                    }}
                    accessorItemValue={(el: User) => {
                      return el ? el.id : "";
                    }}
                  />
                </Grid>

                {/* lastModifiedAtFrom */}
                <Grid item xs={3}>
                  <CustomDatePicker
                    selectedDate={values.lastModifiedAtFrom}
                    handleChange={(date: Date | null) =>
                      setFieldValue("lastModifiedAtFrom", date)
                    }
                    label="Last Modified At From"
                  />
                </Grid>

                {/* lastModifiedAtTo */}
                <Grid item xs={3}>
                  <CustomDatePicker
                    selectedDate={values.lastModifiedAtTo}
                    handleChange={(date: Date | null) =>
                      setFieldValue("lastModifiedAtTo", date)
                    }
                    label="Last Modified At To"
                  />
                </Grid>

                {/* statusList */}
                <Grid item xs={3}>
                  <MultiSelect
                    name="statusList"
                    labelName="Status List"
                    className={margin}
                    value={values.statusList}
                    handleBlur={handleBlur}
                    handleChange={handleChange}
                    data={statusList}
                    accessorItemText={(el: Status) => {
                      return el ? el.name! : "";
                    }}
                    accessorItemValue={(el: Status) => {
                      return el ? el.id : "";
                    }}
                  />
                </Grid>

                {/* labelList */}
                <Grid item xs={3}>
                  <MultiSelect
                    name="labelList"
                    labelName="Label List"
                    className={margin}
                    value={values.labelList}
                    handleBlur={handleBlur}
                    handleChange={handleChange}
                    data={labelList}
                    accessorItemText={(el: Label) => {
                      return el ? el.name! : "";
                    }}
                    accessorItemValue={(el: Label) => {
                      return el ? el.id : "";
                    }}
                  />
                </Grid>

                {/* priorityList */}
                <Grid item xs={3}>
                  <MultiSelect
                    name="priorityList"
                    labelName="Priority List"
                    className={margin}
                    value={values.priorityList}
                    handleBlur={handleBlur}
                    handleChange={handleChange}
                    data={priorityList}
                    accessorItemText={(el: Priority) => {
                      return el ? el.name! : "";
                    }}
                    accessorItemValue={(el: Priority) => {
                      return el ? el.id : "";
                    }}
                  />
                </Grid>

                {/* severityList */}
                <Grid item xs={3}>
                  <MultiSelect
                    name="severityList"
                    labelName="Severity List"
                    className={margin}
                    value={values.severityList}
                    handleBlur={handleBlur}
                    handleChange={handleChange}
                    data={severityList}
                    accessorItemText={(el: Severity) => {
                      return el ? el.name! : "";
                    }}
                    accessorItemValue={(el: Severity) => {
                      return el ? el.id : "";
                    }}
                  />
                </Grid>

                <Grid item xs={1} className={marginTop}>
                  <Button type="submit" variant="contained" color="primary">
                    Submit
                  </Button>
                </Grid>

                <Grid item xs={2} className={marginTop}>
                  <Button
                    variant="contained"
                    color="secondary"
                    onClick={() => {
                      reducerDispatch({
                        type: SET_OBJECT,
                        value: emptyInstance_IssueOverviewData,
                      });
                    }}
                  >
                    Clear Filters
                  </Button>
                </Grid>
              </Grid>
            </form>
          )}
        </Formik>
      </Card>
    </MuiPickersUtilsProvider>
  );
};
