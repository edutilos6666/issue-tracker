import React, { FC, useReducer, useState } from "react";
import { Project } from "../../../models/Project";
import { useStyles } from "../../../hooks/useStyles";
import {
  Button,
  ExpansionPanel,
  ExpansionPanelSummary,
  Typography,
  ExpansionPanelDetails,
} from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import { useLocalStyles } from "../useLocalStyles";
import { IssueOverviewTable } from "./Table/Table";
import {
  IssueOverviewReducer,
  emptyInstance_IssueOverviewData,
} from "./TypesAndReducer";
import { IssueOverviewFilter } from "./Filter/Filter";
import { useListDataHooks } from "../../../utils/useListDataHook";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { ISSUE_CREATE_BASE_PATH } from "../../../utils/RouterConstants";
import { useHistory } from "react-router";

interface Props {
  project: Project;
  currentUserInProject: boolean;
}

export const IssueOverview: FC<Props> = ({ project, currentUserInProject }) => {
  const history = useHistory();
  const { flexDirectionColumn, buttonStyle } = useStyles();
  const { spanStyle, heading } = useLocalStyles();
  const [reducerData, reducerDispatch] = useReducer(
    IssueOverviewReducer,
    emptyInstance_IssueOverviewData
  );

  const {
    useFindAllStatus,
    useFindAllPriorities,
    useFindAllSeverity,
    useFindAllLabels,
  } = useListDataHooks();

  const { statusList } = useFindAllStatus();
  const { priorityList } = useFindAllPriorities();
  const { severityList } = useFindAllSeverity();
  const { labelList } = useFindAllLabels();

  const [expanded, setExpanded] = useState<boolean>(true);

  const handleCreateNewIssue = () => {
    history.push(`${ISSUE_CREATE_BASE_PATH}/${project.id}`, {
      project: project,
    });
  };

  return (
    <div className={flexDirectionColumn}>
      <ExpansionPanel
        expanded={expanded}
        onChange={() => setExpanded(!expanded)}
      >
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel-filter-content"
          id="panel-filter-header"
        >
          <Typography className={heading}>
            <strong>Filter</strong>
          </Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <IssueOverviewFilter
            project={project}
            reducerData={reducerData}
            reducerDispatch={reducerDispatch}
            statusList={statusList}
            priorityList={priorityList}
            severityList={severityList}
            labelList={labelList}
          />
        </ExpansionPanelDetails>
      </ExpansionPanel>

      <IssueOverviewTable project={project} reducerData={reducerData} />
      <Button
        className={buttonStyle}
        variant="contained"
        color="primary"
        onClick={handleCreateNewIssue}
        disabled={!currentUserInProject}
      >
        <span className={spanStyle}>Create New Issue</span>
        <AddIcon />
      </Button>
    </div>
  );
};
