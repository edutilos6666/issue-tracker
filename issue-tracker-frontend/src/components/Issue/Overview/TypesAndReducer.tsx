import { Maybe } from "../../../types/Maybe";

export interface IssueOverviewData {
  name?: Maybe<string>;
  description?: Maybe<string>;
  createdAtFrom?: Maybe<Date>;
  createdAtTo?: Maybe<Date>;
  lastModifiedAtFrom?: Maybe<Date>;
  lastModifiedAtTo?: Maybe<Date>;
  createdById?: Maybe<string>;
  assignedToId?: Maybe<string>;
  priorityList?: Maybe<string[]>;
  severityList?: Maybe<string[]>;
  statusList?: Maybe<string[]>;
  labelList?: Maybe<string[]>;
}

export const emptyInstance_IssueOverviewData: IssueOverviewData = {
  name: undefined,
  description: undefined,
  createdAtFrom: null,
  createdAtTo: null,
  lastModifiedAtFrom: null,
  lastModifiedAtTo: null,
  createdById: undefined,
  assignedToId: undefined,
  priorityList: undefined,
  severityList: undefined,
  statusList: undefined,
  labelList: undefined,
};

export enum IssueOverviewReducerActionTypes {
  SET_NAME = "setName",
  SET_DESCRIPTION = "setDescription",
  SET_CREATEDATFROM = "setCreatedAtFrom",
  SET_CREATEDATTO = "setCreatedAtTo",
  SET_LAST_MODIFED_AT_FROM = "setLastModifiedAtFrom",
  SET_LAST_MODIFED_AT_TO = "setLastModifiedAtTo",
  SET_CREATEDBYID = "setCreatedById",
  SET_ASSIGNEDTOID = "setAssignedToId",
  SET_PRIORITYLIST = "setPriorityList",
  SET_SEVERITYLIST = "setSeverityList",
  SET_STATUSLIST = "setStatusList",
  SET_LABELLIST = "setLabelList",
  SET_OBJECT = "SET_OBJECT",
}

export const {
  SET_NAME,
  SET_DESCRIPTION,
  SET_CREATEDATFROM,
  SET_CREATEDATTO,
  SET_LAST_MODIFED_AT_FROM,
  SET_LAST_MODIFED_AT_TO,
  SET_CREATEDBYID,
  SET_ASSIGNEDTOID,
  SET_PRIORITYLIST,
  SET_SEVERITYLIST,
  SET_STATUSLIST,
  SET_LABELLIST,
  SET_OBJECT,
} = IssueOverviewReducerActionTypes;

export type IssueOverviewReducerAction =
  | {
      type:
        | IssueOverviewReducerActionTypes.SET_NAME
        | IssueOverviewReducerActionTypes.SET_DESCRIPTION
        | IssueOverviewReducerActionTypes.SET_CREATEDBYID
        | IssueOverviewReducerActionTypes.SET_ASSIGNEDTOID;
      value: string;
    }
  | {
      type:
        | IssueOverviewReducerActionTypes.SET_CREATEDATFROM
        | IssueOverviewReducerActionTypes.SET_CREATEDATTO
        | IssueOverviewReducerActionTypes.SET_LAST_MODIFED_AT_FROM
        | IssueOverviewReducerActionTypes.SET_LAST_MODIFED_AT_TO;
      value: Date;
    }
  | {
      type:
        | IssueOverviewReducerActionTypes.SET_PRIORITYLIST
        | IssueOverviewReducerActionTypes.SET_SEVERITYLIST
        | IssueOverviewReducerActionTypes.SET_STATUSLIST
        | IssueOverviewReducerActionTypes.SET_LABELLIST;
      value: string[];
    }
  | {
      type: IssueOverviewReducerActionTypes.SET_OBJECT;
      value: IssueOverviewData;
    };

export const IssueOverviewReducer = (
  state: IssueOverviewData,
  action: IssueOverviewReducerAction
): IssueOverviewData => {
  let newValue;
  switch (action.type) {
    case SET_NAME:
      newValue = { ...state, name: action.value };
      break;
    case SET_DESCRIPTION:
      newValue = { ...state, description: action.value };
      break;
    case SET_CREATEDATFROM:
      newValue = { ...state, createdAtFrom: action.value };
      break;
    case SET_CREATEDATTO:
      newValue = { ...state, createdAtTo: action.value };
      break;
    case SET_LAST_MODIFED_AT_FROM:
      newValue = { ...state, lastModifiedAtFrom: action.value };
      break;
    case SET_LAST_MODIFED_AT_TO:
      newValue = { ...state, lastModifiedAtTo: action.value };
      break;
    case SET_CREATEDBYID:
      newValue = { ...state, createdById: action.value };
      break;
    case SET_ASSIGNEDTOID:
      newValue = { ...state, assignedToId: action.value };
      break;
    case SET_PRIORITYLIST:
      newValue = { ...state, priorityList: action.value };
      break;
    case SET_SEVERITYLIST:
      newValue = { ...state, severityList: action.value };
      break;
    case SET_STATUSLIST:
      newValue = { ...state, statusList: action.value };
      break;
    case SET_LABELLIST:
      newValue = { ...state, labelList: action.value };
      break;
    case SET_OBJECT:
      newValue = action.value;
      break;
    default:
      newValue = state;
  }

  return newValue;
};
