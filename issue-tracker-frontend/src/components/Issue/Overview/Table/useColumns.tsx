import React from "react";
import { PageableTableColumnProps } from "../../../PageableTable/Container/Container";
import { Issue } from "../../../../models/Issue";
import { Label } from "../../../../models/Label";
import { Chip } from "@material-ui/core";
import { useTimeUtil } from "../../../../utils/useTimeUtil";
import { useLocalStyles } from "../../useLocalStyles";

const useColumns = () => {
  const { spanStyle } = useLocalStyles();
  const concatLabels = (labels: Label[]): any => {
    return (
      <div>
        {labels.map((one, i) => (
          <Chip
            key={i}
            label={one.name}
            color="primary"
            className={spanStyle}
          />
        ))}
      </div>
    );
  };

  const { formatDateTime } = useTimeUtil();

  const columns: PageableTableColumnProps[] = [
    {
      name: "Issue Name",
      accessor: (row: Issue) => <h3>{row.name}</h3>,
    },
    {
      name: "Issue Description",
      accessor: (row: Issue) => <span>{row.description}</span>,
    },
    {
      name: "Priority",
      accessor: (row: Issue) => <span>{row.priority.name}</span>,
    },
    {
      name: "Severity",
      accessor: (row: Issue) => <span>{row.severity.name}</span>,
    },
    {
      name: "Status",
      accessor: (row: Issue) => <span>{row.status.name}</span>,
    },
    {
      name: "Labels",
      accessor: (row: Issue) => concatLabels(row.labels),
    },
    {
      name: "Last Modified At",
      accessor: (row: Issue) => (
        <span>{formatDateTime(row.lastModifiedAt + "")}</span>
      ),
    },
  ];

  return {
    columns,
  };
};

export default useColumns;
