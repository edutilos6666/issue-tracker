import React, { FC, useState, useEffect } from "react";
import PageableTableContainer from "../../../PageableTable/Container/Container";
import { Issue } from "../../../../models/Issue";
import useColumns from "./useColumns";
import { useTimeUtil } from "../../../../utils/useTimeUtil";
import { useIssueService } from "../../../../services/useIssueService";
import { Project } from "../../../../models/Project";
import { IssueOverviewData } from "../TypesAndReducer";
import { useStyles } from "../../../../hooks/useStyles";
import { useHistory } from "react-router";

interface Props {
  project: Project;
  reducerData: IssueOverviewData;
}

export const IssueOverviewTable: FC<Props> = ({ project, reducerData }) => {
  const [page, setPage] = useState<number>(0);
  const [rowsPerPage, setRowsPerPage] = useState<number>(5);
  const [rows, setRows] = useState<Issue[]>([]);
  const [count, setCount] = useState<number>(0);

  const { columns } = useColumns();
  const { convertDateToISODateTime } = useTimeUtil();

  const history = useHistory();
  const { cursorPointer } = useStyles();
  const { findAllIssuesCountForProject, findAllIssues } = useIssueService();

  useEffect(() => {
    setPage(0);
    findAllIssuesCountForProject(
      convertDateToISODateTime,
      project.id!,
      reducerData
    ).then((res) => {
      setCount(res.data);
    });
  }, [project, reducerData]);

  useEffect(() => {
    if (count) {
      findAllIssues(
        convertDateToISODateTime,
        project.id!,
        page,
        rowsPerPage,
        reducerData
      ).then((res) => {
        setRows(res.data);
      });
    } else if (count === 0) {
      setRows([]);
    }
  }, [count, page, rowsPerPage]);

  return (
    <PageableTableContainer
      rows={rows}
      columns={columns}
      count={count}
      page={page}
      setPage={setPage}
      rowsPerPage={rowsPerPage}
      setRowsPerPage={setRowsPerPage}
      rowClickHandler={(row: Issue) =>
        history.push(`/issue-details/${row.id}`, {
          project: project,
          issue: row,
        })
      }
      rowCssClassName={cursorPointer}
    />
  );
};
