import { makeStyles } from "@material-ui/core/styles";

export const useLocalStyles = makeStyles(theme => ({
  paperStyle: {
    margin: theme.spacing(5),
    padding: theme.spacing(5)
  },
  margin: {
    margin: theme.spacing(1)
  },
  spanStyle: {
    marginRight: theme.spacing(2)
  },
  marginTop: {
    marginTop: theme.spacing(3)
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular
  },
  centeredDiv: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  }
}));
