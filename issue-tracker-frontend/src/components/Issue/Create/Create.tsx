import React, { FC, useReducer, useState, useEffect, useContext } from "react";
import { Paper, Typography, Grid, Button } from "@material-ui/core";
import { Formik } from "formik";
import * as Yup from "yup";
import {
  emptyInstance_IssueCreateData,
  IssueCreateReducer,
  SET_OBJECT,
} from "./TypesAndReducer";
import { Project } from "../../../models/Project";
import { useListDataHooks } from "../../../utils/useListDataHook";
import { IssueCreateHistoryContext } from "../../../context/HistoryContext";
import { CustomInput } from "../../CustomInput/CustomInput";
import { useLocalStyles } from "../useLocalStyles";
import { SingleSelect } from "../../CustomSelect/SingleSelect";
import { User } from "../../../models/User";
import { Priority } from "../../../models/Priority";
import { Severity } from "../../../models/Severity";
import { Status } from "../../../models/Status";
import { LabelChips } from "../../Label/Chips/Chips";
import SaveIcon from "@material-ui/icons/Save";
import UndoIcon from "@material-ui/icons/Undo";
import { useIssueService } from "../../../services/useIssueService";

interface Props {}
const IssueCreate: FC<Props> = () => {
  const [project, setProject] = useState<Project>();
  const { history } = useContext(IssueCreateHistoryContext);

  const { createIssue, testIfIssueNameExists } = useIssueService();

  const validationSchema = Yup.object().shape({
    name: Yup.string()
      .required("Issue Name is required.")
      .test("name", "Name already exists", async (value: any) => {
        const exists = (await testIfIssueNameExists(value)).data;
        console.log(value, exists);
        return !exists;
      }),
    description: Yup.string().required("Issue Description is required."),
    // here assignedTo, priority, severity and status had to be objects,
    // but because of issue in <SingleSelect /> i can only get id of Objects
    // => so workaround to use Yup.string() instead of Yup.object()[.nullable()]
    assignedTo: Yup.string().required("Issue AssignedTo is required."),
    priority: Yup.string().required("Issue Priority is required.").nullable(),
    severity: Yup.string().required("Issue Severity is required."),
    status: Yup.string().required("Issue Status is required."),
  });
  const [reducerData, reducerDispatch] = useReducer(
    IssueCreateReducer,
    emptyInstance_IssueCreateData
  );

  const {
    useFindAllStatus,
    useFindAllPriorities,
    useFindAllSeverity,
  } = useListDataHooks();

  const { statusList } = useFindAllStatus();
  const { priorityList } = useFindAllPriorities();
  const { severityList } = useFindAllSeverity();

  const { paperStyle, margin, spanStyle, centeredDiv } = useLocalStyles();

  useEffect(() => {
    if (history.location.state.project) {
      setProject(history.location.state.project);
    }
  }, [history.location.state]);

  useEffect(() => {
    if (
      project &&
      (reducerData.triggerCreate === true ||
        reducerData.triggerCreate === false)
    ) {
      createIssue({
        projectId: project.id!,
        name: reducerData.name!,
        description: reducerData.description!,
        createdAt: new Date(),
        lastModifiedAt: new Date(),
        // TODO: createdBy => currentUser
        createdBy: project.users[0],
        assignedTo: reducerData.assignedTo!,
        priority: reducerData.priority!,
        severity: reducerData.severity!,
        status: reducerData.status!,
        labels: reducerData.labels ? reducerData.labels : [],
      }).then((res) => {
        history.goBack();
        // history.push(`${PROJECT_DETAILS_BASE_PATH}/${project.id}`);
      });
    }
  }, [reducerData.triggerCreate]);

  return (
    <Paper className={paperStyle}>
      <div className={centeredDiv}>
        <Typography>
          <strong>Create new Issue</strong>
        </Typography>
      </div>
      <Formik
        enableReinitialize
        validationSchema={validationSchema}
        initialValues={reducerData}
        onSubmit={(values, { setSubmitting }) => {
          console.log(values);
          reducerDispatch({
            type: SET_OBJECT,
            value: {
              ...values,
              assignedTo:
                values.assignedTo && values.assignedTo.id
                  ? values.assignedTo
                  : project && values.assignedTo
                  ? project.users.filter(
                      (one) => one.id === values.assignedTo + ""
                    )[0]
                  : undefined,

              priority:
                values.priority && values.priority.id
                  ? values.priority
                  : values.priority
                  ? priorityList.filter(
                      (one) => one.id === values.priority + ""
                    )[0]
                  : undefined,

              severity:
                values.severity && values.severity.id
                  ? values.severity
                  : values.severity
                  ? severityList.filter(
                      (one) => one.id === values.severity + ""
                    )[0]
                  : undefined,

              status:
                values.status && values.status.id
                  ? values.status
                  : values.status
                  ? statusList.filter((one) => one.id === values.status + "")[0]
                  : undefined,

              triggerCreate: !reducerData.triggerCreate,
            },
          });
          setSubmitting(false);
        }}
        onReset={(fields) => {
          reducerDispatch({
            type: SET_OBJECT,
            value: emptyInstance_IssueCreateData,
          });
        }}
      >
        {({
          isSubmitting,
          values,
          touched,
          handleSubmit,
          setFieldValue,
          handleChange,
          handleBlur,
          errors,
          resetForm,
          handleReset,
        }) => (
          <form noValidate onSubmit={handleSubmit}>
            <Grid container spacing={3}>
              {/* issue name */}
              <Grid item xs={12}>
                <CustomInput
                  name="name"
                  labelName="Issue Name"
                  className={margin}
                  error={errors.name && touched.name ? true : false}
                  value={values.name}
                  handleBlur={handleBlur}
                  handleChange={handleChange}
                />
              </Grid>

              {/* issue description */}
              <Grid item xs={12}>
                <CustomInput
                  name="description"
                  labelName="Issue Description"
                  className={margin}
                  error={
                    errors.description && touched.description ? true : false
                  }
                  value={values.description}
                  handleBlur={handleBlur}
                  handleChange={handleChange}
                />
              </Grid>

              {/* assignedTo */}
              <Grid item xs={12}>
                <SingleSelect
                  name="assignedTo"
                  labelName="Assigned To"
                  className={margin}
                  error={errors.assignedTo && touched.assignedTo ? true : false}
                  value={values.assignedTo}
                  handleBlur={handleBlur}
                  handleChange={handleChange}
                  data={project && project.users ? project.users : []}
                  accessorItemText={(el: User) => {
                    return el ? `${el.firstname} ${el.lastname}` : "";
                  }}
                  accessorItemValue={(el: User) => {
                    return el ? el.id : "";
                  }}
                />
              </Grid>

              {/* priority */}
              <Grid item xs={12}>
                <SingleSelect
                  name="priority"
                  labelName="Priority"
                  className={margin}
                  error={errors.priority && touched.priority ? true : false}
                  value={values.priority}
                  handleBlur={handleBlur}
                  handleChange={handleChange}
                  data={priorityList}
                  accessorItemText={(el: Priority) => {
                    return el ? el.name! : "";
                  }}
                  accessorItemValue={(el: Priority) => {
                    return el ? el.id : "";
                  }}
                />
              </Grid>

              {/* severity */}
              <Grid item xs={12}>
                <SingleSelect
                  name="severity"
                  labelName="Severity"
                  className={margin}
                  error={errors.severity && touched.severity ? true : false}
                  value={values.severity}
                  handleBlur={handleBlur}
                  handleChange={handleChange}
                  data={severityList}
                  accessorItemText={(el: Severity) => {
                    return el ? el.name! : "";
                  }}
                  accessorItemValue={(el: Severity) => {
                    return el ? el.id : "";
                  }}
                />
              </Grid>

              {/* status */}
              <Grid item xs={12}>
                <SingleSelect
                  name="status"
                  labelName="Status"
                  className={margin}
                  error={errors.status && touched.status ? true : false}
                  value={values.status}
                  handleBlur={handleBlur}
                  handleChange={handleChange}
                  data={statusList}
                  accessorItemText={(el: Status) => {
                    return el ? el.name! : "";
                  }}
                  accessorItemValue={(el: Status) => {
                    return el ? el.id : "";
                  }}
                />
              </Grid>

              {/* labels */}
              <Grid item xs={12}>
                <LabelChips
                  labels={values.labels ? values.labels : []}
                  setFieldValue={setFieldValue}
                />
              </Grid>

              <Grid item xs={10}></Grid>
              <Grid item xs={1}>
                <Button
                  // type="submit" -> i use enter to add new label, so i can not use type="submit" here.
                  variant="contained"
                  color="primary"
                  onClick={() => handleSubmit()}
                >
                  <span className={spanStyle}>Submit</span>
                  <SaveIcon />
                </Button>
              </Grid>
              <Grid item xs={1}>
                <Button
                  variant="contained"
                  color="secondary"
                  onClick={() => handleReset()}
                >
                  <span className={spanStyle}>Reset</span>
                  <UndoIcon />
                </Button>
              </Grid>
            </Grid>
          </form>
        )}
      </Formik>
    </Paper>
  );
};
export default IssueCreate;
