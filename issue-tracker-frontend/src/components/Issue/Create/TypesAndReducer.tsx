import { Maybe } from "../../../types/Maybe";
import { User } from "../../../models/User";
import { Priority } from "../../../models/Priority";
import { Severity } from "../../../models/Severity";
import { Status } from "../../../models/Status";
import { Label } from "../../../models/Label";

export interface IssueCreateData {
  name?: Maybe<string>;
  description?: Maybe<string>;
  assignedTo?: Maybe<User>;
  priority?: Maybe<Priority>;
  severity?: Maybe<Severity>;
  status?: Maybe<Status>;
  labels?: Maybe<Array<Label>>;
  triggerCreate?: Maybe<boolean>;
}

export const emptyInstance_IssueCreateData: IssueCreateData = {
  name: undefined,
  description: undefined,
  assignedTo: undefined,
  priority: undefined,
  severity: undefined,
  status: undefined,
  labels: undefined,
  triggerCreate: undefined
};

export enum IssueCreateReducerActionTypes {
  SET_NAME = "setName",
  SET_DESCRIPTION = "setDescription",
  SET_ASSIGNED_TO = "setAssignedTo",
  SET_PRIORITY = "setPriority",
  SET_SEVERITY = "setSeverity",
  SET_STATUS = "setStatus",
  SET_LABELS = "setLabels",
  SET_TRIGGER_CREATE = "setTriggerUpdate",
  SET_OBJECT = "SET_OBJECT"
}

export const {
  SET_NAME,
  SET_DESCRIPTION,
  SET_ASSIGNED_TO: SET_ASSIGNEDTO,
  SET_PRIORITY,
  SET_SEVERITY,
  SET_STATUS,
  SET_LABELS,
  SET_TRIGGER_CREATE,
  SET_OBJECT
} = IssueCreateReducerActionTypes;

export type IssueCreateReducerAction =
  | {
      type:
        | IssueCreateReducerActionTypes.SET_NAME
        | IssueCreateReducerActionTypes.SET_DESCRIPTION;
      value: string;
    }
  | {
      type: IssueCreateReducerActionTypes.SET_ASSIGNED_TO;
      value: User;
    }
  | {
      type: IssueCreateReducerActionTypes.SET_PRIORITY;
      value: Priority;
    }
  | {
      type: IssueCreateReducerActionTypes.SET_SEVERITY;
      value: Severity;
    }
  | {
      type: IssueCreateReducerActionTypes.SET_STATUS;
      value: Status;
    }
  | {
      type: IssueCreateReducerActionTypes.SET_LABELS;
      value: Label[];
    }
  | {
      type: IssueCreateReducerActionTypes.SET_TRIGGER_CREATE;
      value: boolean;
    }
  | {
      type: IssueCreateReducerActionTypes.SET_OBJECT;
      value: IssueCreateData;
    };

export const IssueCreateReducer = (
  state: IssueCreateData,
  action: IssueCreateReducerAction
): IssueCreateData => {
  let newValue;
  switch (action.type) {
    case SET_NAME:
      newValue = { ...state, name: action.value };
      break;
    case SET_DESCRIPTION:
      newValue = { ...state, description: action.value };
      break;
    case SET_ASSIGNEDTO:
      newValue = { ...state, assignedTo: action.value };
      break;
    case SET_PRIORITY:
      newValue = { ...state, priority: action.value };
      break;
    case SET_SEVERITY:
      newValue = { ...state, severity: action.value };
      break;
    case SET_STATUS:
      newValue = { ...state, status: action.value };
      break;
    case SET_LABELS:
      newValue = { ...state, labels: action.value };
      break;
    case SET_TRIGGER_CREATE:
      newValue = { ...state, triggerCreate: action.value };
      break;
    case SET_OBJECT:
      newValue = action.value;
      break;
    default:
      newValue = state;
  }
  return newValue;
};
