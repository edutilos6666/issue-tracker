import "date-fns";
import React, { FC } from "react";
import Grid from "@material-ui/core/Grid";
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker
} from "@material-ui/pickers";
import { Maybe } from "../../types/Maybe";
import { useLocalStyles } from "./useLocalStyles";

export interface CustomDatePickerProps {
  selectedDate: Maybe<Date>;
  handleChange: (date: Date | null) => void;
  label: string;
  format?: string;
  disableToolbar?: boolean;
}

const CustomDatePicker: FC<CustomDatePickerProps> = ({
  selectedDate,
  handleChange,
  label,
  format,
  disableToolbar
}) => {
  const { customDatePicker } = useLocalStyles();
  return (
    // <Grid container justify="space-around">
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <KeyboardDatePicker
        disableToolbar={disableToolbar}
        className={customDatePicker}
        // margin="dense"
        id="not-important"
        label={label}
        format={format}
        value={selectedDate}
        onChange={handleChange}
        KeyboardButtonProps={{
          "aria-label": `${label}`
        }}
      />
    </MuiPickersUtilsProvider>
    // </Grid>
  );
};

CustomDatePicker.defaultProps = {
  format: "dd-MM-yyyy",
  disableToolbar: false
};

export default CustomDatePicker;
