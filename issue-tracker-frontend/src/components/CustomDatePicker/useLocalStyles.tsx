import { makeStyles } from "@material-ui/core/styles";

export const useLocalStyles = makeStyles(theme => ({
  customDatePicker: {
    flexGrow: 1,
    width: "100%",
    marginTop: theme.spacing(1)
  }
}));
