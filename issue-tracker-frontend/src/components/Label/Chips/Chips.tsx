import React, { FC, useState } from "react";
import { Chip } from "@material-ui/core";
import { Label } from "../../../models/Label";
import { useStyles } from "../../../hooks/useStyles";
import { CustomInput } from "../../CustomInput/CustomInput";
import { useLocalStyles } from "../useLocalStyles";

interface Props {
  readOnly?: boolean;
  labels: Label[];
  setFieldValue: (
    field: string,
    value: any,
    shouldValidate?: boolean | undefined
  ) => void;
}

export const LabelChips: FC<Props> = ({ readOnly, labels, setFieldValue }) => {
  const { flexDirectionRow, flexDirectionColumnWithoutPadding } = useStyles();
  const { margin } = useLocalStyles();

  const [newLabel, setNewLabel] = useState<string>();

  const handleDelete = (one: Label) => {
    labels = labels.filter(two => two !== one);
    setFieldValue("labels", labels);
  };

  const generateNewLabelObject = () => {
    if (
      labels.filter(one => one.name.toLowerCase() === newLabel).length === 0
    ) {
      labels.push({ name: newLabel! });
      setFieldValue("labels", labels);
    }
    setNewLabel("");
  };

  if (readOnly) {
    return (
      <div className={flexDirectionRow}>
        {labels.map((one, i) => (
          <Chip className={margin} label={one.name} key={i} color="primary" />
        ))}
      </div>
    );
  } else {
    return (
      <div className={flexDirectionColumnWithoutPadding}>
        <CustomInput
          name="labels"
          labelName="Labels"
          className={margin}
          value={newLabel}
          handleBlur={() => {}}
          handleChange={(evt: any) => {
            setNewLabel(evt.target.value.toLowerCase());
          }}
          readOnlyMode={readOnly}
          handleKeyPress={(evt: React.KeyboardEvent<HTMLDivElement>) => {
            if (evt.key === "Enter" && newLabel && newLabel.trim().length > 0) {
              generateNewLabelObject();
            }
          }}
        />

        <div className={flexDirectionRow}>
          {labels.map((one, i) => (
            <Chip
              className={margin}
              label={one.name}
              key={i}
              color="primary"
              onDelete={() => handleDelete(one)}
            />
          ))}
        </div>
      </div>
    );
  }
};

LabelChips.defaultProps = {
  readOnly: false
};
