import { makeStyles } from "@material-ui/core/styles";

export const useLocalStyles = makeStyles(theme => ({
  margin: {
    margin: theme.spacing(1)
  }
}));
