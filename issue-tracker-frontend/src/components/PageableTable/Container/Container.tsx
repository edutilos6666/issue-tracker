import React, { FC } from "react";
import {
  TableContainer,
  Paper,
  Table,
  TableBody,
  TableRow,
  TableCell,
  TableHead,
  TableFooter,
  TablePagination,
  Tooltip,
} from "@material-ui/core";
import { useStyles } from "../../../hooks/useStyles";
import PageableTablePaginationActions from "../PaginationActions/PaginationAction";

export interface PageableTableProps {
  rows: any[];
  columns: PageableTableColumnProps[];
  count: number;
  page: number;
  setPage: (page: number) => void;
  rowsPerPage: number;
  setRowsPerPage: (rowsPerPage: number) => void;
  rowClickHandler?: (row: any) => void;
  rowCssClassName?: string;
  rowTooltipTitle?: string;
}

export interface PageableTableColumnProps {
  name: string;
  accessor: (row: any) => any;
}

const PageableTableContainer: FC<PageableTableProps> = ({
  page,
  setPage,
  rowsPerPage,
  setRowsPerPage,
  rows,
  columns,
  count,
  rowClickHandler,
  rowCssClassName,
  rowTooltipTitle,
}) => {
  // const [page, setPage] = useState<number>(0);
  // const [rowsPerPage, setRowsPerPage] = useState<number>(5);
  // const emptyRows =
  //   rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);
  const handleChangePage = (event: any, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event: any) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const { pageableTable } = useStyles();

  return (
    <TableContainer component={Paper}>
      <Table className={pageableTable} aria-label="custom pagination table">
        <TableHead>
          <TableRow>
            {columns.map((col, i) => (
              <TableCell key={i}>{col.name}</TableCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row, i) => (
            <TableRow
              key={i}
              onDoubleClick={() => rowClickHandler!(row)}
              className={rowCssClassName}
            >
              {columns.map((col, j) => (
                <Tooltip title={j === 0 ? rowTooltipTitle : ""} key={j}>
                  <TableCell key={j}>{col.accessor(row)}</TableCell>
                </Tooltip>
              ))}
            </TableRow>
          ))}
        </TableBody>
        <TableFooter>
          <TableRow>
            <TablePagination
              rowsPerPageOptions={[5, 10, 25, { label: "All", value: -1 }]}
              colSpan={columns.length}
              count={count}
              rowsPerPage={rowsPerPage}
              page={page}
              SelectProps={{
                inputProps: { "aria-label": "rows per page" },
                native: true,
              }}
              onChangePage={handleChangePage}
              onChangeRowsPerPage={handleChangeRowsPerPage}
              ActionsComponent={PageableTablePaginationActions}
            />
          </TableRow>
        </TableFooter>
      </Table>
    </TableContainer>
  );
};

PageableTableContainer.defaultProps = {
  rowClickHandler: () => {},
  rowCssClassName: "",
  rowTooltipTitle: "Double Click to go into Details",
};

export default PageableTableContainer;
