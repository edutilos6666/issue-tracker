import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
export const useLocalStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: "100%",
      backgroundColor: theme.palette.background.paper
    }
  })
);
