import React, { FC } from "react";
import { useLocalStyles } from "./useLocalStyles";
import { List, ListItem, ListItemIcon, ListItemText } from "@material-ui/core";
import FaceIcon from "@material-ui/icons/Face";

interface CustomListProps {
  items: any[];
  itemClickHandler: (item: any) => void;
  getPrimaryText: (item: any) => any;
  getSecondaryText: (item: any) => any;
}
const CustomList: FC<CustomListProps> = ({
  items,
  itemClickHandler,
  getPrimaryText,
  getSecondaryText,
}) => {
  const { root } = useLocalStyles();

  return (
    <div className={root}>
      <List component="nav" aria-label="custom list">
        {items.map((one, i) => (
          <ListItem key={i} button onClick={() => itemClickHandler(one)}>
            <ListItemIcon>
              <FaceIcon />
            </ListItemIcon>
            <ListItemText
              primary={getPrimaryText(one)}
              secondary={getSecondaryText(one)}
            />
          </ListItem>
        ))}
      </List>
    </div>
  );
};
export default CustomList;
