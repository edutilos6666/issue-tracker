import React, { FC, useEffect, useState } from "react";
import {
  Box,
  Paper,
  Typography,
  styled,
  Divider,
  Avatar,
} from "@material-ui/core";
import FaceIcon from "@material-ui/icons/Face";
import { useLocalStyles } from "../useLocalStyles";
import { UserInfoInterface } from "../../../context/AppContext";
import { LOCAL_STORAGE_SESSION_INFO } from "../../../utils/StorageConstants";
import { ProjectOverview } from "../../Project/Overview/Overview";
import UserHomeForm from "./Form";

interface Props {}

const UserHome: FC<Props> = () => {
  const { cyanAvatar, marginTop } = useLocalStyles();
  const [userInfo, setUserInfo] = useState<UserInfoInterface>();

  useEffect(() => {
    const sessionInfo = JSON.parse(
      localStorage.getItem(LOCAL_STORAGE_SESSION_INFO)!
    );
    sessionInfo && setUserInfo(sessionInfo.userInfo);
  }, []);

  if (userInfo)
    return (
      <Box
        component={Paper}
        marginTop="10px"
        marginLeft="1%"
        marginRight="1%"
        padding="5%"
        paddingTop="20px"
      >
        <Avatar className={cyanAvatar}>
          <FaceIcon />
        </Avatar>
        {/* <HBox label="username" content={userInfo.username} />
        <Divider light />
        <HBox label="firstname" content={userInfo.firstname} />
        <Divider light />
        <HBox label="lastname" content={userInfo.lastname} />
        <Divider light />
        {userInfo.roles ? (
          <HBox label="role" content={userInfo.roles.join(", ")} />
        ) : (
          ""
        )} */}

        <UserHomeForm userInfo={userInfo} />

        <Divider light />

        <Typography className={marginTop}>
          <strong>Participated Projects</strong>
        </Typography>
        <ProjectOverview username={userInfo!.username} />
      </Box>
    );

  return <></>;
};
export default UserHome;

const TypographyWithMargin = styled(Typography)({
  marginRight: "40px",
});
interface HBoxProps {
  label: string;
  content: any;
}
const HBox: FC<HBoxProps> = ({ label, content }) => {
  return (
    <Box
      component="div"
      display="flex"
      flexDirection="row"
      justifyContent="flex-start"
      marginTop="30px"
    >
      <TypographyWithMargin>
        <strong>
          {label.charAt(0).toUpperCase()}
          {label.substr(1, label.length)}:{" "}
        </strong>
      </TypographyWithMargin>
      <Typography>{content}</Typography>
    </Box>
  );
};
