import React, { FC, useContext, useEffect, useState, useReducer } from "react";
import { IssueDetailsHistoryContext } from "../../../context/HistoryContext";
import { useStyles } from "../../../hooks/useStyles";
import {
  UserHomeFormReducer,
  emptyInstance_UserHomeFormData,
  SET_OBJECT,
} from "./TypesAndReducer";
import { Grid, Button, Card } from "@material-ui/core";
import { Formik } from "formik";
import * as Yup from "yup";
import EditIcon from "@material-ui/icons/Edit";
import SaveIcon from "@material-ui/icons/Save";
import { useLocalStyles } from "../useLocalStyles";
import { CustomInput } from "../../CustomInput/CustomInput";
import { SingleSelect } from "../../CustomSelect/SingleSelect";
import { useListDataHooks } from "../../../utils/useListDataHook";
import { useUserService } from "../../../services/useUserService";
import { UserInfoInterface } from "../../../context/AppContext";
import { Role } from "../../../models/Role";
import {
  LOCAL_STORAGE_TOKEN,
  LOCAL_STORAGE_SESSION_INFO,
} from "../../../utils/StorageConstants";
import { useSecurityService } from "../../../services/useSecurityService";

interface Props {
  userInfo: UserInfoInterface;
}

const UserHomeForm: FC<Props> = ({ userInfo }) => {
  const { history } = useContext(IssueDetailsHistoryContext);
  const { flexDirectionColumn, flexJustifyItemsEnd, floatRight } = useStyles();
  const { margin, spanStyle } = useLocalStyles();

  const { updateCurrentUser, testIfUsernameExists } = useUserService();
  const { logout } = useSecurityService();
  const { useFindAllRoles } = useListDataHooks();
  const { roleList } = useFindAllRoles();

  const [reducerData, reducerDispatch] = useReducer(
    UserHomeFormReducer,
    emptyInstance_UserHomeFormData
  );

  useEffect(() => {
    if (
      roleList &&
      roleList.length > 0 &&
      userInfo &&
      userInfo.roles &&
      userInfo.roles.length > 0
    )
      reducerDispatch({
        type: SET_OBJECT,
        value: {
          username: userInfo.username,
          firstname: userInfo.firstname,
          lastname: userInfo.lastname,
          role: roleList.filter((one) => one.name === userInfo.roles[0])[0],
        },
      });
  }, [roleList]);
  const validationSchema = Yup.object().shape({
    username: Yup.string()
      .required("Username is required.")
      .test("username", "Username already taken", async (value: any) => {
        if (userInfo.username === value) return true;
        const exists = (await testIfUsernameExists(value)).data;
        console.log(value, exists);
        return !exists;
      }),
    firstname: Yup.string().required("Firstname is required."),
    lastname: Yup.string().required("Lastname is required."),
    role: Yup.string().required("Role is required."),
  });

  const [readOnlyMode, setReadonlyMode] = useState<boolean>(true);

  useEffect(() => {
    console.log(reducerData);
    if (
      reducerData &&
      (reducerData.triggerUpdate === true ||
        reducerData.triggerUpdate === false)
    ) {
      updateCurrentUser({
        id: userInfo.id!,
        username: reducerData.username!,
        password: "",
        firstname: reducerData.firstname!,
        lastname: reducerData.lastname!,
        roles: [reducerData.role!],
      }).then((res) => {
        console.log("updated user = ", res);
        const token = localStorage.getItem(LOCAL_STORAGE_TOKEN);
        token &&
          logout(token).then((res) => {
            localStorage.removeItem(LOCAL_STORAGE_SESSION_INFO);
            localStorage.removeItem(LOCAL_STORAGE_TOKEN);
            window.location.reload();
          });
      });
    }
  }, [reducerData.triggerUpdate]);

  return (
    <Card className={flexDirectionColumn}>
      <Formik
        enableReinitialize
        validationSchema={validationSchema}
        initialValues={reducerData}
        onSubmit={(values, { setSubmitting }) => {
          console.log(values);
          reducerDispatch({
            type: SET_OBJECT,
            value: {
              ...values,
              triggerUpdate: !reducerData.triggerUpdate,
              role:
                values.role && values.role.id
                  ? values.role
                  : values.role
                  ? roleList.filter((one) => one.id === values.role + "")[0]
                  : undefined,
            },
          });
          setReadonlyMode(true);
          setSubmitting(false);
        }}
      >
        {({
          values,
          touched,
          handleSubmit,
          handleChange,
          handleBlur,
          errors,
        }) => (
          <form noValidate onSubmit={handleSubmit}>
            <Grid container spacing={3}>
              {/* username */}
              <Grid item xs={12}>
                <CustomInput
                  name="username"
                  labelName="Username"
                  className={margin}
                  error={errors.username && touched.username ? true : false}
                  value={values.username}
                  handleBlur={handleBlur}
                  handleChange={handleChange}
                  readOnlyMode={readOnlyMode}
                />
              </Grid>

              {/* firstname */}
              <Grid item xs={12}>
                <CustomInput
                  name="firstname"
                  labelName="Firstname"
                  className={margin}
                  error={errors.firstname && touched.firstname ? true : false}
                  value={values.firstname}
                  handleBlur={handleBlur}
                  handleChange={handleChange}
                  readOnlyMode={readOnlyMode}
                />
              </Grid>

              {/* lastname */}
              <Grid item xs={12}>
                <CustomInput
                  name="lastname"
                  labelName="Lastname"
                  className={margin}
                  error={errors.lastname && touched.lastname ? true : false}
                  value={values.lastname}
                  handleBlur={handleBlur}
                  handleChange={handleChange}
                  readOnlyMode={readOnlyMode}
                />
              </Grid>

              {/* role */}
              <Grid item xs={12}>
                <SingleSelect
                  name="role"
                  labelName="Role"
                  className={margin}
                  error={errors.role && touched.role ? true : false}
                  value={values.role}
                  handleBlur={handleBlur}
                  handleChange={handleChange}
                  data={roleList}
                  accessorItemText={(el: Role) => {
                    return el ? el.name! : "";
                  }}
                  accessorItemValue={(el: Role) => {
                    return el ? el.id : "";
                  }}
                  readOnlyMode={readOnlyMode}
                />
              </Grid>

              <Grid item xs={10} />
              {readOnlyMode ? (
                <Grid item xs={2}>
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={() => setReadonlyMode(false)}
                    className={floatRight}
                  >
                    <span className={spanStyle}>Edit Infos</span>
                    <EditIcon />
                  </Button>
                </Grid>
              ) : (
                <Grid item xs={2}>
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={() => handleSubmit()}
                    className={floatRight}
                  >
                    <span className={spanStyle}>Save Changes</span>
                    <SaveIcon />
                  </Button>
                </Grid>
              )}
            </Grid>
          </form>
        )}
      </Formik>
    </Card>
  );
};

export default UserHomeForm;
