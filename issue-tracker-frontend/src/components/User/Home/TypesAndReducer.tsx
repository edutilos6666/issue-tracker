import { Maybe } from "../../../types/Maybe";
import { Role } from "../../../models/Role";

export interface UserHomeFormData {
  username?: Maybe<string>;
  firstname?: Maybe<string>;
  lastname?: Maybe<string>;
  role?: Maybe<Role>;
  triggerUpdate?: Maybe<boolean>;
}

export const emptyInstance_UserHomeFormData: UserHomeFormData = {
  username: undefined,
  firstname: undefined,
  lastname: undefined,
  role: undefined,
  triggerUpdate: undefined,
};

export enum UserHomeFormReducerActionTypes {
  SET_USERNAME = "setUsername",
  SET_FIRSTNAME = "setFirstname",
  SET_LASTNAME = "setLastname",
  SET_ROLE = "setRole",
  SET_TRIGGER_UPDATE = "setTriggerUpdate",
  SET_OBJECT = "SET_OBJECT",
}

export const {
  SET_USERNAME,
  SET_FIRSTNAME,
  SET_LASTNAME,
  SET_ROLE,
  SET_TRIGGER_UPDATE,
  SET_OBJECT,
} = UserHomeFormReducerActionTypes;

export type UserHomeFormReducerAction =
  | {
      type:
        | UserHomeFormReducerActionTypes.SET_USERNAME
        | UserHomeFormReducerActionTypes.SET_FIRSTNAME
        | UserHomeFormReducerActionTypes.SET_LASTNAME;
      value: string;
    }
  | {
      type: UserHomeFormReducerActionTypes.SET_TRIGGER_UPDATE;
      value: boolean;
    }
  | {
      type: UserHomeFormReducerActionTypes.SET_ROLE;
      value: Role;
    }
  | {
      type: UserHomeFormReducerActionTypes.SET_OBJECT;
      value: UserHomeFormData;
    };

export const UserHomeFormReducer = (
  state: UserHomeFormData,
  action: UserHomeFormReducerAction
): UserHomeFormData => {
  let newValue;
  switch (action.type) {
    case SET_USERNAME:
      newValue = { ...state, username: action.value };
      break;
    case SET_FIRSTNAME:
      newValue = { ...state, firstname: action.value };
      break;
    case SET_LASTNAME:
      newValue = { ...state, lastname: action.value };
      break;
    case SET_TRIGGER_UPDATE:
      newValue = { ...state, triggerUpdate: action.value };
      break;
    case SET_ROLE:
      newValue = { ...state, role: action.value };
      break;
    case SET_OBJECT:
      newValue = action.value;
      break;
    default:
      newValue = state;
  }
  return newValue;
};
