import React, { FC, useReducer, useEffect, useContext } from "react";
import { Paper, Typography, Grid, Button, Link } from "@material-ui/core";
import { Formik } from "formik";
import * as Yup from "yup";
import {
  emptyInstance_UserCreateData,
  UserCreateReducer,
  SET_OBJECT,
} from "./TypesAndReducer";
import { UserCreateHistroyContext } from "../../../context/HistoryContext";
import { CustomInput } from "../../CustomInput/CustomInput";
import { useLocalStyles } from "../useLocalStyles";
import SaveIcon from "@material-ui/icons/Save";
import UndoIcon from "@material-ui/icons/Undo";
import { useUserService } from "../../../services/useUserService";
import { Link as RouterLink } from "react-router-dom";
import { SingleSelect } from "../../CustomSelect/SingleSelect";
import { useListDataHooks } from "../../../utils/useListDataHook";
import { Role } from "../../../models/Role";

interface Props {}
const UserCreate: FC<Props> = () => {
  const { history } = useContext(UserCreateHistroyContext);
  const { createUser, testIfUsernameExists } = useUserService();
  const { useFindAllRoles } = useListDataHooks();
  const { roleList } = useFindAllRoles();

  const validationSchema = Yup.object().shape({
    username: Yup.string()
      .required("Username is required.")
      .test("username", "Username already taken", async (value: any) => {
        const exists = (await testIfUsernameExists(value)).data;
        console.log(value, exists);
        return !exists;
      }),
    firstname: Yup.string().required("Firstname is required."),
    lastname: Yup.string().required("Lastname is required."),
    password: Yup.string().required("Password is required."),
    // passwordRepeat: Yup.string().required("PasswordRepeat is required."),
    passwordRepeat: Yup.string()
      .oneOf([Yup.ref("password"), null])
      .required("passwordRepeat is required"),
    role: Yup.string().required("Role is required."),
  });
  const [reducerData, reducerDispatch] = useReducer(
    UserCreateReducer,
    emptyInstance_UserCreateData
  );

  const { paperStyle, margin, spanStyle, centeredDiv } = useLocalStyles();

  useEffect(() => {
    if (
      reducerData.triggerCreate === true ||
      reducerData.triggerCreate === false
    ) {
      createUser({
        username: reducerData.username!,
        password: reducerData.password!,
        firstname: reducerData.firstname!,
        lastname: reducerData.lastname!,
        roles: [reducerData.role!],
      }).then((res) => {
        history.goBack();
      });
    }
  }, [reducerData.triggerCreate]);

  return (
    <Paper className={paperStyle}>
      <div className={centeredDiv}>
        <Typography>
          <strong>User Registration</strong>
        </Typography>

        {/* <Router> */}
        <Link
          className={margin}
          component={RouterLink}
          to="/login"
          // onClick={() => history.push("/login")}
        >
          (Already registered?)
        </Link>
        {/* </Router> */}
      </div>
      <Formik
        enableReinitialize
        validationSchema={validationSchema}
        initialValues={reducerData}
        onSubmit={(values, { setSubmitting }) => {
          console.log(values);
          reducerDispatch({
            type: SET_OBJECT,
            value: {
              ...values,
              triggerCreate: !reducerData.triggerCreate,
              role:
                values.role && values.role.id
                  ? values.role
                  : values.role
                  ? roleList.filter((one) => one.id === values.role + "")[0]
                  : undefined,
            },
          });
          setSubmitting(false);
        }}
        onReset={(fields) => {
          reducerDispatch({
            type: SET_OBJECT,
            value: emptyInstance_UserCreateData,
          });
        }}
      >
        {({
          isSubmitting,
          values,
          touched,
          handleSubmit,
          setFieldValue,
          handleChange,
          handleBlur,
          errors,
          resetForm,
          handleReset,
        }) => (
          <form noValidate onSubmit={handleSubmit}>
            <Grid container spacing={3}>
              {/* username */}
              <Grid item xs={12}>
                <CustomInput
                  name="username"
                  labelName="Username"
                  className={margin}
                  error={errors.username && touched.username ? true : false}
                  value={values.username}
                  handleBlur={handleBlur}
                  handleChange={handleChange}
                />
              </Grid>

              {/* password */}
              <Grid item xs={12}>
                <CustomInput
                  type="password"
                  name="password"
                  labelName="Password"
                  className={margin}
                  error={errors.password && touched.password ? true : false}
                  value={values.password}
                  handleBlur={handleBlur}
                  handleChange={handleChange}
                />
              </Grid>

              {/* passwordRepeat */}
              <Grid item xs={12}>
                <CustomInput
                  type="password"
                  name="passwordRepeat"
                  labelName="Password Repeat"
                  className={margin}
                  error={
                    errors.passwordRepeat && touched.passwordRepeat
                      ? true
                      : false
                  }
                  value={values.passwordRepeat}
                  handleBlur={handleBlur}
                  handleChange={handleChange}
                />
              </Grid>

              {/* firstname */}
              <Grid item xs={12}>
                <CustomInput
                  name="firstname"
                  labelName="Firstname"
                  className={margin}
                  error={errors.firstname && touched.firstname ? true : false}
                  value={values.firstname}
                  handleBlur={handleBlur}
                  handleChange={handleChange}
                />
              </Grid>

              {/* lastname */}
              <Grid item xs={12}>
                <CustomInput
                  name="lastname"
                  labelName="Lastname"
                  className={margin}
                  error={errors.lastname && touched.lastname ? true : false}
                  value={values.lastname}
                  handleBlur={handleBlur}
                  handleChange={handleChange}
                />
              </Grid>

              {/* role */}
              <Grid item xs={12}>
                <SingleSelect
                  name="role"
                  labelName="Role"
                  className={margin}
                  error={errors.role && touched.role ? true : false}
                  value={values.role}
                  handleBlur={handleBlur}
                  handleChange={handleChange}
                  data={roleList}
                  accessorItemText={(el: Role) => {
                    return el ? el.name! : "";
                  }}
                  accessorItemValue={(el: Role) => {
                    return el ? el.id : "";
                  }}
                />
              </Grid>

              <Grid item xs={10}></Grid>
              <Grid item xs={1}>
                <Button
                  type="submit"
                  variant="contained"
                  color="primary"
                  // onClick={() => handleSubmit()}
                >
                  <span className={spanStyle}>Submit</span>
                  <SaveIcon />
                </Button>
              </Grid>
              <Grid item xs={1}>
                <Button
                  variant="contained"
                  color="secondary"
                  onClick={() => handleReset()}
                >
                  <span className={spanStyle}>Reset</span>
                  <UndoIcon />
                </Button>
              </Grid>
            </Grid>
          </form>
        )}
      </Formik>
    </Paper>
  );
};
export default UserCreate;
