import { Maybe } from "../../../types/Maybe";
import { Role } from "../../../models/Role";

export interface UserCreateData {
  username?: Maybe<string>;
  password?: Maybe<string>;
  passwordRepeat?: Maybe<string>;
  firstname?: Maybe<string>;
  lastname?: Maybe<string>;
  role?: Maybe<Role>;
  triggerCreate?: Maybe<boolean>;
}

export const emptyInstance_UserCreateData: UserCreateData = {
  username: undefined,
  password: undefined,
  passwordRepeat: undefined,
  firstname: undefined,
  lastname: undefined,
  role: undefined,
  triggerCreate: undefined,
};

export enum UserCreateReducerActionTypes {
  SET_USERNAME = "setUsername",
  SET_PASSWORD = "setPassword",
  SET_PASSWORD_REPEAT = "setPasswordRepeat",
  SET_FIRSTNAME = "setFirstname",
  SET_LASTNAME = "setLastname",
  SET_ROLE = "setRole",
  SET_TRIGGER_CREATE = "setTriggerCreate",
  SET_OBJECT = "SET_OBJECT",
}

export const {
  SET_USERNAME,
  SET_PASSWORD,
  SET_PASSWORD_REPEAT,
  SET_FIRSTNAME,
  SET_LASTNAME,
  SET_ROLE,
  SET_TRIGGER_CREATE,
  SET_OBJECT,
} = UserCreateReducerActionTypes;

export type UserCreateReducerAction =
  | {
      type:
        | UserCreateReducerActionTypes.SET_USERNAME
        | UserCreateReducerActionTypes.SET_PASSWORD
        | UserCreateReducerActionTypes.SET_PASSWORD_REPEAT
        | UserCreateReducerActionTypes.SET_FIRSTNAME
        | UserCreateReducerActionTypes.SET_LASTNAME;
      value: string;
    }
  | {
      type: UserCreateReducerActionTypes.SET_TRIGGER_CREATE;
      value: boolean;
    }
  | {
      type: UserCreateReducerActionTypes.SET_ROLE;
      value: Role;
    }
  | {
      type: UserCreateReducerActionTypes.SET_OBJECT;
      value: UserCreateData;
    };

export const UserCreateReducer = (
  state: UserCreateData,
  action: UserCreateReducerAction
): UserCreateData => {
  let newValue;
  switch (action.type) {
    case SET_USERNAME:
      newValue = { ...state, username: action.value };
      break;
    case SET_PASSWORD:
      newValue = { ...state, password: action.value };
      break;
    case SET_PASSWORD_REPEAT:
      newValue = { ...state, passwordRepeat: action.value };
      break;
    case SET_FIRSTNAME:
      newValue = { ...state, firstname: action.value };
      break;
    case SET_LASTNAME:
      newValue = { ...state, lastname: action.value };
      break;
    case SET_TRIGGER_CREATE:
      newValue = { ...state, triggerCreate: action.value };
      break;
    case SET_ROLE:
      newValue = { ...state, role: action.value };
      break;
    case SET_OBJECT:
      newValue = action.value;
      break;
    default:
      newValue = state;
  }
  return newValue;
};
