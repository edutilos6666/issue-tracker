import React, { FC, useEffect, useState, useContext } from "react";
import { User } from "../../../models/User";
import {
  Box,
  Paper,
  Typography,
  styled,
  Divider,
  Avatar,
} from "@material-ui/core";
import { UserDetailsHistoryContext } from "../../../context/HistoryContext";
import FaceIcon from "@material-ui/icons/Face";
import { green, pink } from "@material-ui/core/colors";
import { useLocalStyles } from "../useLocalStyles";

interface Props {}

const UserDetails: FC<Props> = () => {
  const [user, setUser] = useState<User>();
  const { history } = useContext(UserDetailsHistoryContext);

  const { greenAvatar } = useLocalStyles();

  useEffect(() => {
    if (history.location.state.user) {
      setUser(history.location.state.user);
    }
  }, [history.location.state]);

  if (user)
    return (
      <Box
        component={Paper}
        marginTop="10px"
        marginLeft="30%"
        marginRight="30%"
        padding="5%"
        paddingTop="20px"
      >
        <Avatar className={greenAvatar}>
          <FaceIcon />
        </Avatar>
        <HBox label="username" content={user.username} />
        <Divider light />
        <HBox label="firstname" content={user.firstname} />
        <Divider light />
        <HBox label="lastname" content={user.lastname} />
        <Divider light />
        <HBox
          label="role"
          content={user.roles.map((one) => one.name).join(", ")}
        />
        <Divider light />
      </Box>
    );

  return <></>;
};
export default UserDetails;

const GreenAvatar = styled(Avatar)({
  color: "#fff",
  backgroundColor: green[500],
  width: "200px",
  height: "200px",
});

const TypographyWithMargin = styled(Typography)({
  marginRight: "40px",
});
interface HBoxProps {
  label: string;
  content: any;
}
const HBox: FC<HBoxProps> = ({ label, content }) => {
  return (
    <Box
      component="div"
      display="flex"
      flexDirection="row"
      justifyContent="flex-start"
      marginTop="30px"
    >
      <TypographyWithMargin>
        <strong>
          {label.charAt(0).toUpperCase()}
          {label.substr(1, label.length)}:{" "}
        </strong>
      </TypographyWithMargin>
      <Typography>{content}</Typography>
    </Box>
  );
};
