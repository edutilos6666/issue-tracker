import React, { FC, useEffect } from "react";
import { Project } from "../../../models/Project";
import { useStyles } from "../../../hooks/useStyles";
import CustomList from "../../CustomList/CustomList";
import { Typography, Button } from "@material-ui/core";
import { User } from "../../../models/User";
import GroupAddIcon from "@material-ui/icons/GroupAdd";
import PanToolIcon from "@material-ui/icons/PanTool";
import { useLocalStyles } from "../useLocalStyles";
import { useProjectService } from "../../../services/useProjectService";
import { LOCAL_STORAGE_SESSION_INFO } from "../../../utils/StorageConstants";
import { useHistory } from "react-router";

interface Props {
  project: Project;
  currentUserInProject: boolean;
}
const UserOverview: FC<Props> = ({ project, currentUserInProject }) => {
  const { flexDirectionColumn } = useStyles();
  const { spanStyle } = useLocalStyles();
  const history = useHistory();

  const {
    addCurrentUserToProject,
    removeCurrentUserFromProject,
  } = useProjectService();

  const handleBtnJoinClick = () => {
    addCurrentUserToProject(project).then((res) => {
      console.log("updated project = ", project);
      history.goBack();
    });
  };

  const handleBtnLeaveClick = () => {
    removeCurrentUserFromProject(project).then((res) => {
      console.log("updated project = ", project);
      history.goBack();
    });
  };

  const currentUserInfo = JSON.parse(
    localStorage.getItem(LOCAL_STORAGE_SESSION_INFO)!
  );

  useEffect(() => {
    console.log(currentUserInfo);
  }, [currentUserInfo]);

  return (
    <div className={flexDirectionColumn}>
      <Typography>
        <strong>All Other Users</strong>
      </Typography>
      <CustomList
        items={project.users.filter(
          (one) => one.id !== currentUserInfo.userInfo.id
        )}
        itemClickHandler={(one: User) =>
          history.push(`/user-details/${one.id}`, { user: one })
        }
        getPrimaryText={(one: User) => `${one.username}`}
        getSecondaryText={(one: User) =>
          `roles: ${one.roles.map((two) => two.name).join(", ")}`
        }
      />

      {currentUserInProject ? (
        <Button
          variant="contained"
          color="primary"
          onClick={handleBtnLeaveClick}
        >
          {" "}
          <span className={spanStyle}>Leave the project</span>
          <PanToolIcon />
        </Button>
      ) : (
        <Button
          variant="contained"
          color="primary"
          onClick={handleBtnJoinClick}
        >
          {" "}
          <span className={spanStyle}>Join to the project</span>
          <GroupAddIcon />
        </Button>
      )}
    </div>
  );
};
export default UserOverview;
