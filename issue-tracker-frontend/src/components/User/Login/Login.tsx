import React, { FC, useReducer, useEffect } from "react";
import { Paper, Typography, Grid, Button, Link } from "@material-ui/core";
import { Formik } from "formik";
import * as Yup from "yup";
import {
  emptyInstance_UserLoginData,
  UserLoginReducer,
  SET_OBJECT,
} from "./TypesAndReducer";
import { CustomInput } from "../../CustomInput/CustomInput";
import { useLocalStyles } from "../useLocalStyles";
import SaveIcon from "@material-ui/icons/Save";
import UndoIcon from "@material-ui/icons/Undo";
import { Link as RouterLink, useHistory, Redirect } from "react-router-dom";
import { useSecurityService } from "../../../services/useSecurityService";
import useAppController from "../../../hooks/useAppController";
import { PROJECT_OVERVIEW_BASE_PATH } from "../../../utils/RouterConstants";
import { Location } from "history";

interface Props {
  location: Location<any>;
}
const UserLogin: FC<Props> = ({ location }) => {
  const history = useHistory();
  const { login } = useSecurityService();
  const { setToken, loggedIn } = useAppController();

  const { from } = location.state || { from: { pathname: "/" } };

  // if (loggedIn && history) {
  //   history.push(PROJECT_OVERVIEW_BASE_PATH);
  // }

  const validationSchema = Yup.object().shape({
    username: Yup.string().required("Username is required."),
    password: Yup.string().required("Password is required."),
  });
  const [reducerData, reducerDispatch] = useReducer(
    UserLoginReducer,
    emptyInstance_UserLoginData
  );

  const { paperStyle, margin, spanStyle, centeredDiv } = useLocalStyles();

  useEffect(() => {
    if (
      reducerData.triggerCreate === true ||
      reducerData.triggerCreate === false
    ) {
      login(reducerData.username!, reducerData.password!).then((res) => {
        if (res && res.data && res.data.token) {
          setToken(res.data.token);
          history.push(PROJECT_OVERVIEW_BASE_PATH);
        }
      });
    }
  }, [reducerData.triggerCreate]);

  if (loggedIn && from) {
    return <Redirect to={from} />;
  }

  return (
    <Paper className={paperStyle}>
      <div className={centeredDiv}>
        <Typography>
          <strong>User Login</strong>
        </Typography>

        {/* <Router> */}
        <Link
          className={margin}
          component={RouterLink}
          to="/register"
          // onClick={() => history.push("/login")}
        >
          (Not registered yet?)
        </Link>
        {/* </Router> */}
      </div>
      <Formik
        enableReinitialize
        validationSchema={validationSchema}
        initialValues={reducerData}
        onSubmit={(values, { setSubmitting }) => {
          console.log(values);
          reducerDispatch({
            type: SET_OBJECT,
            value: {
              ...values,
              triggerCreate: !reducerData.triggerCreate,
            },
          });
          setSubmitting(false);
        }}
        onReset={(fields) => {
          reducerDispatch({
            type: SET_OBJECT,
            value: emptyInstance_UserLoginData,
          });
        }}
      >
        {({
          isSubmitting,
          values,
          touched,
          handleSubmit,
          setFieldValue,
          handleChange,
          handleBlur,
          errors,
          resetForm,
          handleReset,
        }) => (
          <form onSubmit={handleSubmit}>
            <Grid container spacing={3}>
              {/* username */}
              <Grid item xs={12}>
                <CustomInput
                  name="username"
                  labelName="Username"
                  className={margin}
                  error={errors.username && touched.username ? true : false}
                  value={values.username}
                  handleBlur={handleBlur}
                  handleChange={handleChange}
                />
              </Grid>

              {/* password */}
              <Grid item xs={12}>
                <CustomInput
                  type="password"
                  name="password"
                  labelName="Password"
                  className={margin}
                  error={errors.password && touched.password ? true : false}
                  value={values.password}
                  handleBlur={handleBlur}
                  handleChange={handleChange}
                />
              </Grid>

              <Grid item xs={10}></Grid>
              <Grid item xs={1}>
                <Button
                  type="submit"
                  variant="contained"
                  color="primary"
                  // onClick={() => handleSubmit()}
                >
                  <span className={spanStyle}>Submit</span>
                  <SaveIcon />
                </Button>
              </Grid>
              <Grid item xs={1}>
                <Button
                  variant="contained"
                  color="secondary"
                  onClick={() => handleReset()}
                >
                  <span className={spanStyle}>Reset</span>
                  <UndoIcon />
                </Button>
              </Grid>
            </Grid>
          </form>
        )}
      </Formik>
    </Paper>
  );
};
export default UserLogin;
