import { Maybe } from "../../../types/Maybe";
import { Role } from "../../../models/Role";

export interface UserLoginData {
  username?: Maybe<string>;
  password?: Maybe<string>;
  triggerCreate?: Maybe<boolean>;
}

export const emptyInstance_UserLoginData: UserLoginData = {
  username: undefined,
  password: undefined,
  triggerCreate: undefined,
};

export enum UserLoginReducerActionTypes {
  SET_USERNAME = "setUsername",
  SET_PASSWORD = "setPassword",
  SET_TRIGGER_CREATE = "setTriggerCreate",
  SET_OBJECT = "SET_OBJECT",
}

export const {
  SET_USERNAME,
  SET_PASSWORD,
  SET_TRIGGER_CREATE,
  SET_OBJECT,
} = UserLoginReducerActionTypes;

export type UserLoginReducerAction =
  | {
      type:
        | UserLoginReducerActionTypes.SET_USERNAME
        | UserLoginReducerActionTypes.SET_PASSWORD;
      value: string;
    }
  | {
      type: UserLoginReducerActionTypes.SET_TRIGGER_CREATE;
      value: boolean;
    }
  | {
      type: UserLoginReducerActionTypes.SET_OBJECT;
      value: UserLoginData;
    };

export const UserLoginReducer = (
  state: UserLoginData,
  action: UserLoginReducerAction
): UserLoginData => {
  let newValue;
  switch (action.type) {
    case SET_USERNAME:
      newValue = { ...state, username: action.value };
      break;
    case SET_PASSWORD:
      newValue = { ...state, password: action.value };
      break;
    case SET_TRIGGER_CREATE:
      newValue = { ...state, triggerCreate: action.value };
      break;
    case SET_OBJECT:
      newValue = action.value;
      break;
    default:
      newValue = state;
  }
  return newValue;
};
