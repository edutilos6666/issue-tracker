import { makeStyles } from "@material-ui/core/styles";
import { cyan, green } from "@material-ui/core/colors";

export const useLocalStyles = makeStyles((theme) => ({
  paperStyle: {
    margin: theme.spacing(5),
    padding: theme.spacing(5),
  },
  margin: {
    margin: theme.spacing(1),
  },
  spanStyle: {
    marginRight: theme.spacing(2),
  },
  marginTop: {
    marginTop: theme.spacing(10),
  },
  marginBottom: {
    marginBottom: theme.spacing(3),
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
  centeredDiv: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  cyanAvatar: {
    color: "#fff",
    backgroundColor: cyan[500],
    width: theme.spacing(7),
    height: theme.spacing(7),
  },
  greenAvatar: {
    color: "#fff",
    backgroundColor: green[500],
    width: theme.spacing(7),
    height: theme.spacing(7),
  },
}));
