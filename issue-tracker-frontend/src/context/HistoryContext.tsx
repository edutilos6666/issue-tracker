import { createContext } from "react";

// I don't use this file anymore, because I am using  const history = useHistory() from "react-router"
export interface HistoryContextInterface {
  history: any;
}

export const ProjectOverviewHistoryContext = createContext<
  HistoryContextInterface
>({
  history: undefined,
});

export const ProjectDetailsHistoryContext = createContext<
  HistoryContextInterface
>({
  history: undefined,
});

export const IssueDetailsHistoryContext = createContext<
  HistoryContextInterface
>({
  history: undefined,
});

export const UserDetailsHistoryContext = createContext<HistoryContextInterface>(
  {
    history: undefined,
  }
);

export const IssueCreateHistoryContext = createContext<HistoryContextInterface>(
  {
    history: undefined,
  }
);

export const ProjectCreateHistoryContext = createContext<
  HistoryContextInterface
>({
  history: undefined,
});

export const UserCreateHistroyContext = createContext<HistoryContextInterface>({
  history: undefined,
});

export const UserLoginHistoryContext = createContext<HistoryContextInterface>({
  history: undefined,
});
