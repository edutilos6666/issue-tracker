import React, { createContext, FC, useState } from "react";
import { Maybe } from "../types/Maybe";

interface AppContextInterface {
  token?: Maybe<string>;
  setToken: (token: Maybe<string>) => void;
  loggedIn?: Maybe<boolean>;
  setLoggedIn: (loggedIn: Maybe<boolean>) => void;
  userInfo?: Maybe<UserInfoInterface>;
  setUserInfo: (userInfo: Maybe<UserInfoInterface>) => void;
  lastCheckDate?: Maybe<number>;
  setLastCheckDate: (lastCheckDate: Maybe<number>) => void;
}

export interface UserInfoInterface {
  id: Maybe<string>;
  username: Maybe<string>;
  firstname: Maybe<string>;
  lastname: Maybe<string>;
  roles: string[];
}

export const AppContext = createContext<AppContextInterface>({
  setToken: (token) => {},
  setLoggedIn: (loggedIn) => {},
  setUserInfo: (userInfo) => {},
  setLastCheckDate: (lastCheckDate) => {},
});

export const AppProvider: FC = ({ children }) => {
  const [token, setToken] = useState<Maybe<string>>(null);
  const [loggedIn, setLoggedIn] = useState<Maybe<boolean>>(false);
  const [userInfo, setUserInfo] = useState<Maybe<UserInfoInterface>>();
  const [lastCheckDate, setLastCheckDate] = useState<Maybe<number>>();

  const returnValue = {
    token,
    setToken,
    loggedIn,
    setLoggedIn,
    userInfo,
    setUserInfo,
    lastCheckDate,
    setLastCheckDate,
  };

  return (
    <AppContext.Provider value={returnValue}>{children}</AppContext.Provider>
  );
};
