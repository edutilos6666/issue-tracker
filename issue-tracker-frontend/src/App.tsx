import React from "react";
import "./App.css";
import AppContent from "./pages/App/Content";
import { AppProvider } from "./context/AppContext";

function App() {
  return (
    <AppProvider>
      <AppContent />
    </AppProvider>
  );
}

export default App;
