import { LOCAL_STORAGE_SESSION_INFO } from "../utils/StorageConstants";
import { Project } from "../models/Project";
import { Maybe } from "../types/Maybe";
import { useEffect, useState } from "react";

export const findCurrentUserInfo = () => {
  const currentUserInfo = JSON.parse(
    localStorage.getItem(LOCAL_STORAGE_SESSION_INFO)!
  );

  return {
    currentUserInfo,
  };
};

export const checkIfCurrentUserInProject = (project: Maybe<Project>): any => {
  const { currentUserInfo } = findCurrentUserInfo();
  return project && currentUserInfo
    ? project.users.filter((one) => one.id === currentUserInfo.userInfo.id)
        .length > 0
    : false;
};
