import axios, { AxiosResponse } from "axios";
import {
  IssueOverviewData,
  emptyInstance_IssueOverviewData,
} from "../components/Issue/Overview/TypesAndReducer";
import { Issue } from "../models/Issue";
import useBaseUrl from "../utils/useBaseUrl";
import { getAuthHeaders } from "./AuthHeadersService";

export const useIssueService = () => {
  const { baseUrl } = useBaseUrl();

  const createIssue = (issue: Issue): Promise<AxiosResponse> => {
    const url = `${baseUrl}/issues`;
    return axios.post(url, issue, {
      headers: getAuthHeaders(),
    });
  };

  const updateIssue = (issue: Issue): void => {
    const url = `${baseUrl}/issues`;
    axios.patch(url, issue, {
      headers: getAuthHeaders(),
    });
  };

  const testIfIssueNameExists = (name: string): Promise<AxiosResponse> => {
    const url = `${baseUrl}/issues/name-exists/${name}`;
    return axios.get(url, {
      headers: getAuthHeaders(),
    });
  };

  const findAllIssues = (
    converter: (date: Date, lowerBound?: boolean) => string,
    projectId: string,
    page: number = 0,
    rowsPerPage: number = 0,
    reducerData: IssueOverviewData = emptyInstance_IssueOverviewData
  ): Promise<AxiosResponse<any>> => {
    const url = addQueryParams(
      `${baseUrl}/issues/${projectId}/${page}/${rowsPerPage}`,
      converter,
      reducerData
    );
    return axios.get(url, {
      headers: getAuthHeaders(),
    });
  };

  const findAllIssuesCountForProject = (
    converter: (date: Date) => string,
    projectId: string,
    reducerData: IssueOverviewData = emptyInstance_IssueOverviewData
  ): Promise<AxiosResponse<any>> => {
    const url = addQueryParams(
      `${baseUrl}/issues/count/${projectId}`,
      converter,
      reducerData
    );
    return axios.get(url, {
      headers: getAuthHeaders(),
    });
  };

  return {
    createIssue,
    updateIssue,
    testIfIssueNameExists,
    findAllIssues,
    findAllIssuesCountForProject,
  };
};

const addQueryParams = (
  url: string,
  converter: (date: Date, lowerBound?: boolean) => string,
  reducerData: IssueOverviewData
): string => {
  let res = url;
  let delim = "?";
  if (reducerData.name) {
    res = `${res}${delim}name=${reducerData.name}`;
    delim = "&";
  }
  if (reducerData.description) {
    res = `${res}${delim}description=${reducerData.description}`;
    delim = "&";
  }

  if (reducerData.createdAtFrom) {
    res = `${res}${delim}createdAtFrom=${converter(reducerData.createdAtFrom)}`;
    delim = "&";
  }

  if (reducerData.createdAtTo) {
    res = `${res}${delim}createdAtTo=${converter(
      reducerData.createdAtTo,
      false
    )}`;
    delim = "&";
  }

  if (reducerData.lastModifiedAtFrom) {
    res = `${res}${delim}lastModifiedAtFrom=${converter(
      reducerData.lastModifiedAtFrom
    )}`;
    delim = "&";
  }

  if (reducerData.lastModifiedAtTo) {
    res = `${res}${delim}lastModifiedAtTo=${converter(
      reducerData.lastModifiedAtTo,
      false
    )}`;
    delim = "&";
  }

  if (reducerData.createdById) {
    res = `${res}${delim}createdById=${reducerData.createdById}`;
    delim = "&";
  }

  if (reducerData.assignedToId) {
    res = `${res}${delim}assignedToId=${reducerData.assignedToId}`;
    delim = "&";
  }

  if (reducerData.priorityList) {
    res = `${res}${delim}priorityList=${reducerData.priorityList.join(",")}`;
    delim = "&";
  }

  if (reducerData.severityList) {
    res = `${res}${delim}severityList=${reducerData.severityList.join(",")}`;
    delim = "&";
  }

  if (reducerData.statusList) {
    res = `${res}${delim}statusList=${reducerData.statusList.join(",")}`;
    delim = "&";
  }

  if (reducerData.labelList) {
    res = `${res}${delim}labelList=${reducerData.labelList.join(",")}`;
  }

  return res;
};
