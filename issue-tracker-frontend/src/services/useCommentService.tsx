import Axios, { AxiosResponse } from "axios";
import { Comment } from "../models/Comment";
import useBaseUrl from "../utils/useBaseUrl";
import { getAuthHeaders } from "./AuthHeadersService";

export const useCommentService = () => {
  const { baseUrl } = useBaseUrl();

  const createComment = (comment: Comment): Promise<AxiosResponse> => {
    const url = `${baseUrl}/comments`;
    return Axios.post(url, comment, {
      headers: getAuthHeaders(),
    });
  };

  const findAllCommentsForIssue = (
    issueId: string,
    page: number = 0,
    rowsPerPage: number = 0
  ): Promise<AxiosResponse<any>> => {
    const url = `${baseUrl}/comments/${issueId}/${page}/${rowsPerPage}`;
    return Axios.get(url, {
      headers: getAuthHeaders(),
    });
  };

  const findAllCommentsCountForIssue = (
    issueId: string
  ): Promise<AxiosResponse<any>> => {
    const url = `${baseUrl}/comments/count/${issueId}`;
    return Axios.get(url, {
      headers: getAuthHeaders(),
    });
  };

  return {
    createComment,
    findAllCommentsForIssue,
    findAllCommentsCountForIssue,
  };
};
