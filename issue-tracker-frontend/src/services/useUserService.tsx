import useBaseUrl from "../utils/useBaseUrl";
import Axios, { AxiosResponse } from "axios";
import { User } from "../models/User";
import { getAuthHeaders } from "./AuthHeadersService";

export const useUserService = () => {
  const { baseUrl } = useBaseUrl();

  const currentPathUrl = `${baseUrl}/users`;

  const createUser = (user: User): Promise<AxiosResponse<any>> => {
    return Axios.post(currentPathUrl, user);
  };

  const updateCurrentUser = (user: User): Promise<AxiosResponse<any>> => {
    return Axios.patch(`${currentPathUrl}/update-current-user`, user, {
      headers: getAuthHeaders(),
    });
  };

  const testIfUsernameExists = (username: string): Promise<AxiosResponse> => {
    const url = `${currentPathUrl}/username-exists/${username}`;
    return Axios.get(url);
  };

  return {
    createUser,
    updateCurrentUser,
    testIfUsernameExists,
  };
};
