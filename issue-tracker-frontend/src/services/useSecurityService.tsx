import Axios, { AxiosResponse } from "axios";
import useBaseUrl from "../utils/useBaseUrl";
import { getAuthHeaders } from "./AuthHeadersService";

export const useSecurityService = () => {
  const { baseUrl } = useBaseUrl();
  const currentPathUrl = `${baseUrl}/authentication`;

  const login = (
    username: string,
    password: string
  ): Promise<AxiosResponse> => {
    const url = `${currentPathUrl}/login`;
    return Axios.post(url, {
      username: username,
      password: password,
    });
  };

  const fetchPublicKey = (): Promise<AxiosResponse> => {
    const url = `${currentPathUrl}/public-key`;
    return Axios.get(url);
  };

  const refreshAccessTokenRemote = (): Promise<AxiosResponse> => {
    const url = `${currentPathUrl}/refresh-token`;
    return Axios.post(url, null, {
      headers: getAuthHeaders(),
    });
  };

  const logout = (token: string): Promise<AxiosResponse> => {
    const url = `${currentPathUrl}/logout`;
    return Axios.post(url, token, {
      headers: getAuthHeaders(),
    });
  };

  return {
    login,
    fetchPublicKey,
    refreshAccessTokenRemote,
    logout,
  };
};
