import Axios, { AxiosResponse } from "axios";
import {
  ProjectOverviewData,
  emptyInstance_ProjectOverviewData,
} from "../components/Project/Overview/TypesAndReducer";
import { Project } from "../models/Project";
import useBaseUrl from "../utils/useBaseUrl";
import { getAuthHeaders } from "./AuthHeadersService";

export const useProjectService = () => {
  const { baseUrl } = useBaseUrl();

  const createProject = (project: Project): Promise<AxiosResponse> => {
    const url = `${baseUrl}/projects`;
    return Axios.post(url, project, {
      headers: getAuthHeaders(),
    });
  };

  const updateProject = (project: Project): Promise<AxiosResponse> => {
    const url = `${baseUrl}/projects`;
    return Axios.patch(url, project, {
      headers: getAuthHeaders(),
    });
  };

  const addCurrentUserToProject = (
    project: Project
  ): Promise<AxiosResponse> => {
    const url = `${baseUrl}/projects/add-current-user`;
    return Axios.patch(url, project, {
      headers: getAuthHeaders(),
    });
  };

  const removeCurrentUserFromProject = (
    project: Project
  ): Promise<AxiosResponse> => {
    const url = `${baseUrl}/projects/remove-current-user`;
    return Axios.patch(url, project, {
      headers: getAuthHeaders(),
    });
  };

  const testIfProjectNameExists = (name: string): Promise<AxiosResponse> => {
    const url = `${baseUrl}/projects/name-exists/${name}`;
    return Axios.get(url, {
      headers: getAuthHeaders(),
    });
  };

  const findAllProjects = (
    converter: (date: Date, lowerBound?: boolean) => string,
    page: number = 0,
    rowsPerPage: number = 0,
    reducerData: ProjectOverviewData = emptyInstance_ProjectOverviewData
  ): Promise<AxiosResponse> => {
    const url = addQueryParams(
      `${baseUrl}/projects/${page}/${rowsPerPage}`,
      converter,
      reducerData
    );
    return Axios.get(url, {
      headers: getAuthHeaders(),
    });
  };

  const findALlProjecjtsCount = (
    converter: (date: Date, lowerBound?: boolean) => string,
    reducerData: ProjectOverviewData = emptyInstance_ProjectOverviewData
  ): Promise<AxiosResponse> => {
    const url = addQueryParams(
      `${baseUrl}/projects/count`,
      converter,
      reducerData
    );
    return Axios.get(url, {
      // credentials: 'same-origin',
      headers: getAuthHeaders(),
    });
  };

  return {
    createProject,
    updateProject,
    addCurrentUserToProject,
    removeCurrentUserFromProject,
    testIfProjectNameExists,
    findAllProjects,
    findALlProjecjtsCount,
  };
};

const addQueryParams = (
  url: string,
  converter: (date: Date, lowerBound?: boolean) => string,
  reducerData: ProjectOverviewData
): string => {
  let res = url;
  let delim = "?";
  if (reducerData.name) {
    res = `${res}${delim}name=${reducerData.name}`;
    delim = "&";
  }
  if (reducerData.description) {
    res = `${res}${delim}description=${reducerData.description}`;
    delim = "&";
  }

  if (reducerData.username) {
    res = `${res}${delim}username=${reducerData.username}`;
    delim = "&";
  }

  if (reducerData.firstname) {
    res = `${res}${delim}firstname=${reducerData.firstname}`;
    delim = "&";
  }

  if (reducerData.lastname) {
    res = `${res}${delim}lastname=${reducerData.lastname}`;
    delim = "&";
  }

  if (reducerData.createdAtFrom) {
    res = `${res}${delim}createdAtFrom=${converter(reducerData.createdAtFrom)}`;
    delim = "&";
  }

  if (reducerData.createdAtTo) {
    res = `${res}${delim}createdAtTo=${converter(
      reducerData.createdAtTo,
      false
    )}`;
    delim = "&";
  }

  if (reducerData.lastModifiedAtFrom) {
    res = `${res}${delim}lastModifiedAtFrom=${converter(
      reducerData.lastModifiedAtFrom
    )}`;
    delim = "&";
  }

  if (reducerData.lastModifiedAtTo) {
    res = `${res}${delim}lastModifiedAtTo=${converter(
      reducerData.lastModifiedAtTo,
      false
    )}`;
    delim = "&";
  }

  return res;
};
