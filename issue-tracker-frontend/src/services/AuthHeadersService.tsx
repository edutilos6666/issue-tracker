export const getAuthHeaders = () => {
  return {
    accept: "application/json",
    "content-type": "application/json",
    authorization: `Bearer ${localStorage.getItem("token")}`,
    credentials: "include",
  };
};
