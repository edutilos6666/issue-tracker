import React, { FC } from "react";
import { ProjectOverview } from "../../../components/Project/Overview/Overview";
import { RouteComponentProps } from "react-router";
import { ProjectOverviewHistoryContext } from "../../../context/HistoryContext";

interface Props extends RouteComponentProps {}

export const ProjectOverviewPage: FC<Props> = ({ history }) => {
  return (
    <ProjectOverviewHistoryContext.Provider
      value={{
        history: history,
      }}
    >
      <ProjectOverview />
    </ProjectOverviewHistoryContext.Provider>
  );
};
