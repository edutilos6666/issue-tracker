import React, { FC } from "react";
import { ProjectCreateHistoryContext } from "../../../context/HistoryContext";
import { RouteComponentProps } from "react-router";
import ProjectCreate from "../../../components/Project/Create/Create";
interface Props extends RouteComponentProps {}
const ProjectCreatePage: FC<Props> = ({ history }) => {
  return (
    <ProjectCreateHistoryContext.Provider
      value={{
        history: history,
      }}
    >
      <ProjectCreate />
    </ProjectCreateHistoryContext.Provider>
  );
};
export default ProjectCreatePage;
