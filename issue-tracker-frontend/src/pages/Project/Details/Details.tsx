import React, { FC } from "react";
import { RouteComponentProps } from "react-router";
import { ProjectDetailsHistoryContext } from "../../../context/HistoryContext";
import { ProjectDetails } from "../../../components/Project/Details/Details";

interface Props extends RouteComponentProps {}
export const ProjectDetailsPage: FC<Props> = ({ history }) => {
  return (
    <ProjectDetailsHistoryContext.Provider
      value={{
        history: history
      }}
    >
      <ProjectDetails />
    </ProjectDetailsHistoryContext.Provider>
  );
};
