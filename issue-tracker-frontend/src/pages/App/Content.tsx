import React, { FC } from "react";
import AppRouter from "./Router";
// import useAppController from '../../hooks/useAppController';
import { IntlProvider } from "react-intl";
import useAppController from "../../hooks/useAppController";
// import { ActiveTabContextProvider } from '../../context/ActiveTabContext';

const AppContent: FC = ({ children }) => {
  const { isLoggedInInit } = useAppController();
  const loggedIn = !!isLoggedInInit();

  return (
    <>
      {children}
      <IntlProvider locale="de">
        {/* <ActiveTabContextProvider> */}
        <AppRouter loggedIn={loggedIn} />
        {/* </ActiveTabContextProvider> */}
      </IntlProvider>
    </>
  );
};
export default AppContent;
