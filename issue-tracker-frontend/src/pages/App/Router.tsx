import { BrowserRouter as Router, Route } from "react-router-dom";
import React, { FC } from "react";
import { ProjectOverviewPage } from "../Project/Overview/Overview";
import { ProjectDetailsPage } from "../Project/Details/Details";
import IssueDetailsPage from "../Issue/Details/Details";
import UserDetailsPage from "../User/Details/Details";
import {
  PROJECT_DETAILS_BASE_PATH,
  ISSUE_DETAILS_BASE_PATH,
  USER_DETAILS_BASE_PATH,
  ISSUE_CREATE_BASE_PATH,
  PROJECT_CREATE_BASE_PATH,
} from "../../utils/RouterConstants";
import IssueCreatePage from "../Issue/Create/Create";
import ProjectCreatePage from "../Project/Create/Create";
import LoginPage from "../User/LoginPage/LoginPage";
import SecurityProtectedRoute from "../../components/Security/ProtectedRoute";
import RegisterPage from "../User/RegisterPage/RegisterPage";
import Header from "../../components/Header/Header";
import UserHomePage from "../User/Home/Home";

interface AppRouterInterface {
  loggedIn: boolean;
}
const AppRouter: FC<AppRouterInterface> = ({ loggedIn }) => {
  return (
    <Router>
      <Route path="/login" component={LoginPage} exact />
      <Route path="/register" component={RegisterPage} exact />
      <SecurityProtectedRoute
        loggedIn={loggedIn}
        path="/"
        component={ProjectOverviewPage}
        exact
      />

      <SecurityProtectedRoute
        loggedIn={loggedIn}
        path="/home"
        component={UserHomePage}
        exact
      />

      <SecurityProtectedRoute
        loggedIn={loggedIn}
        path={`${PROJECT_DETAILS_BASE_PATH}/:id`}
        component={ProjectDetailsPage}
        exact
      />
      <SecurityProtectedRoute
        loggedIn={loggedIn}
        path={`${ISSUE_DETAILS_BASE_PATH}/:id`}
        component={IssueDetailsPage}
        exact
      />
      <SecurityProtectedRoute
        loggedIn={loggedIn}
        path={`${USER_DETAILS_BASE_PATH}/:id`}
        component={UserDetailsPage}
        exact
      />
      <SecurityProtectedRoute
        loggedIn={loggedIn}
        path={`${ISSUE_CREATE_BASE_PATH}/:project_id`}
        component={IssueCreatePage}
        exact
      />

      <SecurityProtectedRoute
        loggedIn={loggedIn}
        path={`${PROJECT_CREATE_BASE_PATH}`}
        component={ProjectCreatePage}
        exact
      />
    </Router>
  );
};
export default AppRouter;
