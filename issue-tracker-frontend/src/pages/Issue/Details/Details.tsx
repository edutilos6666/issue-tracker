import React, { FC } from "react";
import IssueDetails from "../../../components/Issue/Details/Details";
import { IssueDetailsHistoryContext } from "../../../context/HistoryContext";
import { RouteComponentProps } from "react-router";

interface Props extends RouteComponentProps {}

const IssueDetailsPage: FC<Props> = ({ history }) => {
  return (
    <IssueDetailsHistoryContext.Provider
      value={{
        history: history
      }}
    >
      <IssueDetails />
    </IssueDetailsHistoryContext.Provider>
  );
};

export default IssueDetailsPage;
