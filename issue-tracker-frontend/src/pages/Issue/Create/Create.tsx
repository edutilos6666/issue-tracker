import React, { FC } from "react";
import { IssueCreateHistoryContext } from "../../../context/HistoryContext";
import IssueCreate from "../../../components/Issue/Create/Create";
import { RouteComponentProps } from "react-router";
interface Props extends RouteComponentProps {}
const IssueCreatePage: FC<Props> = ({ history }) => {
  return (
    <IssueCreateHistoryContext.Provider
      value={{
        history: history
      }}
    >
      <IssueCreate />
    </IssueCreateHistoryContext.Provider>
  );
};
export default IssueCreatePage;
