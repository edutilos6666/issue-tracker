import React, { FC } from "react";
import UserHome from "../../../components/User/Home/Home";

interface Props {}
const UserHomePage: FC<Props> = () => {
  return <UserHome />;
};

export default UserHomePage;
