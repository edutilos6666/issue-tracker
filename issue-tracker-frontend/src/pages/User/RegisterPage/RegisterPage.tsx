import React, { FC } from "react";
import UserCreate from "../../../components/User/Create/Create";
import { UserCreateHistroyContext } from "../../../context/HistoryContext";
import { RouteComponentProps } from "react-router";
interface Props extends RouteComponentProps {}
const RegisterPage: FC<Props> = ({ history }) => {
  return (
    <UserCreateHistroyContext.Provider
      value={{
        history: history,
      }}
    >
      <UserCreate />
    </UserCreateHistroyContext.Provider>
  );
};
export default RegisterPage;
