import React, { FC } from "react";
import { UserDetailsHistoryContext } from "../../../context/HistoryContext";
import UserDetails from "../../../components/User/Details/Details";
import { RouteComponentProps } from "react-router";

interface Props extends RouteComponentProps {}
const UserDetailsPage: FC<Props> = ({ history }) => {
  return (
    <UserDetailsHistoryContext.Provider
      value={{
        history: history
      }}
    >
      <UserDetails />
    </UserDetailsHistoryContext.Provider>
  );
};
export default UserDetailsPage;
