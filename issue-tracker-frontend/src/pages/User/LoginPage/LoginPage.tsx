import React, { FC } from "react";
import UserLogin from "../../../components/User/Login/Login";
import { UserLoginHistoryContext } from "../../../context/HistoryContext";
import { RouteComponentProps } from "react-router";
interface Props extends RouteComponentProps {}
const LoginPage: FC<Props> = ({ history }) => {
  return (
    <UserLoginHistoryContext.Provider
      value={{
        history: history,
      }}
    >
      <UserLogin location={history.location} />
    </UserLoginHistoryContext.Provider>
  );
};
export default LoginPage;
