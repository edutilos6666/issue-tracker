package org.aghayevn.web;

import org.aghayevn.dao.StatusRepository;
import org.aghayevn.model.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

/**
 * created by  Nijat Aghayev on 02.04.20
 */

@RestController
@RequestMapping(path = "/status", produces = MediaType.APPLICATION_JSON_VALUE)
public class StatusController {
    @Autowired
    private StatusRepository statusRepository;

    @GetMapping
    public Flux<Status> findAll() {
        return statusRepository.findAll();
    }
}
