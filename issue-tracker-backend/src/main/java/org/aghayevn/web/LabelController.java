package org.aghayevn.web;

import org.aghayevn.dao.LabelRepository;
import org.aghayevn.model.Label;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

/**
 * created by  Nijat Aghayev on 03.04.20
 */


@RestController
@RequestMapping(path = "/labels", produces = MediaType.APPLICATION_JSON_VALUE)
public class LabelController {
    @Autowired
    private LabelRepository labelRepository;

    @GetMapping
    public Flux<Label> findAll() {
        return labelRepository.findAll();
    }
}
