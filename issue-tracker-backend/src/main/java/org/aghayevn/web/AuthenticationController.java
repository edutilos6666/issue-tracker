package org.aghayevn.web;

import com.auth0.jwt.exceptions.JWTCreationException;
import lombok.extern.slf4j.Slf4j;
import org.aghayevn.dao.UserRepository;
import org.aghayevn.model.User;
import org.aghayevn.security.aspect.Unsecured;
import org.aghayevn.security.auth.AuthError;
import org.aghayevn.security.auth.AuthInformation;
import org.aghayevn.security.auth.AuthRequest;
import org.aghayevn.security.auth.AuthResponse;
import org.aghayevn.security.jwt.JWTReactiveAuthenticationManager;
import org.aghayevn.security.jwt.TokenProvider;
import org.aghayevn.security.model.CurrentUserHolder;
import org.aghayevn.util.CustomRedis;
import org.aghayevn.util.RSAKeyGenerator;
import org.redisson.api.RMapAsync;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.core.io.buffer.DefaultDataBufferFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.ZonedDateTime;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * created by  Nijat Aghayev on 07.04.20
 */

@Slf4j
@RestController
@RequestMapping(path = "/authentication", produces = MediaType.APPLICATION_JSON_VALUE)
public class AuthenticationController {
    @Autowired
    private TokenProvider tokenProvider;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CustomRedis customRedis;
    @Autowired
    private CurrentUserHolder currentUserHolder;
    @Autowired
    private JWTReactiveAuthenticationManager authenticationManager;
    @Autowired
    private RSAKeyGenerator rsaKeyGenerator;

    private static final Integer SESSION_EXPIRE_DELAY = 5;
    private static final String IAUTH = "iAuth";
    private static final String MAP_KEY_USER_ID = "USER_ID";
    private static final String MAP_KEY_REFRESH = "REFRESH";
    private static final String MAP_KEY_LAST_USE = "LAST_USE";
    private static final Integer MAP_TOKEN_EXPIRE_TIME_MINUTES = 5;
    private static final Integer MAP_REFRESH_EXPIRE_TIME_MINUTES = 60;
    private static final Integer PASSWORD_EXPIRE_TIME_DAYS = 90;
    private static final Integer COLUMN_SIZE_IP = 128;


    @GetMapping("/public-key")
    public Mono<String> getPublicKey() {
        try {
            Resource resource = rsaKeyGenerator.loadPublicKey();
            return DataBufferUtils.join(DataBufferUtils.read(resource, new DefaultDataBufferFactory(), (int)resource.contentLength()))
            .map(res-> res.toString(StandardCharsets.UTF_8));
//            return Mono.just(IOUtils.toString(rsaKeyGenerator.loadPublicKey().getInputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Mono.empty();
    }

    @Unsecured
    @PostMapping( "/login")
    public Mono<AuthInformation> loginWithUsernameAndPassword(@RequestBody AuthRequest request) {
        request.setTokenType(IAUTH);
        return login(request);
    }


    @Unsecured
    @PostMapping("/login-with-token")
    public Mono<AuthInformation> loginWithToken(@RequestBody String token) {
        User user = null;
        RMapAsync<String, Object> map = customRedis.getMap(token);
        Object userId = null;
        try {
            userId = map.getAsync(MAP_KEY_USER_ID).get();
            if(userId == null) {
                return Mono.just(new AuthInformation(AuthError.getInvalidLogin(), HttpStatus.FORBIDDEN.value()));
            }
        } catch (InterruptedException | ExecutionException e) {
            // TODO: replace with log.error()
            e.printStackTrace();
        }


        return userRepository.findById(Objects.requireNonNull(userId).toString()).map(userDetails-> {
            map.putAsync(MAP_KEY_LAST_USE, ZonedDateTime.now().toInstant().toEpochMilli());
            map.expireAsync(SESSION_EXPIRE_DELAY, TimeUnit.MINUTES);
            AuthResponse authResponse = AuthResponse.createAuthResponse(user.getId(), token, IAUTH);
            return new AuthInformation(authResponse, tokenProvider.getTokenFromAuthResponse(authResponse, user));
        });
    }



    @PostMapping("/refresh-token")
    public Mono<AuthInformation> refreshToken() {
        try {
//            AuthorizationToken authorizationToken = tokenProvider.renewAuthorizationTokenForCurrentUser();
            return tokenProvider
                    .renewAuthorizationTokenForCurrentUser()
                    .map(authorizationToken-> {
                        AuthResponse response = AuthResponse.createAuthResponse(currentUserHolder.getId(), null, IAUTH);
                        return new AuthInformation(response, authorizationToken.getToken());
                    });
        } catch (JWTCreationException e) {
            // TODO: replace with log.error()
            e.printStackTrace();
        }
        return Mono.just(new AuthInformation(AuthError.getExpiredLogin(), HttpStatus.FORBIDDEN.value()));
    }


    @Unsecured
    @PostMapping("/change-password")
    public Mono<AuthInformation> changePassword(@RequestBody AuthRequest authRequest) {
        final String username = authRequest.getUsername();
        final String password = authRequest.getPassword();
        final String newPassword = authRequest.getNewPassword();

        if(username == null || username.isEmpty()
                || password == null || password.isEmpty()
                || newPassword == null || newPassword.isEmpty()) {
            return Mono.just(new AuthInformation(AuthError.getBadPassword(), HttpStatus.BAD_REQUEST.value()));
        }

        return userRepository.findByUsername(username).flatMap(userDetails-> {
            if(userDetails == null)
                return Mono.just(new AuthInformation(AuthError.getUserDoesntExist(), HttpStatus.BAD_REQUEST.value()));
            if(!passwordEncoder.encode(authRequest.getPassword()).equals(userDetails.getPassword())) {
                return Mono.just(new AuthInformation(AuthError.getIncorrectPassword(), HttpStatus.BAD_REQUEST.value()));
            }
            userDetails.setPassword(passwordEncoder.encode(newPassword));
            return userRepository.save(userDetails).flatMap(savedUserDetails-> {
                authRequest.setPassword(newPassword);
                authRequest.setNewPassword(""); // not important
                return loginWithUsernameAndPassword(authRequest);
            });
        });
    }


    @PostMapping("/logout")
    public Mono<AuthInformation> logout(@RequestBody String token) {
        RMapAsync<String, Object> map = customRedis.getMap(token);
        map.fastRemoveAsync(MAP_KEY_USER_ID, MAP_KEY_REFRESH, MAP_KEY_LAST_USE);
        return currentUserHolder.reset().flatMap(niente -> Mono.just(new AuthInformation(HttpStatus.OK.value())));
    }


    private Mono<AuthInformation> login(final AuthRequest authRequest) {
        final String username = authRequest.getUsername();
        final String password = authRequest.getPassword();
        final String tokenType = authRequest.getTokenType();

        if(username == null || username.isEmpty() ||
           password == null || password.isEmpty()) {
            return Mono.just(new AuthInformation(AuthError.getMissingLogin(), HttpStatus.BAD_REQUEST.value()));
        }

        Authentication authenticationToken =
                new UsernamePasswordAuthenticationToken(username, password);

        Mono<Authentication> authentication = this.authenticationManager.authenticate(authenticationToken);
        authentication.doOnError(throwable -> {
            throw new BadCredentialsException("Bad crendentials");
        });
        ReactiveSecurityContextHolder.withAuthentication(authenticationToken);

        return authentication.flatMap(auth -> {
            return userRepository.findByUsername(username).map(user-> checkLogin(user, IAUTH));
        });
    }



    private AuthInformation checkLogin(User user, String tokenType) {
        if(user != null) {
            AuthResponse response = AuthResponse.createAuthResponse(user.getId(), null, tokenType);
            RMapAsync<String, Object> map = customRedis.getMap(response.getAccessToken());
            map.putAsync(MAP_KEY_USER_ID, user.getId()).thenAcceptAsync(f-> {
                log.info(f.toString());
            });

            map.expireAsync(MAP_TOKEN_EXPIRE_TIME_MINUTES, TimeUnit.MINUTES);

            RMapAsync<String, Object> mapRefresh = customRedis.getMap(response.getRefreshToken());
            mapRefresh.putAsync(MAP_KEY_REFRESH, response.getAccessToken());
            mapRefresh.expireAsync(MAP_REFRESH_EXPIRE_TIME_MINUTES, TimeUnit.MINUTES);

            return new AuthInformation(response, tokenProvider.getTokenFromAuthResponse(response, user));
        }

        return new AuthInformation(AuthError.getInvalidLogin(), HttpStatus.UNAUTHORIZED.value());
    }
}
