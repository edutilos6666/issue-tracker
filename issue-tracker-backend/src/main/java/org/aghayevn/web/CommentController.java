package org.aghayevn.web;

import lombok.extern.slf4j.Slf4j;
import org.aghayevn.dao.CommentRepository;
import org.aghayevn.model.Comment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * created by  Nijat Aghayev on 06.04.20
 */

@Slf4j
@RestController
@RequestMapping(path = "/comments", produces = MediaType.APPLICATION_JSON_VALUE)
public class CommentController {
    @Autowired
    private CommentRepository commentRepository;


    @PostMapping
    void create(@RequestBody final Comment comment) {
        commentRepository.save(comment).subscribe(res-> {
            log.info(res.toString());
        });
    }

    @GetMapping("/{issueId}/{page}/{rowsPerPage}")
    Flux<Comment> findBy(@PathVariable String issueId,
                         @PathVariable Long page,
                         @PathVariable Long rowsPerPage) {
        return commentRepository.findByIssueId(issueId)
                .skip(page * rowsPerPage).take(rowsPerPage);
    }

    @GetMapping("/count/{issueId}")
    Mono<Long> count(@PathVariable String issueId) {
        return commentRepository.count(issueId);
    }
}
