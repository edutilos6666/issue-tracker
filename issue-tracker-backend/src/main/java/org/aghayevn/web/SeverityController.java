package org.aghayevn.web;

import org.aghayevn.dao.SeverityRepository;
import org.aghayevn.model.Severity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

/**
 * created by  Nijat Aghayev on 02.04.20
 */

@RestController
@RequestMapping(path = "/severity", produces = MediaType.APPLICATION_JSON_VALUE)
public class SeverityController {
    @Autowired
    private SeverityRepository sever;

    @GetMapping
    public Flux<Severity> findAll() {
        return sever.findAll();
    }
}
