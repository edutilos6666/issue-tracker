package org.aghayevn.web;

import lombok.extern.slf4j.Slf4j;
import org.aghayevn.dao.ProjectRepository;
import org.aghayevn.dao.UserRepository;
import org.aghayevn.model.User;
import org.aghayevn.security.aspect.Unsecured;
import org.aghayevn.security.model.CurrentUserHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

/**
 * created by  Nijat Aghayev on 09.04.20
 */

@Slf4j
@RestController
@RequestMapping(path="/users", produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private CurrentUserHolder currentUserHolder;

    @Unsecured
    @PostMapping
    Mono<User> create(@RequestBody final User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userRepository.save(user);
    }

    @PatchMapping("/update-current-user")
    Mono<User> updateCurrentUser(@RequestBody final User input) {
        return currentUserHolder
                .getUserNonBlockingWay()
                .flatMap(user-> {
                    user.setUsername(input.getUsername());
                    user.setFirstname(input.getFirstname());
                    user.setLastname(input.getLastname());
                    user.setRoles(input.getRoles());
                    return userRepository.save(user);
                })
                .flatMap(user-> {
                    return projectRepository
                            .findByUserId(user.getId())
                            .flatMap(project-> {
                                project.getUsers().replaceAll(user1 -> {
                                    if(user1.getId().equals(user.getId())) return user;
                                    return user1;
                                });
                                return projectRepository.save(project);
                            })
                            .collectList()
                            .flatMap(projects-> {
                                return userRepository.findById(user.getId());
                            })
                            ;
                });

    }

    @GetMapping("/username-exists/{username}")
    Mono<Boolean> testIfUsernameExists(@PathVariable String username) {
        return userRepository.findCountByUsername(username).map(count -> count > 0);
    }
}
