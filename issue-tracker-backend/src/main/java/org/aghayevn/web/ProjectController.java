package org.aghayevn.web;

import lombok.extern.slf4j.Slf4j;
import org.aghayevn.dao.ProjectRepository;
import org.aghayevn.dao.UserRepository;
import org.aghayevn.model.Project;
import org.aghayevn.security.model.CurrentUserHolder;
import org.springframework.data.domain.Sort;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * created by  Nijat Aghayev on 01.04.20
 */

@Slf4j
@RestController
@RequestMapping(path = "/projects", produces = MediaType.APPLICATION_JSON_VALUE)
public class ProjectController {
    private final ProjectRepository projectRepository;
    private final UserRepository userRepository;
    private final CurrentUserHolder currentUserHolder;

    public ProjectController(
            ProjectRepository projectRepository,
            UserRepository userRepository,
            CurrentUserHolder currentUserHolder) {
        this.projectRepository = projectRepository;
        this.userRepository = userRepository;
        this.currentUserHolder = currentUserHolder;
    }


    @PostMapping
    Mono<Project> create(@RequestBody final Project project) {
        return currentUserHolder
                .getUserNonBlockingWay()
                .flatMap(user -> {
                    project.setUsers(Collections.singletonList(user));
                    return projectRepository.save(project);
                });
    }

    @PatchMapping
    Mono<Project> update(@RequestBody final Project project) {
        return projectRepository.save(project);
    }

    @PatchMapping("/remove-current-user")
    Mono<Project> removeCurrentUser(@RequestBody final Project project) {
        return currentUserHolder
                .getUserNonBlockingWay()
                .flatMap(user -> {
                    project.setUsers(
                            project
                                    .getUsers()
                                    .stream()
                                    .filter(one-> !one.getId().equals(user.getId()))
                                    .collect(Collectors.toList()));
                    return projectRepository.save(project);
                });
    }

    @PatchMapping("/add-current-user")
    Mono<Project> addCurrentUser (@RequestBody final Project project) {
        return currentUserHolder
                .getUserNonBlockingWay()
                .flatMap(user -> {
                    project.getUsers().add(user);
                    return projectRepository.save(project);
                });
    }

    @GetMapping("/name-exists/{name}")
    Mono<Boolean> testIfNameExists(@PathVariable String  name) {
        return projectRepository.findCountByName(name).map(count-> count > 0);
    }

    @GetMapping("/{page}/{rowsPerPage}")
    public Flux<Project> findBy(
            @PathVariable Long page,
            @PathVariable Long rowsPerPage,
            @RequestParam Optional<String> name,
            @RequestParam Optional<String> description,
            @RequestParam
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)  Optional<ZonedDateTime> createdAtFrom,
            @RequestParam
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Optional<ZonedDateTime> createdAtTo,
            @RequestParam
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)  Optional<ZonedDateTime> lastModifiedAtFrom,
            @RequestParam
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Optional<ZonedDateTime> lastModifiedAtTo,
            @RequestParam Optional<String> username,
            @RequestParam Optional<String> firstname,
            @RequestParam Optional<String> lastname
    ) {
        return projectRepository.findBy(
                name.orElse(""),
                description.orElse(""),
                username.orElse(""),
                firstname.orElse(""),
                lastname.orElse(""),
                createdAtFrom.orElse(null),
                createdAtTo.orElse(null),
                lastModifiedAtFrom.orElse(null),
                lastModifiedAtTo.orElse(null),
                Sort.by(Sort.Order.desc("lastModifiedAt")))
                .skip(page * rowsPerPage).take(rowsPerPage);
    }


    @GetMapping("/count")
    public Mono<Long> count(
            @RequestParam Optional<String> name,
            @RequestParam Optional<String> description,
            @RequestParam
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)  Optional<ZonedDateTime> createdAtFrom,
            @RequestParam
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Optional<ZonedDateTime> createdAtTo,
            @RequestParam
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)  Optional<ZonedDateTime> lastModifiedAtFrom,
            @RequestParam
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Optional<ZonedDateTime> lastModifiedAtTo,
            @RequestParam Optional<String> username,
            @RequestParam Optional<String> firstname,
            @RequestParam Optional<String> lastname
    ) {
        return projectRepository.count(
                name.orElse(""),
                description.orElse(""),
                username.orElse(""),
                firstname.orElse(""),
                lastname.orElse(""),
                createdAtFrom.orElse(null),
                createdAtTo.orElse(null),
                lastModifiedAtFrom.orElse(null),
                lastModifiedAtTo.orElse(null));
    }


}
