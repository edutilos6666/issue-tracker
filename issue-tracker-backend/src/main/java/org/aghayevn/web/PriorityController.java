package org.aghayevn.web;

import org.aghayevn.dao.PriorityRepository;
import org.aghayevn.model.Priority;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

/**
 * created by  Nijat Aghayev on 02.04.20
 */

@RestController
@RequestMapping(path = "/priorities", produces = MediaType.APPLICATION_JSON_VALUE)
public class PriorityController {
    @Autowired
    private PriorityRepository priorityRepository;

    @GetMapping
    public Flux<Priority> findAll() {
        return priorityRepository.findAll();
    }
}
