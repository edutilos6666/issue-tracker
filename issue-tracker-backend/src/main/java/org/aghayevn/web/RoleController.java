package org.aghayevn.web;

import org.aghayevn.dao.RoleRepository;
import org.aghayevn.model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

/**
 * created by  Nijat Aghayev on 14.04.20
 */


@RestController
@RequestMapping(path = "/roles", produces = MediaType.APPLICATION_JSON_VALUE)
public class RoleController {
    @Autowired
    private RoleRepository roleRepository;

    @GetMapping
    Flux<Role> findAll() {
        return roleRepository.findAll();
    }
}
