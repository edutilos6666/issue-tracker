package org.aghayevn.web;

import lombok.extern.slf4j.Slf4j;
import org.aghayevn.dao.IssueRepository;
import org.aghayevn.dao.LabelRepository;
import org.aghayevn.dao.ProjectRepository;
import org.aghayevn.model.Issue;
import org.aghayevn.model.Label;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * created by  Nijat Aghayev on 01.04.20
 */

@Slf4j
@RestController
@RequestMapping(path = "/issues", produces = MediaType.APPLICATION_JSON_VALUE)
public class IssueController {
    @Autowired
    private IssueRepository issueRepository;
    @Autowired
    private LabelRepository labelRepository;
    @Autowired
    private ProjectRepository projectRepository;


    @GetMapping("/name-exists/{name}")
    Mono<Boolean> testIfNameExists(@PathVariable String  name) {
        return issueRepository.findCountByName(name).map(count-> count > 0);
    }

    @PostMapping
    void create(@RequestBody final Issue issue) {
        if(!issue.getLabels().isEmpty())
          labelRepository.saveAll(issue.getLabels()).subscribe();

        issueRepository.save(issue).subscribe(res-> {
            log.info(res.toString());
        });

        projectRepository.findById(issue.getProjectId()).subscribe(res-> {
           res.setLastModifiedAt(ZonedDateTime.now());
           projectRepository.save(res).subscribe();
        });
    }

    @PatchMapping
    void update(@RequestBody final Issue issue) {
      List<Label> labels = issue.getLabels();

      for(int i = 0; i < labels.size(); ++i) {
          Label one = labels.get(i);

          if(one.getId() == null) {
              one.setName(one.getName().toLowerCase());
              one.setId(one.getName());
              final int index = i;
              labelRepository.save(one).subscribe(res-> {
                  labels.set(index, res);
              });
          }

      }

      issue.setLabels(labels);
      issueRepository.save(issue).subscribe(res-> {
          log.info(res.toString());
      });

     projectRepository.findById(issue.getProjectId()).subscribe(res-> {
        res.setLastModifiedAt(ZonedDateTime.now());
        projectRepository.save(res).subscribe();
     });
    }

    @GetMapping
    public Flux<Issue> findAll() {
        return issueRepository.findAll();
    }


    // ISO.DATE_TIME:   2015-09-26T01:30:00.120
//    http://localhost:8765/api/issue-tracker/issues/2/0/5?name=frontend&description=issue&createdBy=1&assignedTo=2&createdAtFrom=2010-10-10T00:00:00&createdAtTo=2022-10-10T00:00:00&priorityList=1,2,3,4&severityList=1,2,3,4&statusList=1,2,3&labelList=1,2,3,4,5,6,7
    @GetMapping("/{projectId}/{page}/{rowsPerPage}")
    public Flux<Issue> findBy(
            @PathVariable String projectId,
            @PathVariable Long page,
            @PathVariable Long rowsPerPage,
            @RequestParam Optional<String> name,
            @RequestParam Optional<String> description,
            @RequestParam
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)  Optional<ZonedDateTime> createdAtFrom,
            @RequestParam
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Optional<ZonedDateTime> createdAtTo,
            @RequestParam
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)  Optional<ZonedDateTime> lastModifiedAtFrom,
            @RequestParam
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Optional<ZonedDateTime> lastModifiedAtTo,
            @RequestParam Optional<String> createdById,
            @RequestParam Optional<String> assignedToId,
            @RequestParam Optional<List<String>> priorityList,
            @RequestParam Optional<List<String>> severityList,
            @RequestParam Optional<List<String>> statusList,
            @RequestParam Optional<List<String>> labelList
            ) {
       return issueRepository.findBy(
                projectId,
                name.orElse(""),
                description.orElse(""),
                createdAtFrom.orElse(null),
                createdAtTo.orElse(null),
                lastModifiedAtFrom.orElse(null),
                lastModifiedAtTo.orElse(null),
                createdById.orElse(null),
                assignedToId.orElse(null),
                priorityList.orElse(Collections.emptyList()),
                severityList.orElse(Collections.emptyList()),
                statusList.orElse(Collections.emptyList()),
                labelList.orElse(Collections.emptyList()),
                Sort.by(Sort.Order.desc("lastModifiedAt")))
                .skip(page * rowsPerPage).take(rowsPerPage);
    }

    @GetMapping("/count/{projectId}")
    public Mono<Long> count(
            @PathVariable String projectId,
            @RequestParam Optional<String> name,
            @RequestParam Optional<String> description,
            @RequestParam
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)  Optional<ZonedDateTime> createdAtFrom,
            @RequestParam
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Optional<ZonedDateTime> createdAtTo,
            @RequestParam
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)  Optional<ZonedDateTime> lastModifiedAtFrom,
            @RequestParam
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Optional<ZonedDateTime> lastModifiedAtTo,
            @RequestParam Optional<String> createdById,
            @RequestParam Optional<String> assignedToId,
            @RequestParam Optional<List<String>> priorityList,
            @RequestParam Optional<List<String>> severityList,
            @RequestParam Optional<List<String>> statusList,
            @RequestParam Optional<List<String>> labelList
    ) {
       return issueRepository.count(
               projectId,
               name.orElse(""),
               description.orElse(""),
               createdAtFrom.orElse(null),
               createdAtTo.orElse(null),
               lastModifiedAtFrom.orElse(null),
               lastModifiedAtTo.orElse(null),
               createdById.orElse(null),
               assignedToId.orElse(null),
               priorityList.orElse(Collections.emptyList()),
               severityList.orElse(Collections.emptyList()),
               statusList.orElse(Collections.emptyList()),
               labelList.orElse(Collections.emptyList())
       );
    }
}
