package org.aghayevn.dao;

import org.aghayevn.model.Role;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

/**
 * created by  Nijat Aghayev on 01.04.20
 */
public interface RoleRepository extends ReactiveCrudRepository<Role, String> {
}
