package org.aghayevn.dao;

import org.aghayevn.model.Issue;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.ZonedDateTime;
import java.util.List;

/**
 * created by  Nijat Aghayev on 01.04.20
 */
public interface IssueRepository extends ReactiveCrudRepository<Issue, String> {

//    db.issue.find({
//        name: {$ne: null, $regex: /frontend/i},
//        description: {$ne: null, $regex: /for/i},
//        createdAt: {$gte: ISODate("2010-10-10"), $lte: ISODate("2022-10-10")},
//        "createdBy._id": {$ne: null, $eq: "2" },
//        "createdBy.username": {$ne: null, $regex: /pako/},
//        "createdBy.password": {$ne: null, $regex: /DsUep/},
//        "assignedTo._id": {$ne: null, $eq: "1"},
//        "priority._id": {$in: ["1", "2", "3"]},
//        "status._id": {$in: ["2", "3"]},
//        "severity._id": {$in: ["1", "4", "5"]},
//        "labels._id": {$in: ["1", "2", "3"]}
//    })


//    db.issue.find({
//        name: {$ne: null, $regex: /frontend/i},
//        $and: [{$where: "'name' != null"}, {description: {$ne: null, $regex: /for/i}}]
//    })


//    db.issue.find({
//        "projectId" : { "$ne" : null, "$eq" : "2"},
//        "name" : { "$ne" : null, "$regex" : "frontend", "$options" : "i"},
//        "description" : { "$ne" : null, "$regex" : "frontend", "$options" : "i"},
//        "$or" : [{ "$where" : "\"null\" == \"null\""}, { "createdAt" : { "$ne" : null, "$gte" : null}}]
//    })

    @Query("{ $and: [" +
            "{projectId: {$ne: null, $eq: ?0}}," +
            "{$or: [{$where: '\"?1\" == \"\"'}, {name: {$ne: null, $regex: ?1, $options: 'i'}}]}," +
            "{$or: [{$where: '\"?2\" == \"\"'}, {description: {$ne: null, $regex: ?2, $options: 'i'}}]}," +
            "{$or: [{$where: '\"?3\" == \"null\"'}, {createdAt: {$ne: null, $gte: ?3}}]}," +
            "{$or: [{$where: '\"?4\" == \"null\"'}, {createdAt: {$ne: null, $lte: ?4}}]}," +
            "{$or: [{$where: '\"?5\" == \"null\"'}, {createdAt: {$ne: null, $gte: ?5}}]}," +
            "{$or: [{$where: '\"?6\" == \"null\"'}, {createdAt: {$ne: null, $lte: ?6}}]}," +
            "{$or: [{$where: '\"?7\" == \"null\"'}, {'createdBy._id': {$ne: null, $eq: ?7}}]}," +
            "{$or: [{$where: '\"?8\" == \"null\"'}, {'assignedTo._id': {$ne: null, $eq: ?8}}]}," +
            "{$or: [{$where: '\"?9\" == \"[]\"'}, {'priority._id': {$ne: null, $in: ?9}}]}," +
            "{$or: [{$where: '\"?10\" == \"[]\"'}, {'severity._id': {$ne: null, $in: ?10}}]}," +
            "{$or: [{$where: '\"?11\" == \"[]\"'}, {'status._id': {$ne: null, $in: ?11}}]}," +
            "{$or: [{$where: '\"?12\" == \"[]\"'}, {'labels._id': {$ne: null, $in: ?12}}]}," +
            "]}")
    Flux<Issue> findBy(
            String projectId,
            String name,
            String description,
            ZonedDateTime createdAtFrom,
            ZonedDateTime createdAtTo,
            ZonedDateTime lastModifiedAtFrom,
            ZonedDateTime lastModifiedAtTo,
            String createdById,
            String assignedToId,
            List<String> priorityList,
            List<String> severityList,
            List<String> statusList,
            List<String> labelList,
            Sort sort);



    @Query(value = "{ $and: [" +
            "{projectId: {$ne: null, $eq: ?0}}," +
            "{$or: [{$where: '\"?1\" == \"\"'}, {name: {$ne: null, $regex: ?1, $options: 'i'}}]}," +
            "{$or: [{$where: '\"?2\" == \"\"'}, {description: {$ne: null, $regex: ?2, $options: 'i'}}]}," +
            "{$or: [{$where: '\"?3\" == \"null\"'}, {createdAt: {$ne: null, $gte: ?3}}]}," +
            "{$or: [{$where: '\"?4\" == \"null\"'}, {createdAt: {$ne: null, $lte: ?4}}]}," +
            "{$or: [{$where: '\"?5\" == \"null\"'}, {createdAt: {$ne: null, $gte: ?5}}]}," +
            "{$or: [{$where: '\"?6\" == \"null\"'}, {createdAt: {$ne: null, $lte: ?6}}]}," +
            "{$or: [{$where: '\"?7\" == \"null\"'}, {'createdBy._id': {$ne: null, $eq: ?7}}]}," +
            "{$or: [{$where: '\"?8\" == \"null\"'}, {'assignedTo._id': {$ne: null, $eq: ?8}}]}," +
            "{$or: [{$where: '\"?9\" == \"[]\"'}, {'priority._id': {$ne: null, $in: ?9}}]}," +
            "{$or: [{$where: '\"?10\" == \"[]\"'}, {'severity._id': {$ne: null, $in: ?10}}]}," +
            "{$or: [{$where: '\"?11\" == \"[]\"'}, {'status._id': {$ne: null, $in: ?11}}]}," +
            "{$or: [{$where: '\"?12\" == \"[]\"'}, {'labels._id': {$ne: null, $in: ?12}}]}," +
            "]}", count = true)
    Mono<Long> count(
            String projectId,
            String name,
            String description,
            ZonedDateTime createdAtFrom,
            ZonedDateTime createdAtTo,
            ZonedDateTime lastModifiedAtFrom,
            ZonedDateTime lastModifiedAtTo,
            String createdById,
            String assignedToId,
            List<String> priorityList,
            List<String> severityList,
            List<String> statusList,
            List<String> labelList);


    @Query(value = "{name: {$ne: null, $regex: '^?0$', $options: 'i'}}", count = true)
    Mono<Long> findCountByName(String issueName);


}
