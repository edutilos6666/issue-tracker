package org.aghayevn.dao;

import org.aghayevn.model.User;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Mono;

/**
 * created by  Nijat Aghayev on 01.04.20
 */
public interface UserRepository extends ReactiveCrudRepository<User, String> {
    Mono<User> findByUsernameIgnoreCase(String username);
    Mono<User> findByUsername(String username);

    @Query(value = "{username: {$ne: null, $regex: '^?0$', $options: 'i'}}", count = true)
    Mono<Long> findCountByUsername(String username);
}
