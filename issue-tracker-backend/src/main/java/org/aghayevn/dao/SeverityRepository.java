package org.aghayevn.dao;

import org.aghayevn.model.Severity;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

/**
 * created by  Nijat Aghayev on 01.04.20
 */
public interface SeverityRepository extends ReactiveCrudRepository<Severity, String> {
}
