package org.aghayevn.dao;

import org.aghayevn.model.Label;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

/**
 * created by  Nijat Aghayev on 01.04.20
 */
public interface LabelRepository extends ReactiveCrudRepository<Label, String> {
}
