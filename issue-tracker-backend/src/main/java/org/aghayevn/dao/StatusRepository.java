package org.aghayevn.dao;

import org.aghayevn.model.Status;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

/**
 * created by  Nijat Aghayev on 01.04.20
 */
public interface StatusRepository extends ReactiveCrudRepository<Status, String> {
}
