package org.aghayevn.dao;

import org.aghayevn.model.Priority;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

/**
 * created by  Nijat Aghayev on 01.04.20
 */
public interface PriorityRepository extends ReactiveCrudRepository<Priority, String> {
}
