package org.aghayevn.dao;

import org.aghayevn.model.Comment;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * created by  Nijat Aghayev on 01.04.20
 */
public interface CommentRepository extends ReactiveCrudRepository<Comment, String> {
    Flux<Comment> findByIssueId(String issueId);

    @Query(value =  "{issueId: {$ne: null, $eq: ?0}}", count = true)
    Mono<Long> count(String issueId);
}
