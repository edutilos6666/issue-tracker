package org.aghayevn.dao;

import org.aghayevn.model.Project;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.ZonedDateTime;

/**
 * created by  Nijat Aghayev on 01.04.20
 */
public interface ProjectRepository extends ReactiveCrudRepository<Project, String> {

    @Query("{ $and: [" +
            "{$or: [{$where: '\"?0\" == \"\"'}, {name: {$ne: null, $regex: ?0, $options: 'i'}}]}," +
            "{$or: [{$where: '\"?1\" == \"\"'}, {description: {$ne: null, $regex: ?1, $options: 'i'}}]}," +
            "{$or: [{$where: '\"?2\" == \"\"'}, {'users.username': {$ne: null, $regex: ?2, $options: 'i'}}]}," +
            "{$or: [{$where: '\"?3\" == \"\"'}, {'users.firstname': {$ne: null, $regex: ?3, $options: 'i'}}]}," +
            "{$or: [{$where: '\"?4\" == \"\"'}, {'users.lastname': {$ne: null, $regex: ?4, $options: 'i'}}]}," +
            "{$or: [{$where: '\"?5\" == \"null\"'}, {createdAt: {$ne: null, $gte: ?5}}]}," +
            "{$or: [{$where: '\"?6\" == \"null\"'}, {createdAt: {$ne: null, $lte: ?6}}]}," +
            "{$or: [{$where: '\"?7\" == \"null\"'}, {lastModifiedAt: {$ne: null, $gte: ?7}}]}," +
            "{$or: [{$where: '\"?8\" == \"null\"'}, {lastModifiedAt: {$ne: null, $lte: ?8}}]}," +
            "]}")
    Flux<Project> findBy(
            String name,
            String description,
            String username,
            String firstname,
            String lastname,
            ZonedDateTime createdAtFrom,
            ZonedDateTime createdAtTo,
            ZonedDateTime lastModifiedAtFrom,
            ZonedDateTime lastModifiedAtTo,
            Sort sort
    );

    @Query(value = "{ $and: [" +
            "{$or: [{$where: '\"?0\" == \"\"'}, {name: {$ne: null, $regex: ?0, $options: 'i'}}]}," +
            "{$or: [{$where: '\"?1\" == \"\"'}, {description: {$ne: null, $regex: ?1, $options: 'i'}}]}," +
            "{$or: [{$where: '\"?2\" == \"\"'}, {'users.username': {$ne: null, $regex: ?2, $options: 'i'}}]}," +
            "{$or: [{$where: '\"?3\" == \"\"'}, {'users.firstname': {$ne: null, $regex: ?3, $options: 'i'}}]}," +
            "{$or: [{$where: '\"?4\" == \"\"'}, {'users.lastname': {$ne: null, $regex: ?4, $options: 'i'}}]}," +
            "{$or: [{$where: '\"?5\" == \"null\"'}, {createdAt: {$ne: null, $gte: ?5}}]}," +
            "{$or: [{$where: '\"?6\" == \"null\"'}, {createdAt: {$ne: null, $lte: ?6}}]}," +
            "{$or: [{$where: '\"?7\" == \"null\"'}, {lastModifiedAt: {$ne: null, $gte: ?7}}]}," +
            "{$or: [{$where: '\"?8\" == \"null\"'}, {lastModifiedAt: {$ne: null, $lte: ?8}}]}," +
            "]}", count = true)
    Mono<Long> count(
            String name,
            String description,
            String username,
            String firstname,
            String lastname,
            ZonedDateTime createdAtFrom,
            ZonedDateTime createdAtTo,
            ZonedDateTime lastModifiedAtFrom,
            ZonedDateTime lastModifiedAtTo
    );


   @Query(value = "{name: {$ne: null, $regex: '^?0$', $options: 'i'}}", count = true)
   Mono<Long> findCountByName(String projectName);


   @Query("{'users._id': {$eq: ?0}}")
   Flux<Project> findByUserId(String id);

}
