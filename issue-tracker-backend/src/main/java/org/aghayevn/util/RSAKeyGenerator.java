package org.aghayevn.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.poi.util.IOUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.security.KeyFactory;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

/**
 * created by  Nijat Aghayev on 08.04.20
 */

@Slf4j
@Component
public class RSAKeyGenerator {
    private RSAPublicKey publicKey;
    private RSAPrivateKey privateKey;

    public Resource loadPrivateKey() {
        return new ClassPathResource("keys/issue_tracker.pkcs8");
    }
    public Resource loadPublicKey() {
        return new ClassPathResource("keys/issue_tracker.pub");
    }


    public RSAPrivateKey getPrivateKey() {
        if (privateKey == null) {
            try {
                InputStream privateKeyStream = loadPrivateKey().getInputStream();
                String privateKeyContent = new String(IOUtils.toByteArray(privateKeyStream));
                privateKeyContent = privateKeyContent.replaceAll("\\r", "").replaceAll("\\n", "").replace("-----BEGIN PRIVATE KEY-----", "").replace("-----END PRIVATE KEY-----", "");
                KeyFactory kf = KeyFactory.getInstance("RSA");

                PKCS8EncodedKeySpec keySpecPKCS8 = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(privateKeyContent));
                privateKey = (RSAPrivateKey) kf.generatePrivate(keySpecPKCS8);
            } catch (Exception e) {
                log.error("Failed to initialize key pairs", e);
            }

        }
        return privateKey;
    }




    public RSAPublicKey getPublicKey() {
        if (publicKey == null) {
            try {
                InputStream publicKeyStream = loadPublicKey().getInputStream();
                String publicKeyContent = new String(IOUtils.toByteArray(publicKeyStream));

                publicKeyContent = publicKeyContent.replaceAll("\\r", "").replaceAll("\\n", "").replace("-----BEGIN PUBLIC KEY-----", "").replace("-----END PUBLIC KEY-----", "");
                KeyFactory kf = KeyFactory.getInstance("RSA");
                X509EncodedKeySpec keySpecX509 = new X509EncodedKeySpec(Base64.getDecoder().decode(publicKeyContent));
                publicKey = (RSAPublicKey) kf.generatePublic(keySpecX509);
            } catch (Exception e) {
                log.error("Failed to initialize key pairs", e);
            }

        }
        return publicKey;
    }
}
