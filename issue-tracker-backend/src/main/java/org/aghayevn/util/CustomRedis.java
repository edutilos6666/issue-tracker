package org.aghayevn.util;

import lombok.NoArgsConstructor;
import org.redisson.Redisson;
import org.redisson.api.RMapAsync;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * created by  Nijat Aghayev on 08.04.20
 */

@Component
@Scope("singleton")
@NoArgsConstructor
public class CustomRedis {
    @Value("${application.redis.path}")
    private String defaultHost;
    private RedissonClient client;

    @PostConstruct
    public void init() {
        Config config = new Config();
        String redisHost = System.getenv("REDIS_URL");
        if(redisHost == null || redisHost.isEmpty()) {
            redisHost = defaultHost;
        }
        config.useSingleServer().setAddress(redisHost);
        client = Redisson.create(config);
    }

    public <T, U>RMapAsync<T, U> getMap(String name) {
        if(client == null) init();
        return client.getMap(name);
    }
}
