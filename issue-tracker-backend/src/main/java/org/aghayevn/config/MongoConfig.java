package org.aghayevn.config;

import com.mongodb.reactivestreams.client.MongoClients;
import com.mongodb.reactivestreams.client.MongoClient;
import org.aghayevn.converter.ZonedDateTimeReadConverter;
import org.aghayevn.converter.ZonedDateTimeWriteConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.CustomConversions;
import org.springframework.data.mongodb.config.AbstractReactiveMongoConfiguration;
import org.springframework.data.mongodb.core.convert.MongoCustomConversions;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import java.util.ArrayList;
import java.util.List;

/**
 * created by  Nijat Aghayev on 06.04.20
 */

@Configuration
@EnableMongoRepositories(basePackages = "org.aghayevn")
public class MongoConfig extends AbstractReactiveMongoConfiguration {
    private final List<Converter<?,?>> converters = new ArrayList<>();
    @Autowired
    private ZonedDateTimeReadConverter zonedDateTimeReadConverter;
    @Autowired
    private ZonedDateTimeWriteConverter zonedDateTimeWriteConverter;

    @Override
    protected String getDatabaseName() {
        return "issue-tracker";
    }

    @Override
    public CustomConversions customConversions() {
        converters.add(zonedDateTimeReadConverter);
        converters.add(zonedDateTimeWriteConverter);
        return new MongoCustomConversions(converters);
    }

    @Override
    public MongoClient reactiveMongoClient() {
        return MongoClients.create();
    }
}
