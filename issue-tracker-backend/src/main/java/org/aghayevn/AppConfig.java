package org.aghayevn;

import lombok.extern.slf4j.Slf4j;
import org.aghayevn.dao.*;
import org.aghayevn.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * created by  Nijat Aghayev on 01.04.20
 */

@Slf4j
@Configuration
public class AppConfig {
//    @Bean
//    public MongoClient mongoClient() {
//        return MongoClients.create();
//    }

    @Autowired
    private PasswordEncoder passwordEncoder;

    private void incrementFlagCounter(List<Integer> flagCounter) {
        flagCounter.set(0, flagCounter.get(0)+1);
    }
    @Bean
    public CommandLineRunner loadData(
            StatusRepository statusRepository,
            RoleRepository roleRepository,
            PriorityRepository priorityRepository,
            SeverityRepository severityRepository,
            IssueRepository issueRepository,
            UserRepository userRepository,
            LabelRepository labelRepository,
            CommentRepository commentRepository,
            ProjectRepository projectRepository
    ) {
        return (args)-> {
            // clear up all collections
            List<Integer> flagCounter = Arrays.asList(0);
            statusRepository.deleteAll().subscribe(res-> {
               incrementFlagCounter(flagCounter);
            });
            roleRepository.deleteAll().subscribe(res-> {
                incrementFlagCounter(flagCounter);
            });
            priorityRepository.deleteAll().subscribe(res-> {
                incrementFlagCounter(flagCounter);
            });
            severityRepository.deleteAll().subscribe(res-> {
                incrementFlagCounter(flagCounter);
            });
            issueRepository.deleteAll().subscribe(res-> {
                incrementFlagCounter(flagCounter);
            });
            userRepository.deleteAll().subscribe(res-> {
                incrementFlagCounter(flagCounter);
            });
            labelRepository.deleteAll().subscribe(res-> {
                incrementFlagCounter(flagCounter);
            });
            commentRepository.deleteAll().subscribe(res-> {
                incrementFlagCounter(flagCounter);
            });
            projectRepository.deleteAll().subscribe(res-> {
                incrementFlagCounter(flagCounter);
            });


//            while(flagCounter.get(0) < 9);

            statusRepository
                    .saveAll(Status.getAllPredefinedStatus())
                    .subscribe(c-> {
                        log.info(c.getName());
                    });
            roleRepository.saveAll(Role.getAllPredefinedRoles())
                    .subscribe(c-> {
                        log.info(c.getName());
                    });

            priorityRepository.saveAll(Priority.getAllPredefinedPriorities())
                    .subscribe(c-> {
                        log.info(c.getName());
                    });

            severityRepository.saveAll(Severity.getAllPredefinedSeverities())
                    .subscribe(c-> {
                       log.info(c.getName());
                    });


            Role savedRoleDeveloper = roleRepository.findById(Role.ROLE_DEVELOPER.getId()).block();
            Role savedRoleTeamLeader = roleRepository.findById(Role.ROLE_TEAM_LEADER.getId()).block();
            Role savedRoleAdmin = roleRepository.findById(Role.ROLE_ADMIN.getId()).block();

            User user1 = new User("1", "foobar", passwordEncoder.encode("foobar"), "foo", "bar", Collections.singletonList(savedRoleDeveloper));
            User user2 = new User("2", "pako", passwordEncoder.encode("pako"), "pa", "ko", Collections.singletonList(savedRoleTeamLeader));


            User userAdmin = new User("2", "admin", passwordEncoder.encode("admin"), "admin", "admin", Collections.singletonList(savedRoleAdmin));

            userRepository.saveAll(Arrays.asList(user1, user2, userAdmin))
                    .subscribe(c-> {
                       log.info(c.getUsername());
                    });


            Label labelBackend = new Label("1", "Backend");
            Label labelFrontend = new Label("2", "Frontend");

            labelRepository.saveAll(Arrays.asList(labelBackend, labelFrontend))
                    .subscribe(c-> {
                        log.info(c.getName());
                    });

            Project projectElasticSearchExample = new Project("1", "ElasticSearch Example", "Description for ElasticSearch Example");
            Project project2 = new Project("2", "ElasticSearch Example 2", "Description for ElasticSearch Example 2");
            Project project3 = new Project("3", "ElasticSearch Example 3", "Description for ElasticSearch Example 3");
            Project project4 = new Project("4", "ElasticSearch Example 4", "Description for ElasticSearch Example 4");
            Project project5 = new Project("5", "ElasticSearch Example 5", "Description for ElasticSearch Example 5");
            Arrays.asList(project2, project3, project4, project5).forEach(one-> {
                one.setUsers(Arrays.asList(user1, user2));
            });

            projectRepository.saveAll(Arrays.asList(projectElasticSearchExample,
                    project2,
                    project3,
                    project4,
                    project5
                    ))
                .subscribe(c-> {
                    log.info(c.getName());
                });

            Project savedProjectElasticSearchExample = projectRepository.findById("1").block();
            User savedUserFoobar = userRepository.findById("1").block();
            User savedUserPako = userRepository.findById("2").block();

            Status savedStatusOpen = statusRepository.findById("1").block();
            Status savedStatusClosed = statusRepository.findById("2").block();

            Priority savedPriorityUrgent = priorityRepository.findById("1").block();
            Priority savedPriorityMedium = priorityRepository.findById("3").block();

            Severity savedSeverityBlocker = severityRepository.findById("1").block();
            Severity savedSeverityMinor = severityRepository.findById("4").block();

            Label savedLabelBackend = labelRepository.findById("1").block();
            Label savedLabelFrontend = labelRepository.findById("2").block();


            Issue issue1 = getIssue(savedUserFoobar, savedUserPako, savedStatusOpen, savedPriorityUrgent, savedSeverityBlocker, savedLabelBackend,
                    "1","1", "Backend Issue", "Description For Backend Issue");

            Issue issue2 = getIssue(savedUserPako, savedUserFoobar, savedStatusClosed, savedPriorityMedium, savedSeverityMinor, savedLabelFrontend,
                    "2","2", "Frontend Issue", "Description For Frontend Issue");


            issueRepository.saveAll(Arrays.asList(issue1, issue2))
                    .subscribe(c-> {
                       log.info(c.getName());
                    });

            for(int i=3; i< 100; ++i) {
                issueRepository.save(getIssue(savedUserFoobar, savedUserPako, savedStatusOpen, savedPriorityUrgent, savedSeverityBlocker, savedLabelBackend,
                        i+"","1", "Backend Issue "+ i, "Description For Backend Issue "+ i)).subscribe();
            }

            Issue savedIssue1 = issueRepository.findById("1").block();
            Issue savedIssue2 = issueRepository.findById("2").block();

            while(savedIssue1 == null) {
                savedIssue1 = issueRepository.findById("1").block();
            }

            while(savedIssue2 == null) {
                savedIssue2 = issueRepository.findById("2").block();
            }

            Comment comment1ForIssue1 = new Comment("1", savedIssue1.getId(),  savedUserFoobar, "Comment1 for issue1", ZonedDateTime.now());
            Comment comment2ForIssue1 = new Comment("2", savedIssue1.getId(), savedUserPako, "Comment2 for issue1", ZonedDateTime.now());

            Comment comment1ForIssue2 = new Comment("3", savedIssue2.getId(), savedUserFoobar, "Comment1 for issue2", ZonedDateTime.now());
            Comment comment2ForIssue2 = new Comment("4", savedIssue2.getId(), savedUserPako, "Comment2 for issue2", ZonedDateTime.now());

            commentRepository.saveAll(Arrays.asList(comment1ForIssue1, comment2ForIssue1, comment1ForIssue2, comment2ForIssue2))
                .subscribe(c-> {
                    log.info(c.getContent());
                });

//            Comment savedComment1ForIssue1 = commentRepository.findById("1").block();
//            Comment savedComment2ForIssue1 = commentRepository.findById("2").block();
//            Comment savedComment1ForIssue2 = commentRepository.findById("3").block();
//            Comment savedComment2ForIssue2 = commentRepository.findById("4").block();

//            savedIssue1.setComments(Arrays.asList(savedComment1ForIssue1, savedComment2ForIssue1));
//            savedIssue2.setComments(Arrays.asList(savedComment1ForIssue2, savedComment2ForIssue2));
//            issueRepository.saveAll(Arrays.asList(savedIssue1, savedIssue2))
//                .subscribe(c-> {
//                    log.info(c.getName());
//                });


            savedIssue1 = issueRepository.findById("1").block();
            savedIssue2 = issueRepository.findById("2").block();

//            savedProjectElasticSearchExample.setIssues(Arrays.asList(savedIssue1, savedIssue2));
            while(savedProjectElasticSearchExample == null);
            savedProjectElasticSearchExample.setUsers(Arrays.asList(savedUserFoobar, savedUserPako));

            projectRepository.save(savedProjectElasticSearchExample).subscribe();
        };
    }

    private Issue getIssue(User savedUserFoobar, User savedUserPako, Status savedStatusOpen, Priority savedPriorityUrgent, Severity savedSeverityBlocker, Label savedLabelBackend,
                           String id, String projectId, String name, String description) {
        return new Issue(id, projectId, name, description,
                ZonedDateTime.now(), ZonedDateTime.now(), savedUserFoobar, savedUserPako, savedPriorityUrgent,
                savedSeverityBlocker, savedStatusOpen, Arrays.asList(savedLabelBackend));
    }
}
