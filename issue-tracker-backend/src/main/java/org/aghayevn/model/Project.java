package org.aghayevn.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * created by  Nijat Aghayev on 01.04.20
 */

@Document
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Project {
    @Id
    private String id;
    private String name;
    private String description;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private ZonedDateTime createdAt;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private ZonedDateTime lastModifiedAt;
    private List<User> users;
//    private List<Issue> issues;


    public Project(String id, String name, String description) {
        this(id, name, description, ZonedDateTime.now(), ZonedDateTime.now(), new ArrayList<>());
    }
}
