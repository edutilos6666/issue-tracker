package org.aghayevn.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.ZonedDateTime;
import java.util.List;

/**
 * created by  Nijat Aghayev on 01.04.20
 */

@Document
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Issue {
    @Id
    private String id;
    private String projectId;
    private String name;
    private String description;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private ZonedDateTime createdAt;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private ZonedDateTime lastModifiedAt;
    private User createdBy;
    private User assignedTo;
    private Priority priority;
    private Severity severity;
    private Status status;
    private List<Label> labels;
//    private List<Comment> comments;
}
