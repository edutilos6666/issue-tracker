package org.aghayevn.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Arrays;
import java.util.List;

/**
 * created by  Nijat Aghayev on 01.04.20
 */

@Document
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Role {
    @Id
    private String id;
    private String name;
    private boolean readonly;

    public static final Role ROLE_ADMIN = new Role("1", "ROLE_ADMIN", false);
    public static final Role ROLE_TEAM_LEADER = new Role("2", "ROLE_TEAM_LEADER", false);
    public static final Role ROLE_DEVELOPER = new Role("3", "ROLE_DEVELOPER", false);
    public static final Role ROLE_TESTER = new Role("4", "ROLE_TESTER", false);
    public static final Role ROLE_USER = new Role("4", "ROLE_USER", true);

    public static List<Role> getAllPredefinedRoles() {
        return Arrays.asList(ROLE_ADMIN, ROLE_TEAM_LEADER, ROLE_DEVELOPER, ROLE_TESTER, ROLE_USER);
    }

    public static Role findByName(String name) {
        if(name.equalsIgnoreCase(ROLE_ADMIN.getName())) return ROLE_ADMIN;
        if(name.equalsIgnoreCase(ROLE_TEAM_LEADER.getName())) return ROLE_TEAM_LEADER;
        if(name.equalsIgnoreCase(ROLE_DEVELOPER.getName())) return ROLE_DEVELOPER;
        if(name.equalsIgnoreCase(ROLE_TESTER.getName())) return ROLE_TESTER;
        return ROLE_USER;
    }
}
