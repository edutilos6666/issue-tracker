package org.aghayevn.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * created by  Nijat Aghayev on 01.04.20
 */

@Document
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Label {
    @Id
    private String id;
    private String name;
}
