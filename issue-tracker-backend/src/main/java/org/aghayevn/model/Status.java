package org.aghayevn.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Arrays;
import java.util.List;

/**
 * created by  Nijat Aghayev on 01.04.20
 */

@Document
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Status {
    @Id
    private String id;
    private String name;

    public static final Status STATUS_OPEN = new Status("1", "STATUS_OPEN");
    public static final Status STATUS_CLOSED = new Status("2", "STATUS_CLOSED");
    public static final Status STATUS_REOPENED = new Status("3", "STATUS_REOPENED");

    public static List<Status> getAllPredefinedStatus() {
        return Arrays.asList(STATUS_OPEN, STATUS_CLOSED, STATUS_REOPENED);
    }
}
