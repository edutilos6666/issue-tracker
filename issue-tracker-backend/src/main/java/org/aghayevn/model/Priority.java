package org.aghayevn.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Arrays;
import java.util.List;

/**
 * created by  Nijat Aghayev on 01.04.20
 */

@Document
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Priority {
    @Id
    private String id;
    private String name;

    public static final Priority PRIORITY_URGENT = new Priority("1", "PRIORITY_URGENT");
    public static final Priority PRIORITY_HIGH = new Priority("2", "PRIORITY_HIGH");
    public static final Priority PRIORITY_MEDIUM = new Priority("3", "PRIORITY_MEDIUM");
    public static final Priority PRIORITY_LOW = new Priority("4", "PRIORITY_LOW");

    public static List<Priority> getAllPredefinedPriorities() {
        return Arrays.asList(PRIORITY_URGENT, PRIORITY_HIGH, PRIORITY_MEDIUM, PRIORITY_LOW);
    }
}
