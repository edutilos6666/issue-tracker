package org.aghayevn.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Arrays;
import java.util.List;

/**
 * created by  Nijat Aghayev on 01.04.20
 */

@Document
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Severity {
    @Id
    private String id;
    private String name;

    public static final Severity SEVERITY_BLOCKER = new Severity("1", "SEVERITY_BLOCKER");
    public static final Severity SEVERITY_CRITICAL = new Severity("2", "SEVERITY_CRITICAL");
    public static final Severity SEVERITY_MAJOR = new Severity("3", "SEVERITY_MAJOR");
    public static final Severity SEVERITY_MINOR = new Severity("4", "SEVERITY_MINOR");
    public static final Severity SEVERITY_TRIVIAL = new Severity("5", "SEVERITY_TRIVIAL");

    public static List<Severity> getAllPredefinedSeverities() {
        return Arrays.asList(SEVERITY_BLOCKER, SEVERITY_CRITICAL, SEVERITY_MAJOR, SEVERITY_MINOR, SEVERITY_TRIVIAL);
    }
}
