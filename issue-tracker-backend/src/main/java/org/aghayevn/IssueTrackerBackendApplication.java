package org.aghayevn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//@EnableDiscoveryClient
@SpringBootApplication
public class IssueTrackerBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(IssueTrackerBackendApplication.class, args);
	}


//	@RestController
//	class ServiceInstanceRestController {
//		@Autowired
//		private DiscoveryClient discoveryClient;
//
//		@GetMapping("/service-instances/{applicationName}")
//		public Flux<ServiceInstance> serviceInstances(@PathVariable String applicationName) {
//			return Flux.fromIterable(discoveryClient.getInstances(applicationName));
//		}
//	}

//	@PostConstruct
//	public void postConstruct() {
//		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
//	}


}
