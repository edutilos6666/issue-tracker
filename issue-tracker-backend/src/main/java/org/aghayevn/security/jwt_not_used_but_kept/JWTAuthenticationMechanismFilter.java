package org.aghayevn.security.jwt_not_used_but_kept;

import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import lombok.extern.slf4j.Slf4j;
import org.aghayevn.security.jwt.TokenProvider;
import org.springframework.http.HttpCookie;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.web.server.authentication.AuthenticationWebFilter;
import org.springframework.util.MultiValueMap;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilterChain;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * created by  Nijat Aghayev on 08.04.20
 */

// This class can be useful for Servlet based blocking web applications for spring security support
    // Becase I have refactored this class very nicely.
@Slf4j
public class JWTAuthenticationMechanismFilter extends AuthenticationWebFilter {
    private static final Pattern PATTERN_AUTHORIZATION_HEADER = Pattern.compile("^Bearer [a-zA-Z0-9\\-_\\.]+$", Pattern.CASE_INSENSITIVE);
    private static final Pattern PATTERN_AUTHORIZATION_COOKIE = Pattern.compile("^[a-zA-Z0-9\\-_\\.]+$", Pattern.CASE_INSENSITIVE);
    private ReactiveAuthenticationManager authenticationManager;
    private TokenProvider tokenProvider;


    public JWTAuthenticationMechanismFilter(ReactiveAuthenticationManager authenticationManager, TokenProvider tokenProvider) {
        super(authenticationManager);
        this.authenticationManager = authenticationManager;
        this.tokenProvider = tokenProvider;
    }

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse response = exchange.getResponse();
        addCorsHeader(request, response);
        log.info("validateRequest: {}", request.getURI().toString());

        if(request.getMethod() != null && request.getMethod().equals(HttpMethod.OPTIONS)) {
            return chain.filter(exchange);
        }

        String header = getAuthorization(request);
        if(header == null) {
            log.warn("Authorization header is missing.");
            return chain.filter(exchange);
        }
        if(!isValidAuthorizationCookie(header) && !isValidAuthorizationHeader(header)) {
            log.warn("Authorization header is invalid.");
            return chain.filter(exchange);
        }
        // extract & verify token
        try {
            String token = "";
            if(header.contains(" ")) {
                token = header.split(" ")[1];
            }

            DecodedJWT jwt = tokenProvider.verifyAndDecodeJwt(token);
            String userId = jwt.getSubject();
            List<String> roles = jwt.getClaim("roles").asList(String.class);
            UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(userId, null,
                    roles.parallelStream()
                            .map(SimpleGrantedAuthority::new)
                            .collect(Collectors.toList())
            );
            authenticationManager.authenticate(auth);
            ReactiveSecurityContextHolder.withAuthentication(auth);
//            return ReactiveSecurityContextHolder.getContext().flatMap(securityContext -> {
//                securityContext.setAuthentication(auth);
//                return chain.filter(exchange);
//            });
        } catch(JWTVerificationException ex) {
            log.warn("JWT token verification failed", ex);
            ReactiveSecurityContextHolder.clearContext();
        }


        return chain.filter(exchange);
    }

    private String getAuthorization(final ServerHttpRequest request) {
        MultiValueMap<String , HttpCookie> cookies = request.getCookies();
        if(cookies.containsKey("auth_token")) {
            return cookies.getFirst("auth_token").getValue();
        }
        List<String> headers = request.getHeaders().get(HttpHeaders.AUTHORIZATION);
        if(headers != null && !headers.isEmpty())
            return headers.get(0);

        return null;
    }

    private boolean isValidAuthorizationHeader(final String header) {
        return PATTERN_AUTHORIZATION_HEADER.matcher(header).matches();
    }

    private boolean isValidAuthorizationCookie(final String header) {
        return PATTERN_AUTHORIZATION_COOKIE.matcher(header).matches();
    }

    private String getOrigin(ServerHttpRequest httpRequest) {
        if (httpRequest.getHeaders().containsKey("origin")) {
            return httpRequest.getHeaders().get("origin").get(0);
        } else return "*";
    }

    private void addCorsHeader(final ServerHttpRequest request,
                                    final ServerHttpResponse response) {
        response.getHeaders().add("Access-Control-Allow-Origin", getOrigin(request));
        response.getHeaders().add("Access-Control-Allow-Methods", "POST, PATCH, GET, OPTIONS, DELETE, PUT");
//        response.getHeaders().add("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,Authorization,X-Test");
        response.getHeaders().add("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, Credentials");
        response.getHeaders().add("Access-Control-Allow-Credentials", "true");
//        response.getHeaders().add("Access-Control-Max-Age", "86400");
    }


}
