package org.aghayevn.security.jwt;

import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;
import reactor.core.publisher.Mono;

/**
 * created by  Nijat Aghayev on 08.04.20
 */
public class CORSFilter implements WebFilter {

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain) {
        addCorsHeader(exchange.getRequest(), exchange.getResponse());
        return chain.filter(exchange);
    }

    private String getOrigin(ServerHttpRequest httpRequest) {
        if (httpRequest.getHeaders().containsKey("origin")) {
            return httpRequest.getHeaders().get("origin").get(0);
        } else return "*";
    }

    private void addCorsHeader(final ServerHttpRequest request,
                               final ServerHttpResponse response) {
        response.getHeaders().add("Access-Control-Allow-Origin", getOrigin(request));
        response.getHeaders().add("Access-Control-Allow-Methods", "POST, PATCH, GET, OPTIONS, DELETE, PUT");
//        response.getHeaders().add("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,Authorization,X-Test");
        response.getHeaders().add("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, Credentials");
        response.getHeaders().add("Access-Control-Allow-Credentials", "true");
//        response.getHeaders().add("Access-Control-Max-Age", "86400");
    }


}
