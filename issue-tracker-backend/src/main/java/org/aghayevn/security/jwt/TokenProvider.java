package org.aghayevn.security.jwt;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;
import org.aghayevn.model.Role;
import org.aghayevn.model.User;
import org.aghayevn.security.auth.AuthResponse;
import org.aghayevn.security.auth.AuthorizationToken;
import org.aghayevn.security.model.CurrentUserHolder;
import org.aghayevn.util.CustomDateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import reactor.core.publisher.Mono;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

/**
 * created by  Nijat Aghayev on 07.04.20
 */
@Component
@Slf4j
public class TokenProvider implements Serializable {

    private static final long serialVersionUID = 1L;

    @Value("${security.token.secret}")
    private String secret;
    @Value("${security.token.expiration}")
    private String expirationTime;
    @Value("${security.token.issuer}")
    private String issuer;
    @Value("${security.token.validity}")
    private long validity;

    @Autowired
    private CustomDateUtils customDateUtils;
    @Autowired
    private Algorithm algorithmRS;
    @Autowired
    private CurrentUserHolder currentUserHolder;

    public static final String AUTHORITIES_KEY = "roles";

    public Claims getAllClaimsFromToken(String token) {
        return Jwts.parser().setSigningKey(Base64.getEncoder().encodeToString(secret.getBytes())).parseClaimsJws(token).getBody();
    }

    public String getUsernameFromToken(String token) {
        return getAllClaimsFromToken(token).getSubject();
    }

    public Date getExpirationDateFromToken(String token) {
        return getAllClaimsFromToken(token).getExpiration();
    }

    private Boolean isTokenExpired(String token) {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }


    public Mono<AuthorizationToken> renewAuthorizationTokenForCurrentUser() throws JWTCreationException {
        LocalDateTime now = LocalDateTime.now();
        Date issuedAt = customDateUtils.toDate(now);
        Date expiresAt = customDateUtils.toDate(now.plus(validity, ChronoUnit.MINUTES));
        Date notBefore = customDateUtils.toDate(now.minus(10,ChronoUnit.MINUTES));


        return currentUserHolder
                .resetAndGet()
                .map(user-> {
                    String subject = user.getId();
                    String token = issueToken(user, issuedAt, expiresAt, notBefore);
                    return new AuthorizationToken(
                            token,
                            subject,
                            issuer,
                            issuedAt,
                            expiresAt,
                            validity,
                            user.getFirstname(),
                            user.getLastname(),
                            user.getUsername()
                    );
                });

    }

    public DecodedJWT verifyAndDecodeJwt(String token) {
        return JWT.require(algorithmRS)
                .withIssuer(issuer)
                .acceptLeeway(1)
                .acceptExpiresAt(5)
                .build()
                .verify(token);
    }




    public String getTokenFromAuthResponse(AuthResponse authResponse, User user) {
        Set<String> roles = user.getRoles()
                .parallelStream()
                .map(Role::getName)
                .collect(Collectors.toSet());

        return issueToken(
                issuer,
                new Date(),
                new Date(System.currentTimeMillis() + (authResponse.getExpiresIn() * 60 * 1000)),
                customDateUtils.toDate(LocalDateTime.now().minus(30, ChronoUnit.MINUTES)),
                authResponse.getUserId(),
                user.getFirstname(),
                user.getLastname(),
                user.getUsername(),
                roles
        );
    }


    public String issueToken(User user, Date issuedAt, Date expiresAt, Date notBefore) {
        return issueToken(
                issuer,
                issuedAt,
                expiresAt,
                notBefore,
                user.getId(),
                user.getFirstname(),
                user.getLastname(),
                user.getUsername(),
                user.getRoles()
                        .parallelStream()
                        .map(Role::getName)
                        .collect(Collectors.toSet())
        );
    }



    public String  issueToken(
            final String issuer,
            final Date issuedAt,
            final Date expiresAt,
            final Date notBefore,
            final String subject,
            final String firstname,
            final String lastname,
            final String username,
            final Set<String> roles
    ) throws JWTCreationException {
        return JWT.create()
                .withIssuer(issuer)
                .withIssuedAt(issuedAt)
                .withExpiresAt(expiresAt)
                .withNotBefore(notBefore)
                .withSubject(subject)
                .withClaim("firstname", firstname)
                .withClaim("lastname", lastname)
                .withClaim("username", username)
                .withArrayClaim(AUTHORITIES_KEY , roles.toArray(new String[0]))
                .sign(algorithmRS);
    }



    public Authentication getAuthentication(String token) {
        DecodedJWT decodeJwt = verifyAndDecodeJwt(token);
        if (StringUtils.isEmpty(token) || decodeJwt == null) {
            throw new BadCredentialsException("Invalid token");
        }
//        Claims claims = Jwts.parser()
////                .setSigningKey(secretKey)
//                .parseClaimsJws(token)
//                .getBody();

        Map<String, Claim> claims = decodeJwt.getClaims();

        Collection<? extends GrantedAuthority> authorities =
                Arrays.stream(claims.get(AUTHORITIES_KEY).toString().split(","))
                        .map(SimpleGrantedAuthority::new)
                        .collect(Collectors.toList());

        User principal = new User(decodeJwt.getSubject(),
                authorities
                        .parallelStream()
                        .map(one-> Role.findByName(((GrantedAuthority) one).getAuthority()))
                        .collect(Collectors.toList()));

        return new UsernamePasswordAuthenticationToken(principal, token, authorities);
    }

    public boolean validateToken(String authToken) {
        try {
            Jwts.parser()
//                    .setSigningKey(secretKey)
                    .parseClaimsJws(authToken);
            return true;
        } catch (SignatureException e) {
            log.info("Invalid JWT signature.");
            log.trace("Invalid JWT signature trace: {}", e);
        } catch (MalformedJwtException e) {
            log.info("Invalid JWT token.");
            log.trace("Invalid JWT token trace: {}", e);
        } catch (ExpiredJwtException e) {
            log.info("Expired JWT token.");
            log.trace("Expired JWT token trace: {}", e);
        } catch (UnsupportedJwtException e) {
            log.info("Unsupported JWT token.");
            log.trace("Unsupported JWT token trace: {}", e);
        } catch (IllegalArgumentException e) {
            log.info("JWT token compact of handler are invalid.");
            log.trace("JWT token compact of handler are invalid trace: {}", e);
        }
        return false;
    }
}