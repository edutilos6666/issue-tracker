package org.aghayevn.security.model;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.aghayevn.dao.UserRepository;
import org.aghayevn.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;
import reactor.core.publisher.Mono;

/**
 * created by  Nijat Aghayev on 08.04.20
 */

@Slf4j
@Component
// @Scope("session" | "request") => works only with Spring MVC, not with Spring WebFlux
//@SessionScope
@Data
// did not work
//@Scope(value = "websession", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class CurrentUserHolderImpl implements CurrentUserHolder {
    private User user;
    @Autowired
    private UserRepository userRepository;

    @Override
    public Mono<User> getUserNonBlockingWay() {
        if(user == null) {
            return ReactiveSecurityContextHolder
                    .getContext()
                    .map(SecurityContext::getAuthentication)
                    .flatMap(authentication -> {
                        if(authentication != null && authentication.isAuthenticated()) {
                            String id = ((User)authentication.getPrincipal()).getId();
                            if(id != null && id.length() > 0) {
                                return userRepository.findById(id);
                            }
                        }
                        return Mono.empty();
                    })
                    .map(user -> {
                        this.user = user;
                        return user;
                    });
        }
        return Mono.just(user);
    }

    @Override
    public User getUser() {
        if(user == null) {
            Authentication authentication = ReactiveSecurityContextHolder.getContext().map(SecurityContext::getAuthentication).block();
            if(authentication != null && authentication.isAuthenticated()) {
                String username = (String)authentication.getPrincipal();
                if(username != null && username.length() > 0) {
                    user = userRepository.findById(username).block();
                }
            }
        }
        return user;
    }

    @Override
    public String getId() {
        if(getUser() == null) return null;
        return getUser().getId();
    }

    @Override
    public Mono<User> resetAndGet() {
        user = null;
        return getUserNonBlockingWay();
    }

    @Override
    public Mono<Void> reset() {
        user = null;
        return Mono.empty();
    }
}
