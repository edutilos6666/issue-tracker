package org.aghayevn.security.model;

import org.aghayevn.model.User;
import reactor.core.publisher.Mono;

/**
 * created by  Nijat Aghayev on 08.04.20
 */


public interface CurrentUserHolder {
    Mono<User> getUserNonBlockingWay();
    User getUser();
    String getId();
    Mono<User> resetAndGet();
    Mono<Void> reset();
}
