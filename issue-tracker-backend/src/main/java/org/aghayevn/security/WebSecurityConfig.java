package org.aghayevn.security;

import com.auth0.jwt.algorithms.Algorithm;
import lombok.extern.slf4j.Slf4j;
import org.aghayevn.dao.UserRepository;
import org.aghayevn.security.jwt.*;
import org.aghayevn.util.RSAKeyGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableReactiveMethodSecurity;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.SecurityWebFiltersOrder;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.server.SecurityWebFilterChain;
import org.springframework.security.web.server.authentication.AuthenticationWebFilter;
import org.springframework.security.web.server.context.WebSessionServerSecurityContextRepository;

/**
 * created by  Nijat Aghayev on 07.04.20
 */

@Slf4j
@EnableWebFluxSecurity
@EnableReactiveMethodSecurity
@EnableAspectJAutoProxy
public class WebSecurityConfig {
    private static final String[] AUTH_WHITELIST = {
            "/resources/**",
            "/webjars/**",
            "/authentication/public-key",
            "/authentication/login",
            "/authentication/login-with-token",
            "/users/username-exists/**",
            "/users",
            "/roles",
            "/favicon.ico"
    };


    @Autowired
    private RSAKeyGenerator rsaKeyGenerator;
    @Autowired
    private TokenProvider tokenProvider;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private UserRepository userRepository;

    @Bean
    public Algorithm algorithmRS() {
        return Algorithm.RSA256(rsaKeyGenerator.getPublicKey(), rsaKeyGenerator.getPrivateKey());
    }



    @Bean
    public SecurityWebFilterChain securitygWebFilterChain(ServerHttpSecurity http) {
        return http
                .csrf().disable()
                .httpBasic().disable()
                .formLogin().disable()
                .logout().disable()
//                .securityContextRepository(new WebSessionServerSecurityContextRepository())
//                .addFilterAt(new JWTAuthenticationMechanismFilter(authenticationManager, tokenProvider), SecurityWebFiltersOrder.AUTHORIZATION)
                .addFilterBefore(new CORSFilter(), SecurityWebFiltersOrder.CORS)
                .addFilterAt(webFilter(), SecurityWebFiltersOrder.AUTHORIZATION)
                .authorizeExchange()
                .pathMatchers("/health", "/info")
                .permitAll()
                .and()
                .authorizeExchange()
                .pathMatchers(HttpMethod.OPTIONS).permitAll()
                .and()
                .authorizeExchange()
                .pathMatchers(AUTH_WHITELIST)
                .permitAll()
                .and()
                .authorizeExchange()
                .anyExchange().authenticated()
                .and()
                .build();

    }


    @Bean
    public AuthenticationWebFilter webFilter() {
        AuthenticationWebFilter authenticationWebFilter = new AuthenticationWebFilter(repositoryReactiveAuthenticationManager());
        authenticationWebFilter.setAuthenticationConverter(new TokenAuthenticationConverter(tokenProvider));
        authenticationWebFilter.setRequiresAuthenticationMatcher(new JWTHeadersExchangeMatcher());
        authenticationWebFilter.setSecurityContextRepository(new WebSessionServerSecurityContextRepository());
        return authenticationWebFilter;
    }



    @Bean
    public JWTReactiveAuthenticationManager repositoryReactiveAuthenticationManager() {
        JWTReactiveAuthenticationManager repositoryReactiveAuthenticationManager = new JWTReactiveAuthenticationManager(userRepository, passwordEncoder);
        return repositoryReactiveAuthenticationManager;
    }

}
