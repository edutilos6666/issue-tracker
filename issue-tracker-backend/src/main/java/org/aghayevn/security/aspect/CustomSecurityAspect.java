package org.aghayevn.security.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.stereotype.Component;

/**
 * created by  Nijat Aghayev on 08.04.20
 */

@Aspect
@Component
@Order(1)
public class CustomSecurityAspect {

    @Before("isDefinedInApplication() && !isMethodAnnotatedAsUnsecured()")
    public void doSecurityCheck() {
        ReactiveSecurityContextHolder.getContext().map(SecurityContext::getAuthentication).subscribe(authentication -> {
            if(!authentication.isAuthenticated() ||
            AnonymousAuthenticationToken.class.isAssignableFrom(authentication.getClass()))
                throw new NotAuthenticatedException("User not authenticated");
//            return authentication;
        });
//        if(ReactiveSecurityContextHolder.getContext() == null ||
//                ReactiveSecurityContextHolder.getContext().getAuthentication() == null ||
//          !SecurityContextHolder.getContext().getAuthentication().isAuthenticated() ||
//                AnonymousAuthenticationToken.class.isAssignableFrom(SecurityContextHolder.getContext().getAuthentication().getClass())) {
//            throw new NotAuthenticatedException("User not authenticated");
//        }
    }

    @Pointcut("within(org.aghayevn..*)")
    private void isDefinedInApplication() {

    }

    @Pointcut("@annotation(org.aghayevn.security.aspect.Unsecured)")
    private void isMethodAnnotatedAsUnsecured() {

    }
}
