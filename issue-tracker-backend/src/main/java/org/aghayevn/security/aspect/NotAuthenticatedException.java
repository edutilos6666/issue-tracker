package org.aghayevn.security.aspect;

/**
 * created by  Nijat Aghayev on 08.04.20
 */
public class NotAuthenticatedException extends RuntimeException {
    public NotAuthenticatedException(String message) {
        super(message);
    }
}
