package org.aghayevn.security.auth;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
/**
 * created by  Nijat Aghayev on 07.04.20
 */
@Data @NoArgsConstructor @AllArgsConstructor @ToString
public class  AuthRequest {
    private String username;
    private String password;
    private String newPassword;
    private String tokenType;

    public AuthRequest(String username, String password) {
        this(username, password, "", "");
        this.username = username;
        this.password = password;
        this.newPassword = "";
        this.tokenType = "";
    }

    public AuthRequest(String username, String password, String newPassword) {
        this(username, password, newPassword, "");
    }
}