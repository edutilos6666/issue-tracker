package org.aghayevn.security.auth;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * created by  Nijat Aghayev on 08.04.20
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AuthInformation {
    private AuthResponse authResponse;
    private AuthError authError;
    String token;
    private int status;

    public AuthInformation(AuthResponse authResponse, String token) {
        this.authResponse = authResponse;
        this.token = token;
        this.status = 200;
    }

    public AuthInformation(AuthError authError, int status) {
        this.authError = authError;
        this.status = status;
    }

    public AuthInformation(int status) {
        this.status = status;
    }
}
