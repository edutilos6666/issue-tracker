package org.aghayevn.security.auth;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.Validate;

import java.util.Date;

/**
 * created by  Nijat Aghayev on 08.04.20
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AuthorizationToken {
    private String token;
    private String tokenType = "Bearer";
    private String subject;
    private String issuer;
    private long issuedAt;
    private long expiresAt;
    private long expiresIn;
    private long expireMinutes;
    private String firstname;
    private String lastname;
    private String username;

    public AuthorizationToken(
            final String token,
            final String subject,
            final String issuer,
            final Date issuedAt,
            final Date expiresAt,
            final long expireMinutes,
            final String firstname,
            final String lastname,
            final String username) {
        this.token = Validate.notNull(token, "token can not be null");
        this.subject = Validate.notNull(subject, "subject can not be null");
        this.issuer = Validate.notNull(issuer, "issuer can  not be null");
        this.issuedAt = Validate.notNull(issuedAt, "issuedAt can not be null").getTime();
        this.expiresAt = Validate.notNull(expiresAt, "expiresAt can not be null").getTime();
        this.expiresIn = (expiresAt.getTime() - issuedAt.getTimezoneOffset()) / 1000;
        this.firstname = firstname;
        this.lastname = lastname;
        this.username = username;
        this.expireMinutes = expireMinutes;
    }
}
