package org.aghayevn.security.auth;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.ZonedDateTime;
import java.util.UUID;

/**
 * created by  Nijat Aghayev on 07.04.20
 */
@Data @NoArgsConstructor @AllArgsConstructor @ToString
public class AuthResponse {
    private String tokenType;
    private String accessToken;
    private String refreshToken;
    private Integer expiresIn;
    private String userId;
    private ZonedDateTime lastLogin = ZonedDateTime.now();

    // e.g. tokenType = 'IAuth'
    public static AuthResponse createAuthResponse(String userId, String accessToken, String tokenType) {
        if(userId == null) return null;
        AuthResponse response = new AuthResponse();
        response.setTokenType(tokenType);
        response.setRefreshToken(UUID.randomUUID().toString());
        response.setExpiresIn(60);
        response.setUserId(userId);
        response.setAccessToken(accessToken != null? accessToken: UUID.randomUUID().toString());
        return response;
    }
}