package org.aghayevn.security.auth;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * created by  Nijat Aghayev on 08.04.20
 */

@Data
public class AuthError {
    @JsonProperty("error")
    private String error;
    @JsonProperty("error-description")
    private String errorDescription;
    @JsonProperty("x-error")
    private String xError;
    private static final String invalidClient = "invalid_client";
    private static final String invalidRequest = "invalid_request";
    private static final String invalidGrant = "invalid_grant";

    public static AuthError getInvalidLogin() {
        AuthError error = new AuthError();
        error.setError(invalidClient);
        error.setErrorDescription("Wrong username or password");
        error.setXError("auth:unknown");
        return error;
    }

    public static AuthError getExpiredLogin() {
        AuthError error = new AuthError();
        error.setErrorDescription(invalidGrant);
        error.setErrorDescription("Password expired and must be renewed.");
        error.setXError("auth:password-expired");
        return error;
    }

    public static AuthError getMissingLogin() {
        AuthError error = new AuthError();
        error.setError(invalidRequest);
        error.setErrorDescription("Missing username or password");
        error.setXError("auth:invalid");
        return error;
    }

    public static AuthError getBadPassword() {
        AuthError error = new AuthError();
        error.setErrorDescription(invalidRequest);
        error.setErrorDescription("The password you entered doesn't meet password policy requirements");
        error.setXError("auth:bad-password");
        return error;
    }

    public static AuthError getIncorrectPassword() {
        AuthError error = new AuthError();
        error.setError(invalidRequest);
        error.setErrorDescription("Password incorrect");
        error.setXError("auth:incorrect-password");
        return error;
    }

    public static AuthError getPasswordAlreadyUsed() {
        AuthError error = new AuthError();
        error.setError(invalidRequest);
        error.setErrorDescription("Password already used");
        error.setXError("auth:password-already-used");
        return error;
    }

    public static AuthError getUserDoesntExist() {
        AuthError error = new AuthError();
        error.setError(invalidRequest);
        error.setErrorDescription("Account doesn't exist");
        error.setXError("auth:account-does-not-exist");
        return error;
    }
}
